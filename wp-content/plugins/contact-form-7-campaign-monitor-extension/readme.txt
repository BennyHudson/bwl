=== Contact Form 7 Campaign Monitor Extension ===
Contributors: rnzo
Donate link: http://renzojohnson.com/contributions/contact-form-7-campaign-monitor-extension
Tags: contact form 7, email, mailing list, createsend, renzo johnson, cme, contact, form, contact form, campaign monitor
Requires at least: 3.9
Tested up to: 4.3
Stable tag: 4.0.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Simple way to integrate Campaign Monitor mailing lists to Contact Form 7. Save your subscribers in a proper mailing list.

== Description ==

Integrate Contact Form 7 with Campaign Monitor. Automatically add form submissions to predetermined lists in Campaign Monitor, using its latest API. This Campaign Monitor Extension supports multiple mailing lists and API Keys.


= Key Features =

* Easy to use
* Use a different mailing list per contact form
* Opt-in checkbox
* Support for custom fields
* Latest Campaign Monitor API
* Constantly updated

= Requirements =

1. Self hosted WordPress.org installation (3.9 or higher)
2. Contact Form 7 (4.2 or higher)
3. Campaign Monitor account


= Leave a Comment or a Review =

If this plugin is useful to you, please leave a review here: [Campaign Monitor Extension Review Page](https://wordpress.org/support/view/plugin-reviews/contact-form-7-campaign-monitor-extension) Or Leave a comment here: [Campaign Monitor Extension comments page](http://renzojohnson.com/contributions/contact-form-7-campaign-monitor-extension)



== Installation ==

1. Upload the entire 'contact-form-7-campaign-monitor-extension' folder to the '/wp-content/plugins/' directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.

You will find 'Campaign Monitor: Subscriber List Details' menu in your Contact Form 7 editor.

For basic usage, you can also have a look at the [Campaign Monitor Extension homepage](http://renzojohnson.com/contributions/contact-form-7-campaign-monitor-extension), you can also have a look at the [Contact Form 7 homepage](http://contactform7.com/).

== Frequently Asked Questions ==

Do you have questions or issues with Contact Form 7 Campaign Monitor Extension? Use these support channels appropriately.

1. [Campaign Monitor Extension Docs](http://renzojohnson.com/contributions/contact-form-7-campaign-monitor-extension)
2. [Campaign Monitor Extension FAQ](http://renzojohnson.com/contributions/contact-form-7-campaign-monitor-extension)
3. [Campaign Monitor Extension Support Forum](http://renzojohnson.com/contributions/contact-form-7-campaign-monitor-extension)

[Support](http://renzojohnson.com/contributions/contact-form-7-campaign-monitor-extension)

== Screenshots ==

1. screenshot-1.png
2. screenshot-2.png

== Changelog ==

For more information, see [Releases](http://renzojohnson.com/contributions/contact-form-7-campaign-monitor-extension).

= 0.3.10 =

* Fixed a bug when 'duplicating' a contact form 7

= 0.3.6 =

* Added more custom fields

= 0.3.2 =

* Hotfix to work with the new CF7 editor panels

= 0.3.1 =

* Fixed error on line 36


= 0.3.0 =

* Updated readme.txt

= 0.2.9 =

* Added opt-in checkbox

= 0.1.2 =

* Release version


= 0.1.5 =

* Updated documentation

= 0.1.6 =

* Help button added
