<?php

	add_action( 'after_setup_theme', 'tastic_theme_setup' );
	
	function tastic_theme_setup() {
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );
		add_action( 'widgets_init', 'tastic_register_sidebars' );
		add_filter('wp_mail_from_name', 'new_mail_from_name');
		add_action( 'init', 'register_my_menus' );
		add_action( 'wp_enqueue_scripts', 'jk_load_dashicons' );
		add_action('init', 'flush_rewrite_rules');	
		add_theme_support( 'woocommerce' );
	} 

// Enqueue Dashicons

	function jk_load_dashicons() {
		wp_enqueue_style( 'dashicons' );
	}

// Widget Areas
	
	function tastic_register_sidebars() {
		register_sidebar( array(
			'name' => 'Sidebar',
			'id' => 'blog-sidebar',
			'before_widget' => '<section class="widget">',
			'after_widget' => '</section>',
			'before_title' => '<h2>',
			'after_title' => '</h2>',
		) );
		register_sidebar( array(
			'name' => 'Shop Sidebar',
			'id' => 'shop-sidebar',
			'before_widget' => '<section class="widget">',
			'after_widget' => '</section>',
			'before_title' => '<h2>',
			'after_title' => '</h2>',
		) );
		register_sidebar( array(
			'name' => 'Home Widgets',
			'id' => 'home-widgets',
			'before_widget' => '<aside><section class="widget">',
			'after_widget' => '</section></aside>',
			'before_title' => '<h2>',
			'after_title' => '</h2>',
		) );
		register_sidebar( array(
			'name' => 'Header Search',
			'id' => 'header-search',
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '',
			'after_title' => '',
		) );
		register_sidebar( array(
			'name' => 'Header Adverts',
			'id' => 'header-adverts',
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '',
			'after_title' => '',
		) );
		register_sidebar( array(
			'name' => 'Footer Adverts',
			'id' => 'footer-adverts',
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '',
			'after_title' => '',
		) );
		register_sidebar( array(
			'name' => 'Footer Menus',
			'id' => 'footer-menus',
			'before_widget' => '<aside class="footer-menu">',
			'after_widget' => '</aside>',
			'before_title' => '<h2>',
			'after_title' => '</h2>',
		) );
		register_sidebar( array(
			'name' => 'Login Area',
			'id' => 'login-area',
			'before_widget' => '<li>',
			'after_widget' => '</li>',
			'before_title' => '<h2>',
			'after_title' => '</h2>',
		) );
	}
	
// Menus

	function register_my_menus() {
	  register_nav_menus(
		array( 'mobile-menu' => __( 'Mobile Menu' ),
			   'footer-menu' => __( 'Footer Menu' ),
			   'header-menu' => __( 'Header Menu' )
			 )
	  );
	}

// Custom Post Types

// Custom Taxonomies	

	
// Email Settings

	function new_mail_from_name($old) {
	 return 'Wood Floors Magazine';
	}
	
// WooCommerce Functions

// Extra Functions

	add_filter('single_template', create_function(
		'$the_template',
		'foreach( (array) get_the_category() as $cat ) {
			if ( file_exists(TEMPLATEPATH . "/single-{$cat->slug}.php") )
			return TEMPLATEPATH . "/single-{$cat->slug}.php"; }
		return $the_template;' )
	);	
	add_filter( 'post_class', 'wpse_2266_custom_taxonomy_post_class', 10, 3 );
	if ( ! function_exists('wpse_2266_custom_taxonomy_post_class') ) {
		function wpse_2266_custom_taxonomy_post_class($classes, $class, $ID) {
			$taxonomies_args = array(
				'public' => true,
				'_builtin' => false,
			);
			$taxonomies = get_taxonomies( $taxonomies_args, 'names', 'and' );
			$terms = get_the_terms( (int) $ID, (array) $taxonomies );
			if ( ! empty( $terms ) ) {
				foreach ( (array) $terms as $order => $term ) {
					if ( ! in_array( $term->slug, $classes ) ) {
						$classes[] = $term->slug;
					}
				}
			}
			$classes[] = 'clearfix';
			return $classes;
		}
	}
	function the_title_trim($title) {
		$title = attribute_escape($title);
		$findthese = array(
			'#Protected:#',
			'#Private:#'
		);
		$replacewith = array(
			'', // What to replace "Protected:" with
			'' // What to replace "Private:" with
		);
		$title = preg_replace($findthese, $replacewith, $title);
		return $title;
	}
	add_filter('the_title', 'the_title_trim');
	define('WOOCOMMERCE_USE_CSS', false);
	add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 15;' ), 20 );
?>