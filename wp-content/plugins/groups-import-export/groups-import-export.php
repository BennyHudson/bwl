<?php
/**
 * groups-import-export.php
 * 
 * Copyright (c) 2014 "kento" Karim Rahimpur www.itthinx.com
 * 
 * =============================================================================
 * 
 *                             LICENSE RESTRICTIONS
 * 
 *           This plugin is provided subject to the license granted.
 *              Unauthorized use and distribution is prohibited.
 *                     See COPYRIGHT.txt and LICENSE.txt.
 * 
 * Files licensed under the GNU General Public License state so explicitly in
 * their header or where implied. Other files are not licensed under the GPL
 * and the license obtained applies.
 * 
 * =============================================================================
 * 
 * You MUST be granted a license by the copyright holder for those parts that
 * are not provided under the GPLv3 license.
 * 
 * If you have not been granted a license DO NOT USE this plugin until you have
 * BEEN GRANTED A LICENSE.
 * 
 * Use of this plugin without a granted license constitutes an act of COPYRIGHT
 * INFRINGEMENT and LICENSE VIOLATION and may result in legal action taken
 * against the offending party.
 * 
 * Being granted a license is GOOD because you will get support and contribute
 * to the development of useful free and premium themes and plugins that you
 * will be able to enjoy.
 * 
 * Thank you!
 * 
 * Visit www.itthinx.com for more information.
 * 
 * =============================================================================
 * 
 * This code is released under the GNU General Public License.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * 
 * This header and all notices must be kept intact.
 * 
 * @author Karim Rahimpur
 * @package groups-import-export
 * @since 1.0.0
 *
 * Plugin Name: Groups Import Export
 * Plugin URI: http://www.itthinx.com/plugins/groups-import-export/
 * Description: Import and export extension for <a href="http://www.itthinx.com/plugins/groups/">Groups</a>.
 * Author: itthinx
 * Author URI: http://www.itthinx.com
 * Version: 1.1.0
 */
define( 'GROUPS_IMPORT_EXPORT_PLUGIN_VERSION', '1.1.0' );
define( 'GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN', 'groups-import-export' );
define( 'GROUPS_IMPORT_EXPORT_PLUGIN_FILE', __FILE__ );
define( 'GROUPS_IMPORT_EXPORT_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'GROUPS_IMPORT_EXPORT_PLUGIN_DIR', WP_PLUGIN_DIR . '/groups-import-export' );
define( 'GROUPS_IMPORT_EXPORT_CORE_LIB', GROUPS_IMPORT_EXPORT_PLUGIN_DIR . '/lib/core' );
define( 'GROUPS_IMPORT_EXPORT_ADMIN_LIB', GROUPS_IMPORT_EXPORT_PLUGIN_DIR . '/lib/admin' );
define( 'GROUPS_IMPORT_EXPORT_VIEWS_LIB', GROUPS_IMPORT_EXPORT_PLUGIN_DIR . '/lib/views' );
if ( !function_exists( 'itthinx_plugins' ) ) {
	require_once 'itthinx/itthinx.php';
}
itthinx_plugins( __FILE__ );
require_once( GROUPS_IMPORT_EXPORT_CORE_LIB . '/class-groups-import-export.php' );
