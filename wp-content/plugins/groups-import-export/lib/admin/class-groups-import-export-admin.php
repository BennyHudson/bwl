<?php
/**
 * class-groups-import-export-admin.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is provided subject to the license granted.
 * Unauthorized use and distribution is prohibited.
 * See COPYRIGHT.txt and LICENSE.txt
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-import-export
 * @since 1.0.0
 */

/**
 * Admin section.
 */
class Groups_Import_Export_Admin {

	/**
	 * Admin options setup.
	 */
	public static function init() {
		add_action( 'admin_menu', array( __CLASS__, 'admin_menu' ) );
		add_action( 'admin_init', array( __CLASS__, 'admin_init' ) );
		add_filter( 'plugin_action_links_'. plugin_basename( GROUPS_IMPORT_EXPORT_PLUGIN_FILE ), array( __CLASS__, 'admin_settings_link' ) );
		add_action( 'admin_enqueue_scripts', array( __CLASS__, 'admin_enqueue_scripts' ) );
	}

	/**
	 * Admin options admin setup.
	 */
	public static function admin_init() {
		wp_register_style( 'groups_import_export_admin', GROUPS_IMPORT_EXPORT_PLUGIN_URL . 'css/admin.css', array(), GROUPS_IMPORT_EXPORT_PLUGIN_VERSION );
	}

	/**
	 * Loads styles for the admin section.
	 */
	public static function admin_print_styles() {
		wp_enqueue_style( 'groups_import_export_admin' );
	}

	/**
	 * Enqueues the select script.
	 */
	public static function admin_enqueue_scripts() {
		$screen = get_current_screen();
		if ( isset( $screen->id ) ) {
			switch( $screen->id ) {
				case 'groups_page_groups-user-import' :
				case 'groups_page_groups-user-export' :
					require_once GROUPS_VIEWS_LIB . '/class-groups-uie.php';
					Groups_UIE::enqueue( 'select' );
					break;
			}
		}
	}

	public static function admin_print_scripts() {
	}

	/**
	 * Add a menu item to the Appearance menu.
	 */
	public static function admin_menu() {
		$page = add_submenu_page(
			'groups-admin',
			__( 'Import Users', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ),
			__( 'Import Users', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ),
			'manage_options',
			'groups-user-import',
			array( __CLASS__, 'user_import' )
		);
		add_action( 'admin_print_styles-' . $page, array( __CLASS__, 'admin_print_styles' ) );
		add_action( 'admin_print_scripts-' . $page, array( __CLASS__, 'admin_print_scripts' ) );
		add_action( 'load-' . $page, array( __CLASS__, 'add_help_tab' ) );

		$page = add_submenu_page(
			'groups-admin',
			__( 'Export Users', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ),
			__( 'Export Users', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ),
			'manage_options',
			'groups-user-export',
			array( __CLASS__, 'user_export' )
		);
		add_action( 'admin_print_styles-' . $page, array( __CLASS__, 'admin_print_styles' ) );
		add_action( 'admin_print_scripts-' . $page, array( __CLASS__, 'admin_print_scripts' ) );
		add_action( 'load-' . $page, array( __CLASS__, 'add_help_tab' ) );
	}

	/**
	 * Adds the help tab.
	 */
	public static function add_help_tab() {
		if ( $screen = get_current_screen() ) {
			$id = null;
			switch( $screen->id ) {
				case 'groups_page_groups-user-import' :
					$id = 'import-users';
					$title = __( 'Import Users', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
					$content = '';
					$content .= '<h2>';
					$content .= __( 'File Format', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
					$content .= '</h2>';
					$content .= '<p>';
					$content .= __( 'The accepted file format is a text file with values separated by tabs, providing one ore more of the following values on a single line for each user:', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
					$content .= '</p>';
					$content .= '<ol>';
					$content .= '<li>';
					$content .= __( '<code>user_email</code> The user\'s email address.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
					$content .= '</li>';
					$content .= '<li>';
					$content .= __( '<code>user_login</code> The username.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
					$content .= '</li>';
					$content .= '<li>';
					$content .= __( '<code>first_name</code> The user\'s first name.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
					$content .= '</li>';
					$content .= '<li>';
					$content .= __( '<code>last_name</code> The user\'s surname.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
					$content .= '</li>';
					$content .= '<li>';
					$content .= __( '<code>user_url</code> The URL of the user\'s website.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
					$content .= '</li>';
					$content .= '<li>';
					$content .= __( '<code>user_pass</code> The user\'s password. Unless provided, a password is generated automatically.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
					$content .= '</li>';
					$content .= '<li>';
					$roles = get_editable_roles();
					$roles = array_keys( $roles );
					$roles = implode( ', ', $roles );
					$content .= sprintf( __( '<code>roles</code> The roles that the user should have. The user will only be assigned to existing roles, new roles are not created. The default role is assumed when empty. Roles are to be indicated by their keys: <code>%s</code>.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ), $roles );
					$content .= '</li>';
					$content .= '<li>';
					$content .= __( '<code>groups</code> The groups that the user is to be assigned to. If enabled on import, new groups are created.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
					$content .= '</li>';
					$content .= '</ol>';

					$content .= '<ul>';
					$content .= '<li>';
					$content .= __( 'The <code>user_email</code> column is required, all other columns are optional.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
					$content .= '</li>';
					$content .= '<li>';
					$content .= __( 'If no username is provided in the <code>user_login</code> column, the email address is taken as the username.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
					$content .= '</li>';
					$content .= '<li>';
					$content .= __( 'Empty columns are required if values in subsequent columns are provided; a single tab is considered as an empty column.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
					$content .= '</li>';
					$content .= '</ul>';

					$content .= '<p>';
					$content .= '<label style="width:95%">';
					$content .= sprintf( '<span id="file-example-toggle" style="display:block" title="%s">', __( 'Click to view an example', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ) );
					$content .= __( 'Example', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
					$content .= '</span>';
					$content .= '<textarea id="file-example" readonly="readonly" style="font-family: monospace; width: 100%; height:10em">';
					$content .= 'test@example.com';
					$content .= "\n";
					$content .= 'test1@example.com	test1					subscriber	Premium';
					$content .= "\n";
					$content .= 'test2@example.com	test2	John	Doe	http://www.example.com			Premium';
					$content .= "\n";
					$content .= 'test3@example.com		Sue	Doe			editor	Premium, Gold';
					$content .= '</textarea>';
					$content .= '</label>';
					$content .= '</p>';

					$content .= '<h2>';
					$content .= __( 'Column Order', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
					$content .= '</h2>';
					$content .= '<p>';
					$content .= __( 'The columns are assumed to be in this order: <code>user_email, user_login, first_name, last_name, user_url, user_pass, roles, groups</code>', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
					$content .= '</p>';
					$content .= '<p>';
					$content .= __( 'A different column order can be declared by starting a line with the <code>&#64;</code> symbol.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
					$content .= '</p>';

					$content .= '<p>';
					$content .= '<label style="width:95%">';
					$content .= sprintf( '<span id="file-example-toggle" style="display:block" title="%s">', __( 'Click to view an example', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ) );
					$content .= __( 'Example', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
					$content .= '</span>';
					$content .= '<textarea id="file-example" readonly="readonly" style="font-family: monospace; width: 100%; height:10em">';
					$content .= '@user_login user_email groups';
					$content .= "\n";
					$content .= 'joe	joe1@example.com	Premium';
					$content .= "\n";
					$content .= 'jill	jill.123@example.com	Premium, Privileged, Silver';
					$content .= "\n";
					$content .= 'Sue	mary.sue@example.com	Premium, Gold';
					$content .= '</textarea>';
					$content .= '</label>';
					$content .= '</p>';

// 					$content .= '<script type="text/javascript">';
// 					$content .= 'if (typeof jQuery !== "undefined") {';
// 					$content .= 'jQuery("#file-example").hide();';
// 					$content .= 'jQuery("#file-example-toggle").click(function(){';
// 					$content .= 'jQuery("#file-example").toggle();';
// 					$content .= '});';
// 					$content .= '}';
// 					$content .= '</script>';
					break;
				case 'groups_page_groups-user-export' :
					$id = 'export-users';
					$title = __( 'Export Users', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
					$content = '';
					$content .= '<h2>';
					$content .= __( 'File Format', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
					$content .= '</h2>';
					$content .= '<p>';
					$content .= __( 'Here a text file can be generated with values separated by tabs. A single line is created for each user.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
					$content .= '</p>';
			}
			if ( $id !== null ) {
				$screen->add_help_tab( array(
					'id'      => $id,
					'title'   => $title,
					'content' => $content
				) );
			}
		}
	}

	/**
	 * User import.
	 */
	public static function user_import() {
		if ( !current_user_can( 'manage_options' ) ) {
			wp_die( __( 'Access denied.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ) );
		}
		echo
			'<h2>' .
			__( 'Import Users', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ) .
			'</h2>';
		echo '<div class="groups-import-users">';
		include_once ( GROUPS_IMPORT_EXPORT_ADMIN_LIB . '/user-import.php' );
		echo '</div>';
	}

	/**
	 * User export.
	 */
	public static function user_export() {
		if ( !current_user_can( 'manage_options' ) ) {
			wp_die( __( 'Access denied.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ) );
		}
		echo
			'<h2>' .
			__( 'Export Users', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ) .
			'</h2>';
		echo '<div class="groups-export-users">';
		include_once ( GROUPS_IMPORT_EXPORT_ADMIN_LIB . '/user-export.php' );
		echo '</div>';
	}

	/**
	 * Adds plugin links.
	 *
	 * @param array $links
	 * @param array $links with additional links
	 */
	public static function admin_settings_link( $links ) {
		if ( current_user_can( 'manage_options' ) ) {
			$links[] = '<a href="' . get_admin_url( null, 'admin.php?page=groups-user-import' ) . '">' . __( 'Import Users', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ) . '</a>';
			$links[] = '<a href="' . get_admin_url( null, 'admin.php?page=groups-user-export' ) . '">' . __( 'Export Users', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ) . '</a>';
		}
		return $links;
	}

}
add_action( 'init', array( 'Groups_Import_Export_Admin', 'init' ) );
