<?php
/**
 * user-export.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is provided subject to the license granted.
 * Unauthorized use and distribution is prohibited.
 * See COPYRIGHT.txt and LICENSE.txt
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-import-export 1.0.0
 * @since 1.0.0
 */
?>

<?php
if ( !defined( 'ABSPATH' ) ) {
	die;
}

if ( !current_user_can( 'manage_options' ) ) {
	wp_die( __( 'Access denied.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ) );
}
?>

<div>
<form enctype="multipart/form-data" name="export-users" method="post" action="">
<div>

<?php
	echo '<p>';
	echo __( 'Export users to a text file &hellip;', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
	echo '</p>';
?>
<?php wp_nonce_field( 'export', 'groups-user-export', true, true ); ?>

<style type="text/css">
div.selectize-input { width: 95%; }
</style>

<?php
	global $wpdb;
	$groups_table = _groups_get_tablename( 'group' );
	$groups = $wpdb->get_results( "SELECT * FROM $groups_table ORDER BY name" );

	$selected_groups = !empty( $_POST['groups'] ) ? $_POST['groups'] : array();

	$min_id = isset( $_POST['min_id'] ) ? $_POST['min_id'] : '';
	if ( !empty( $min_id ) ) {
		$min_id = intval( $min_id );
	}
	$max_id = isset( $_POST['max_id'] ) ? $_POST['max_id'] : '';
	if ( !empty( $max_id ) ) {
		$max_id = intval( $max_id );
	}
	$min_user_id = $wpdb->get_var( "SELECT MIN(ID) FROM $wpdb->users" );
	$max_user_id = $wpdb->get_var( "SELECT MAX(ID) FROM $wpdb->users" );

	$include_user_meta = !empty( $_POST['include_user_meta'] );
	$meta_keys = !empty( $_POST['meta_keys'] ) ? $_POST['meta_keys'] : array();
	if ( !is_array( $meta_keys ) ) {
		$meta_keys = array( $meta_keys );
	}

	echo '<h3>';
	echo __( 'Groups', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
	echo '</h3>';

	echo '<p>';
	echo '<label>';
	echo __( 'Users in groups &hellip;', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
	echo ' ';
	printf(
		'<select id="assign-groups" class="groups" name="groups[]" multiple="multiple" placeholder="%s" data-placeholder="%s">',
		esc_attr( __( 'Choose groups &hellip;', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ) ) ,
		esc_attr( __( 'Choose groups &hellip;', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ) )
	);
	foreach( $groups as $group ) {
		$selected = is_array( $selected_groups ) && in_array( $group->name, $selected_groups );
		printf( '<option value="%s" %s>%s</option>', esc_attr( $group->name ), $selected ? ' selected="selected" ' : '', wp_filter_nohtml_kses( $group->name ) );
	}
	echo '</select>';
	echo '</label>';
	echo Groups_UIE::render_select( '#assign-groups' );
	echo '<p class="description">';
	echo __( 'If one ore more groups are chosen, only users in those groups are included. Otherwise all users are included.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
	echo '</p>';

	echo '<h3>';
	echo __( 'Range', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
	echo '</h3>';

	echo '<p>';
	echo '<label>';
	echo __( 'From user ID', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
	echo ' ';
	printf( '<input type="number" name="min_id" value="%s" placeholder="%s" />', $min_id, $min_user_id );
	echo '</label>';
	echo ' ';
	echo '<label>';
	echo __( 'Until user ID', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
	echo ' ';
	printf( '<input type="number" name="max_id" value="%s" placeholder="%s" />', $max_id, $max_user_id );
	echo '</label>';
	echo '</p>';

	echo '<h3>';
	echo __( 'Profile', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
	echo '</h3>';

	echo '<p>';
	echo '<label>';
	printf( '<input type="checkbox" name="include_user_meta" %s />', $include_user_meta ? ' checked="checked" ' : '' );
	echo ' ';
	echo __( 'Include extended user profile data', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
	echo '</label>';
	echo '</p>';

	$usermeta_keys = $wpdb->get_results( "SELECT DISTINCT meta_key FROM $wpdb->usermeta" );
	$keys = array();
	foreach( $usermeta_keys as $key ) {
		$keys[] = $key->meta_key;
	}
	sort( $keys );
	$size = min( 10, count( $keys ) );
	echo '<p>';
	printf(
		'<select id="user-meta-keys" class="meta-keys" name="meta_keys[]" multiple="multiple" placeholder="%s" data-placeholder="%s" size="%d">',
		__( 'None', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ),
		__( 'None', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ),
		$size
	);
	foreach( $keys as $key ) {
		printf(
			'<option value="%s" %s>%s</option>',
			esc_attr( $key ),
			empty( $meta_keys ) || in_array( $key, $meta_keys ) ? ' selected="selected" ' : '',
			esc_html( $key )
		);
	}
	echo '</select>';
	echo Groups_UIE::render_select( '#user-meta-keys' );
	echo '</p>';

	echo '<p class="description">';
	echo __( 'If extended user profile data is included, the corresponding meta data fields can be restricted. Otherwise, all fields are exported by default.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
	echo '</p>';
?>

<div class="buttons">
<p>
<input class="export button-primary" type="submit" name="submit" value="<?php echo __( 'Export', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ); ?>" />
</p>
<input type="hidden" name="action" value="export_users" />
</div>

</div>
</form>
</div>
