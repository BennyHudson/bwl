<?php
/**
 * user-import.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is provided subject to the license granted.
 * Unauthorized use and distribution is prohibited.
 * See COPYRIGHT.txt and LICENSE.txt
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-import-export 1.0.0
 * @since 1.0.0
 */
?>

<?php
if ( !defined( 'ABSPATH' ) ) {
	die;
}

if ( !current_user_can( 'manage_options' ) ) {
	wp_die( __( 'Access denied.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ) );
}
?>

<div>
<form enctype="multipart/form-data" name="import-users" method="post" action="">
<div>
<br/>
<?php
	echo '<p>';
	echo __( 'Import users from a text file &hellip;', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
	echo '</p>';
?>
<?php wp_nonce_field( 'import', 'groups-user-import', true, true ); ?>

<div class="buttons">

<p>
<label>
<?php _e( 'Import users from file', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ); ?> <input type="file" name="file" />
</label>
</p>

<style type="text/css">
div.selectize-input { width: 95%; }
</style>

<p>
<?php
	global $wpdb;
	$groups_table = _groups_get_tablename( 'group' );
	$groups = $wpdb->get_results( "SELECT * FROM $groups_table ORDER BY name" );

	$selected_groups = !empty( $_POST['groups'] ) ? $_POST['groups'] : array();

	echo '<label>';
	printf( __( 'Assign to groups', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ) );
	echo ' ';
	printf(
		'<select id="assign-groups" class="groups" name="groups[]" multiple="multiple" placeholder="%s" data-placeholder="%s">',
		esc_attr( __( 'Choose groups &hellip;', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ) ) ,
		esc_attr( __( 'Choose groups &hellip;', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ) )
	);
	foreach( $groups as $group ) {
		$selected = is_array( $selected_groups ) && in_array( $group->name, $selected_groups );
		printf( '<option value="%s" %s>%s</option>', esc_attr( $group->name ), $selected ? ' selected="selected" ' : '', wp_filter_nohtml_kses( $group->name ) );
	}
	echo '</select>';
	echo '</label>';
	echo Groups_UIE::render_select( '#assign-groups' );
?>
</p>

<p>
<label>
<input type="checkbox" name="test" value="1" <?php echo !empty( $_POST['test'] ) ? ' checked="checked" ' : '' ?>" /> <?php _e( 'Test only, no users are imported.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ); ?>
</label>
</p>

<p>
<label>
<input type="checkbox" name="suppress_warnings" value="1" <?php echo !empty( $_POST['suppress_warnings'] ) ? ' checked="checked" ' : '' ?>" /> <?php _e( 'Suppress warnings.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ); ?>
</label>
</p>

<p>
<label>
<input type="checkbox" name="stop_on_errors" value="1" <?php echo !empty( $_POST['stop_on_errors'] ) ? ' checked="checked" ' : '' ?>" /> <?php _e( 'Stop on errors.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ); ?>
</label>
</p>

<p>
<label>
<input type="checkbox" name="notify_users" value="1" <?php echo ( empty( $_POST['action'] ) || !empty( $_POST['notify_users'] ) ) ? ' checked="checked" ' : '' ?>" /> <?php _e( 'Send new users their password by email.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ); ?>
</label>
</p>

<p>
<label>
<input type="checkbox" name="create_groups" value="1" <?php echo !empty( $_POST['create_groups'] ) ? ' checked="checked" ' : '' ?>" /> <?php _e( 'Create new groups if they do not exist.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ); ?>
</label>
</p>

<p>
<label>
<?php
	$limit = isset( $_POST['limit'] ) ? intval( $_POST['limit'] ) : Groups_Import_Export::DEFAULT_LIMIT;
	if ( $limit <= 0 ) {
		$limit = Groups_Import_Export::DEFAULT_LIMIT;
	}
	printf( __( 'Import up to <input style="width:5em;text-align:right" type="text" name="limit" value="%d" /> users.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ), $limit );
?>
</label>
</p>

<p>
<input class="import button-primary" type="submit" name="submit" value="<?php echo __( 'Import', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ); ?>" />
</p>

<input type="hidden" name="action" value="import_users" />
</div>
</div>
</form>
</div>

<?php
	echo '<p>';
	echo __( 'An import can not be undone, when in doubt, run the import on a test installation first.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
	echo '</p>';

	echo '<p>';
	echo __( 'To import very large sets, several imports using the same file can be done. In this case, enabling <em>Suppress warnings</em> is useful to avoid being alerted about existing entries.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
	echo '</p>';

	echo '<p>';
	echo __( 'Please refer to the <em>Help</em> tab on this page for details on the file format.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
	echo '</p>';
