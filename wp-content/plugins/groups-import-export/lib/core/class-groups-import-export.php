<?php
/**
 * class-groups-import-export.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is provided subject to the license granted.
 * Unauthorized use and distribution is prohibited.
 * See COPYRIGHT.txt and LICENSE.txt
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * This header and all notices must be kept intact.
 * 
 * @author Karim Rahimpur
 * @package groups-import-export
 * @since 1.0.0
 */

/**
 * Plugin controller and booter.
 */
class Groups_Import_Export {

	public static $admin_messages = array();

	const DEFAULT_LIMIT = 100;

	/**
	 * Boots the plugin.
	 */
	public static function boot() {
		add_action( 'admin_notices', array( __CLASS__, 'admin_notices' ) );
		load_plugin_textdomain( GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN, null, 'groups-import-export/languages' );
		if ( self::check_dependencies() ) {
			if ( is_admin() ) {
				require_once( GROUPS_IMPORT_EXPORT_ADMIN_LIB . '/class-groups-import-export-admin.php' );
				require_once( GROUPS_IMPORT_EXPORT_CORE_LIB . '/class-groups-user-export.php' );
			}
			if ( self::is_user_import_request() ) {
				require_once( GROUPS_IMPORT_EXPORT_CORE_LIB . '/class-groups-user-import.php' );
			}
			if ( self::is_user_export_request() ) {
				require_once( GROUPS_IMPORT_EXPORT_CORE_LIB . '/class-groups-user-export.php' );
			}
			//register_activation_hook( GROUPS_IMPORT_EXPORT_PLUGIN_FILE, array( __CLASS__, 'activate' ) );
			//register_deactivation_hook( GROUPS_IMPORT_EXPORT_PLUGIN_FILE, array( __CLASS__, 'deactivate' ) );
			//add_action( 'init', array( __CLASS__, 'wp_init' ) );
		} else {
			self::$admin_messages[] = __( '<div class="error"><em>Groups Import Export</em> is an extension for <a href="http://www.itthinx.com/plugins/groups/">Groups</a> which is required, please install and activate <em>Groups</em>.</div>', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
		}
	}

	/**
	 * Returns true if the request is for a user import.
	 * 
	 * @return boolean
	 */
	public static function is_user_import_request() {
		return isset( $_REQUEST['groups-user-import'] ) && isset( $_REQUEST['action'] );
	}

	/**
	 * Returns true if the request is for a user export.
	 * 
	 * @return boolean
	 */
	public static function is_user_export_request() {
		return isset( $_REQUEST['groups-user-export'] ) && isset( $_REQUEST['action'] );
	}

	/**
	 * Init hook.
	 * 
	 * Not used.
	 */
	public static function wp_init() {
	}

	/**
	 * Activation hook.
	 * 
	 * Not used.
	 * 
	 * @param boolean $network_wide
	 */
	public static function activate( $network_wide = false ) {
	}

	/**
	 * Deactivation hook.
	 * 
	 * Not used.
	 * 
	 * @param boolean $network_wide
	 */
	public static function deactivate( $network_wide = false ) {
	}

	/**
	 * Checks if Groups is activated.
	 * @return true if Groups is there, false otherwise
	 */
	public static function check_dependencies() {
		$active_plugins = get_option( 'active_plugins', array() );
		if ( is_multisite() ) {
			$active_sitewide_plugins = get_site_option( 'active_sitewide_plugins', array() );
			$active_sitewide_plugins = array_keys( $active_sitewide_plugins );
			$active_plugins = array_merge( $active_plugins, $active_sitewide_plugins );
		}
		$groups_is_active = in_array( 'groups/groups.php', $active_plugins );
		define( 'GROUPS_IMPORT_EXPORT_GROUPS_IS_ACTIVE', $groups_is_active );
		return $groups_is_active;
	}

	/**
	 * Prints admin notices.
	 */
	public static function admin_notices() {
		if ( !empty( self::$admin_messages ) ) {
			foreach ( self::$admin_messages as $msg ) {
				echo $msg;
			}
		}
	}
}
Groups_Import_Export::boot();
