<?php
/**
 * class-groups-user-export.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is provided subject to the license granted.
 * Unauthorized use and distribution is prohibited.
 * See COPYRIGHT.txt and LICENSE.txt
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-import-export
 * @since 1.0.0
 */

/**
 * User export.
 */
class Groups_User_Export {

	/**
	 * Init hook to catch export file generation request.
	 */
	public static function init() {
		add_action( 'init', array( __CLASS__, 'wp_init' ) );
	}

	/**
	 * Catch request to generate export file.
	 */
	public static function wp_init() {
		if ( Groups_Import_Export::is_user_export_request() ) {
			if ( wp_verify_nonce( $_REQUEST['groups-user-export'], 'export' ) ) {
				self::export_users();
			}
		}
	}

	/**
	 * Renders the user export file.
	 */
	public static function export_users() {
		global $wpdb;
		if ( !headers_sent() ) {

			$group_ids         = array();
			$requested_groups  = !empty( $_REQUEST['groups'] ) ? $_REQUEST['groups'] : array();
			$min_id            = !empty( $_REQUEST['min_id'] ) ? intval( $_REQUEST['min_id'] ) : null;
			$max_id            = !empty( $_REQUEST['max_id'] ) ? intval( $_REQUEST['max_id'] ) : null;
			$include_user_meta = !empty( $_REQUEST['include_user_meta'] );
			$meta_keys         = !empty( $_REQUEST['meta_keys'] ) ? $_REQUEST['meta_keys'] : array();
			if ( !is_array( $meta_keys ) ) {
				$meta_keys = array( $meta_keys );
			}

			if ( is_array( $requested_groups ) ) {
				foreach( $requested_groups as $name ) {
					if ( $group = Groups_Group::read_by_name( $name ) ) {
						$group_ids[] = $group->group_id;
					}
				}
			}

			$user_ids = array();
			$filters  = array( '1=%d' );
			$params   = array( 1 );
			if ( !empty( $min_id ) ) {
				$filters[] = 'user_id >= %d';
				$params[] = $min_id;
			}
			if ( !empty( $max_id ) ) {
				$filters[] = 'user_id <= %d';
				$params[] = $max_id;
			}
			if ( !empty( $group_ids ) ) {
				$filters[] = "group_id IN (" . implode( ',', $group_ids ) . ')';
			}
			$user_group_table = _groups_get_tablename( 'user_group' );
			$results = $wpdb->get_results( $wpdb->prepare( "SELECT DISTINCT user_id FROM $user_group_table WHERE " . implode( ' AND ', $filters ), $params ) );
			foreach( $results as $result ) {
				$user_ids[] = $result->user_id;
			}

			$charset = get_bloginfo( 'charset' );
			$now     = date( 'Y-m-d-H-i-s', time() );
			header( 'Content-Description: File Transfer' );
			if ( !empty( $charset ) ) {
				header( 'Content-Type: text/plain; charset=' . $charset );
			} else {
				header( 'Content-Type: text/plain' );
			}
			header( "Content-Disposition: attachment; filename=\"groups-users-$now.txt\"" );

			foreach( $user_ids as $user_id ) {
				if ( $user = get_user_by( 'id', $user_id ) ) {
					$user_email = $user->user_email;
					$user_login = $user->user_login;
					$first_name = get_user_meta( $user_id, 'first_name', true );
					$last_name = get_user_meta( $user_id, 'last_name', true );
					$user_url = $user->user_url;
					$user_pass = '';
					$roles      = $user->roles;
					if ( !empty( $roles ) && is_array( $roles ) ) {
						$roles = implode( ',', $roles );
					}
					$gu = new Groups_User( $user_id );
					$groups = array();
					foreach( $gu->groups as $group ) {
						$groups[] = $group->name;
					}
					$groups = implode( ',', $groups );
					printf( "$user_email\t$user_login\t$first_name\t$last_name\t$user_url\t$user_pass\t$roles\t$groups" );
					if ( $include_user_meta ) {
						$user_meta = get_user_meta( $user_id );
						if ( !empty( $meta_keys ) ) {
							$filtered_user_meta = array();
							foreach( $user_meta as $key => $value ) {
								if ( in_array( $key, $meta_keys ) ) {
									// Un-array the value, as we have all meta data from
									// get_user_meta(...), its $single attribute is without
									// effect.
									if ( is_array( $value ) ) {
										$value = array_shift( $value );
									}
									$filtered_user_meta[$key] = $value;
								}
							}
							$user_meta = $filtered_user_meta;
						}
						if ( !empty( $meta_keys ) ) {
							printf( "\t%s", json_encode( $user_meta ) );
						}
					}
					printf( "\n" );
				}
			}
			die;
		} else {
			wp_die( 'ERROR: headers already sent' );
		}
	}
}
Groups_User_Export::init();
