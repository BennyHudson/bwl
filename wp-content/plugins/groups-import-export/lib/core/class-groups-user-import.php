<?php
/**
 * class-groups-user-import.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is provided subject to the license granted.
 * Unauthorized use and distribution is prohibited.
 * See COPYRIGHT.txt and LICENSE.txt
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-import-export
 * @since 1.0.0
 */

/**
 * User import.
 */
class Groups_User_Import {

	const MAX_FGET_LENGTH = 1024;
	const BASE_DELTA = 1048576;
	const DELTA_F    = 1.62;

	private static $admin_messages = array();

	private static $fields = array(
		'user_email',
		'user_login',
		'first_name',
		'last_name',
		'user_url',
		'user_pass',
		'roles',
		'groups',
		'meta'
	);

	private static $notify_users = true;

	/**
	 * Init hook to catch import file generation request.
	 */
	public static function init() {
		add_action( 'init', array( __CLASS__, 'wp_init' ) );
		add_action( 'admin_notices', array( __CLASS__, 'admin_notices' ) );
	}

	/**
	 * Prints admin notices.
	 */
	public static function admin_notices() {
		if ( !empty( self::$admin_messages ) ) {
			echo '<div style="padding:1em;margin:1em;border:1px solid #aa0;border-radius:4px;background-color:#ffe;color:#333;">';
			foreach ( self::$admin_messages as $msg ) {
				echo '<p>';
				echo $msg;
				echo '</p>';
			}
			echo '</div>';
		}
	}

	/**
	 * Catch request to generate import file.
	 */
	public static function wp_init() {
		
		if ( Groups_Import_Export::is_user_import_request() ) {
			if ( wp_verify_nonce( $_REQUEST['groups-user-import'], 'import' ) ) {
				if ( $_REQUEST['action'] == 'import_users' ) {
					self::import_users( !empty( $_REQUEST['test'] ) );
				}
			} 
		}
	}

	/**
	 * Import from uploaded file.
	 * 
	 * @param boolean $test if true, no users are imported; defaults to false
	 * @return int number of records created
	 */
	private static function import_users( $test = false ) {

		global $wpdb;

		$charset = get_bloginfo( 'charset' );
		$now     = date( 'Y-m-d H:i:s', time() );

		$memory_limit = ini_get( 'memory_limit' );
		preg_match( '/([0-9]+)(.)/', $memory_limit, $matches );
		if ( isset( $matches[2] ) ) {
			$exp = array( 'K' => 1, 'M' => 2, 'G' => 3, 'T' => 4, 'P' => 5, 'E' => 6 );
			if ( key_exists( $matches[2], $exp ) ) {
				$memory_limit *= pow( 1024, $exp[$matches[2]] );
			}
		}

		$bytes              = memory_get_usage( true );
		$max_execution_time = ini_get( 'max_execution_time' );
		if ( function_exists( 'getrusage' ) ) {
			$resource_usage = getrusage();
			if ( isset( $resource_usage['ru_utime.tv_sec'] ) ) {
				$initial_execution_time = $resource_usage['ru_stime.tv_sec'] + $resource_usage['ru_utime.tv_sec'] + 2; // add 2 as top value for the sum of ru_stime.tv_usec and ru_utime.tv_usec
			}
		}

		$assign_groups = array();
		$requested_groups = !empty( $_REQUEST['groups'] ) ? $_REQUEST['groups'] : array();
		$create_groups    = !empty( $_REQUEST['create_groups'] );

		if ( is_array( $requested_groups ) ) {
			foreach( $requested_groups as $name ) {
				$name = trim( $name );
				if ( ( $group = Groups_Group::read_by_name( $name ) ) || $create_groups ) {
					if ( $group ) {
						$assign_groups[] = $group->name;
					} else {
						$assign_groups[] = $name;
					}
				}
			}
		}

		self::$notify_users = !empty( $_REQUEST['notify_users'] );

		if ( isset( $_FILES['file'] ) ) {
			if ( $_FILES['file']['error'] == UPLOAD_ERR_OK ) {
				$tmp_name = $_FILES['file']['tmp_name'];
				if ( file_exists( $tmp_name ) ) {
					if ( $h = @fopen( $tmp_name, 'r' ) ) {

						$imported           = 0;
						$valid              = 0;
						$invalid            = 0;
						$skipped            = 0;
						$line_number        = 0;
						$stop_on_errors     = !empty( $_REQUEST['stop_on_errors'] );
						$suppress_warnings  = !empty( $_REQUEST['suppress_warnings'] );
						$errors             = 0;
						$warnings           = 0;
						$limit              = !empty( $_REQUEST['limit'] ) ? intval( $_REQUEST['limit'] ) : Groups_Import_Export::DEFAULT_LIMIT;
						if ( $limit <= 0 ) {
							$limit = Groups_Import_Export::DEFAULT_LIMIT;
						}

						$fields = self::$fields;
						while( !feof( $h ) ) {

							$line  = '';
							$chunk = '';
							while( ( $chunk = fgets( $h, self::MAX_FGET_LENGTH ) ) !== false ) {
								$line .= $chunk;
								if ( ( strpos( $chunk, "\n" ) !== false ) || feof( $h ) ) {
									break;
								}
							}
							if ( strlen( $line ) == 0 ) {
								break;
							}

							$line_number++;

							$skip = false;
							$line = preg_replace( '/\r|\n/', '', $line );
							$line = trim( $line );

							// skip comment and empty lines
							if ( strpos( $line, '#' ) === 0 ) {
								continue;
							}
							if ( strlen( $line ) === 0 ) {
								continue;
							}

							// set fields and check for column indicators
							if ( strpos( $line, '@' ) === 0 ) {
								// reset?
								if ( $line == '@' ) {
									$fields = self::$fields;
									self::$admin_messages[] = sprintf( __( 'Column declaration reset on line %d: <code>%s</code>', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ), $line_number, esc_html( implode( ', ', $fields ) ) );
								} else {
									preg_match_all( '/[a-zA-Z_]+/', $line, $matches );
									if ( isset( $matches[0] ) && is_array( $matches[0] ) ) {
										$fields = array();
										foreach( $matches[0] as $field ) {
											if ( in_array( $field, self::$fields ) ) {
												$fields[] = $field;
											}
										}
										self::$admin_messages[] = sprintf( __( 'Column declaration found on line %d, the following column order is assumed: <code>%s</code>', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ), $line_number, esc_html( implode( ', ', $fields ) ) );
									}
								}
								continue;
							}

							// data values
							$data = explode( "\t", $line );
							$userdata = array();
							foreach( $fields as $i => $field ) {
								if ( isset( $data[$i] ) ) {
									$value = trim( $data[$i] );
									$userdata[$field] = $value;
								}
							}
							if ( empty( $userdata['user_pass'] ) ) {
								$userdata['user_pass'] = wp_generate_password();
							}

							// groups
							if ( !empty( $assign_groups ) ) {
								if ( !empty( $userdata['groups'] ) ) {
									$userdata['groups'] = implode( ',', array_unique( array_merge( explode( ',', $userdata['groups'] ), $assign_groups ) ) );
								} else {
									$userdata['groups'] = implode( ',', $assign_groups );
								}
							}

							// email checks
							if ( empty( $userdata['user_email'] ) ) {
								self::$admin_messages[] = sprintf( __( 'Error on line %d, missing email address.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ), $line_number );
								$errors++;
								$skip = true;
							} else if ( !is_email( $userdata['user_email'] ) ) {
								self::$admin_messages[] = sprintf( __( 'Error on line %d, <code>%s</code> is not a valid email address.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ), $line_number, esc_html( $userdata['user_email'] ) );
								$errors++;
								$skip = true;
							} else if ( get_user_by( 'email', $userdata['user_email'] ) ) {
								if ( !$suppress_warnings ) {
									self::$admin_messages[] = sprintf( __( 'Warning on line %d, a user with the email address <code>%s</code> already exists.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ), $line_number, esc_html( $userdata['user_email'] ) );
								}
								$warnings++;
								$skip = true;
							}

							// username check
							if ( empty( $userdata['user_login'] ) && !empty( $userdata['user_email'] ) ) {
								$userdata['user_login'] = $userdata['user_email'];
							}
							if ( !empty( $userdata['user_login'] ) && get_user_by( 'login', $userdata['user_login'] ) ) {
								if ( !$suppress_warnings ) {
									self::$admin_messages[] = sprintf( __( 'Warning on line %d, the username <code>%s</code> already exists.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ), $line_number, esc_html( $userdata['user_login'] ) );
								}
								$warnings++;
								$skip = true;
							}

							// import or skip
							if ( !$skip ) {
								if ( !empty( $userdata['user_login'] ) && !empty( $userdata['user_email'] ) ) {
									$valid++;
									if ( !$test ) {
										if ( self::insert_user( $userdata ) ) {
											$imported++;
											if ( $imported >= $limit ) {
												break;
											}
										}
									}
								}
							} else {
								$skipped++;
							}
							if ( $stop_on_errors && ( $errors > 0 ) ) {
								break;
							}

							// memory guard
							if ( is_numeric( $memory_limit ) ) {
								$old_bytes = $bytes;
								$bytes     = memory_get_usage( true );
								$remaining = $memory_limit - $bytes;
								$delta = self::BASE_DELTA;
								if ( $bytes > $old_bytes ) {
									$delta += intval( ( $bytes - $old_bytes ) * self::DELTA_F );
								}
								if ( $remaining < $delta ) {
									self::$admin_messages[] = sprintf( __( 'Error, stopped after line %d due to low memory.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ), $line_number );
									break;
								}
							}

							// time guard
							if ( function_exists( 'getrusage' ) ) {
								$resource_usage = getrusage();
								if ( isset( $resource_usage['ru_utime.tv_sec'] ) ) {
									$execution_time = $resource_usage['ru_stime.tv_sec'] + $resource_usage['ru_utime.tv_sec'] + 2; // add 2 as top value for the sum of ru_stime.tv_usec and ru_utime.tv_usec
									$d = ceil( $execution_time - $initial_execution_time ) / $line_number;
									if ( intval( $d * self::DELTA_F ) > ( $max_execution_time - $execution_time ) ) {
										self::$admin_messages[] = sprintf( __( 'Error, stopped after line %d as the maximum execution time for PHP is about to be reached.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ), $line_number );
										break;
									}
								}
							}
						}
						@fclose( $h );

						self::$admin_messages[] = sprintf( _n( '1 valid entry has been read.', '%d valid entries have been read.', $valid, GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ), $valid );
						self::$admin_messages[] = sprintf( _n( '1 entry has been skipped.', '%d entries have been skipped.', $skipped, GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ), $skipped );
						self::$admin_messages[] = sprintf( _n( '1 user has been imported.', '%d users have been imported.', $imported, GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ), $imported );

					} else {
						self::$admin_messages[] = __( 'Import failed (error opening temporary file).', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
					}
				}
			} else if ( $_FILES['file']['error'] == UPLOAD_ERR_NO_FILE ) {
				self::$admin_messages[] = __( 'Please choose a file to import from.', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN );
			}
		}

	}

	/**
	 * Insert a new user entry.
	 * 
	 * @param array $userdata
	 * @return user ID if successul, otherwise null
	 */
	private static function insert_user( $userdata = array() ) {

		$result = null;

		$_userdata = array(
			'user_login' => esc_sql( $userdata['user_login'] ),
			'user_email' => esc_sql( $userdata['user_email'] ),
			'user_pass'  => esc_sql( $userdata['user_pass'] )
		);
		if ( !empty( $userdata['first_name'] ) ) {
			$_userdata['first_name'] = esc_sql( $userdata['first_name'] );
		}
		if ( !empty( $userdata['last_name'] ) ) {
			$_userdata['last_name'] = esc_sql( $userdata['last_name'] );
		}
		if ( !empty( $userdata['user_url'] ) ) {
			$_userdata['user_url'] = esc_sql( $userdata['user_url'] );
		}

		if ( !empty( $userdata['roles'] ) ) {
			// Figure out the role with the widest range of capabilities
			// so that it is assigned first and thus shown in the user
			// profile. This is to avoid that we get users that have two or
			// more roles and that the most capabable role is hidden by
			// a lesser one (e.g. subscriber and administrator) - as that
			// would pose a security risk, the administrator role would
			// be hidden by the subscriber role in the admin back end.
			$roles = array_map( 'trim', explode( ',', $userdata['roles'] ) );
			usort( $roles, array( __CLASS__, 'inv_cmp_role_caps' ) );
			$wp_roles = new WP_Roles();
			if ( $wp_roles->is_role( $roles[0] ) ) {
				$_userdata['role'] = $roles[0];
			}
		}

		$user_id = wp_insert_user( $_userdata );
		if ( !is_wp_error( $user_id ) ) {
			$result = $user_id;
			if ( !empty( $userdata['roles'] ) ) {
				$user = new WP_User( $user_id );
				if ( !empty( $roles ) ) {
					$wp_roles = new WP_Roles();
					foreach( $roles as $role ) {
						if ( $wp_roles->is_role( $role ) ) {
							if ( !$user->has_cap( $role ) ) {
								$user->add_role( $role );
							}
						}
					}
				}
			}
			if ( !empty( $userdata['groups'] ) ) {
				$create_groups = !empty( $_REQUEST['create_groups'] );
				$groups = explode( ',', $userdata['groups'] );
				foreach( $groups as $group ) {
					$group = trim( $group );
					if ( ( $g = Groups_Group::read_by_name( $group ) ) || $create_groups ) {
						if ( !$g ) {
							if ( $group_id = Groups_Group::create( array( 'name' => $group ) ) ) {
								$g = Groups_Group::read( $group_id );
							}
						}
						if ( $g ) {
							Groups_User_Group::create( array( 'group_id' => $g->group_id, 'user_id' => $user_id ) );
						}
					}
				}
			}
			if ( !empty( $userdata['meta'] ) ) {
				$user_meta = json_decode( $userdata['meta'], true );
				if ( is_array( $user_meta ) ) {
					foreach( $user_meta as $meta_key => $meta_value ) {
						add_user_meta( $user_id, $meta_key, maybe_unserialize( $meta_value ) );
					}
				}
			}
			if ( self::$notify_users ) {
				self::new_user_notification( $user_id, $userdata['user_pass'] );
			}
		}
		return $result;
	}

	/**
	 * Notifies a user.
	 * 
	 * @param int $user_id User ID
	 * @param string $plaintext_pass
	 */
	public static function new_user_notification( $user_id, $plaintext_pass = '' ) {
		$user = get_userdata( $user_id );
		$blogname = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );
		if ( !empty( $plaintext_pass ) ) {
			$message  = sprintf( __( 'Username: %s', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ), $user->user_login) . "\r\n";
			$message .= sprintf( __( 'Password: %s', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ), $plaintext_pass ) . "\r\n";
			$message .= wp_login_url() . "\r\n";
			@wp_mail(
				$user->user_email,
				apply_filters(
					'groups_import_export_new_user_registration_subject',
					sprintf( __( '[%s] Your username and password', GROUPS_IMPORT_EXPORT_PLUGIN_DOMAIN ), $blogname ),
					$user_id,
					$plaintext_pass
				),
				apply_filters(
					'groups_import_export_new_user_registration_message',
					$message,
					$user_id,
					$plaintext_pass
				)
			);
		}
	}

	/**
	 * Comparison helper function to compare roles by their number of
	 * capabilities.
	 * 
	 * @param string $r1
	 * @param string $r2
	 * @return int
	 */
	public static function inv_cmp_role_caps( $r1, $r2 ) {
		$n_1 = 0;
		$n_2 = 0;
		if ( $role_1 = get_role( $r1 ) ) {
			$n_1 = count( $role_1->capabilities );
		}
		if ( $role_2 = get_role( $r2 ) ) {
			$n_2 = count( $role_2->capabilities );
		}
		return  $n_2 - $n_1;
	}

}
Groups_User_Import::init();
