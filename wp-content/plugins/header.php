<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/modernizr-latest.js" type="text/javascript"></script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5089c16f5cedcc91"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width"/>  
<title><?php wp_title('|'); ?></title>
<?php wp_head(); ?>
</head>
<body>
	<div class="lightbox-overlay">
        <div class="lightbox-bg" id="login">
            <div class="lightbox-container">
                <section class="bwl-login">
                    <a href="#" id="login-close"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/lightbox-close.png" alt="close"></a>
                    <ul id="login-forms">
                    	<?php dynamic_sidebar( 'login-area' ); ?>
                    </ul>
                    <ul id="login-options">
                    	<li><a href="" data-slide-index="0">Already a member? Login here</a></li>
                        <li><a href="" data-slide-index="1">Not a member? Register here</a></li>
                    </ul>
                </section>
            </div>
        </div>
    </div>
    <section id="footer-fix">
    	<section id="page-content">
        	<header>
            	<section class="header-meta">
                	<section class="container">
                    	<aside>
                        	<ul>
                        		<li><a href="#" id="login-trigger">Login</a></li>
                                <li><a href="#">About British Weight Lifting</a></li>
                                <li><a href="#">Sponsors</a></li>
                                <li><a href="#">Contact Us</a></li>
                            </ul>
                        </aside>
                        <aside class="header-search">
                        	<a href="#"><i class="fa fa-twitter fa-2x"></i></a><a href="#"><i class="fa fa-facebook fa-2x"></i></a><a href="#"><i class="fa fa-youtube fa-2x"></i></a><?php dynamic_sidebar( 'header-search' ); ?>
                        </aside>	
                    </section>
                </section>
                <section class="header-main">
                	<section class="container">
                    	<aside id="logo">
                        	<a href="<?php bloginfo('url'); ?>">
								<?php echo is_front_page() ? '<h1 id="wf-logo">' : '<h2 id="wf-logo">'; ?><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt="<?php wp_title('|'); ?>"><?php echo is_front_page() ? '</h1>' : '</h2>'; ?>
                            </a>
                        </aside>
                        <aside id="header-ad">
                        	<?php dynamic_sidebar( 'header-adverts' ); ?>		
                        </aside>	
                    </section>
                </section>
                <nav>
                	<section class="container">
                    	<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>	
                    </section>
                </nav>
            </header>