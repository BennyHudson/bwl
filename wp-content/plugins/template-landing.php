<?php 
	// Template Name: Landing Page
?>
<?php get_header(); ?>
	<section class="container">
    	<aside class="home-slider">
        	<ul id="home-slider">
            	<?php $postslist = get_posts('numberposts=5');
					foreach ($postslist as $post) :
					setup_postdata($post);
				?>
                	<li>
                    	<aside class="slide-content">
                        	<h2><?php the_title(); ?></h2>
                            <section class="title-clip">
                            	<?php the_excerpt(); ?>
                            </section>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="read-more">Read more</a>
                        </aside>
                        <aside class="slide-image">
                        	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('thumbnail'); ?></a>
                        </aside>
                    </li>
				<?php endforeach ?>
            </ul>	
        </aside>
        <aside class="home-sidebar">
        	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        	<?php if(get_field('home_sidebar')) : ?>
            	<?php while(the_repeater_field('home_sidebar')) : ?>
                	<a href="<?php the_sub_field('page_link'); ?>">
                    	<img src="<?php the_sub_field('image'); ?>">
                    </a>
                <?php endwhile; ?>
            <?php endif; ?>
        </aside>
        <section class="home-split">
        	<?php if(get_field('footer_ad_zones')) : ?>
            	<?php while(the_repeater_field('footer_ad_zones')) : ?>
                	<aside>
                        <a href="<?php the_sub_field('page_link'); ?>">
                            <img src="<?php the_sub_field('image'); ?>">
                        </a>
                    </aside>
                <?php endwhile; ?>
            <?php endif; ?>
        </section>
        <section class="home-content">
        	<?php dynamic_sidebar( 'home-widgets' ); ?>
        </section>
        <section class="home-split">
        	<?php if(get_field('extra_home_ad_zones')) : ?>
            	<?php while(the_repeater_field('extra_home_ad_zones')) : ?>
                	<aside>
                        <a href="<?php the_sub_field('page_link'); ?>">
                            <img src="<?php the_sub_field('image'); ?>">
                        </a>
                    </aside>
                <?php endwhile; ?>
            <?php endif; ?>
		<?php endwhile; else: ?>
            <?php get_template_part('partials/template', 'error'); ?>
        <?php endif; ?>
        </section>
    </section>
<?php get_footer(); ?>
<!--
<div id="shailan-countdown-2_1" class="shailan-countdown-2 countdown hasCountdown">
<span class="countdown_row countdown_show3">
	<span class="countdown_section">
    	<span class="countdown_amount">31</span>
        <br>Days
    </span>
    <span class="countdown_section">
    	<span class="countdown_amount">13</span>
        <br>Hours
    </span>
    <span class="countdown_section">
    	<span class="countdown_amount">9</span>
        <br>Minutes
    </span></span>
    <span class="countdown_row countdown_descr">2014 British Senior Weightlifting and Para-Powerlifting Championships</span>
</div>
-->