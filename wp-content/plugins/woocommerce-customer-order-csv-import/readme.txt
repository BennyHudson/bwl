=== WooCommerce Customer/Order/Coupon CSV Import ===
Author: skyverge
Tags: woocommerce
Requires at least: 3.8
Requires WooCommerce at least: 2.1

== Installation ==

1. Upload the entire 'woocommerce-customer-order-csv-import' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
