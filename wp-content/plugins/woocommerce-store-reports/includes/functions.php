<?php
if( is_admin() ) {

	/* Start of: WordPress Administration */

	// Add Store Reports to WordPress Administration menu
	function woo_sr_admin_menu() {

		add_submenu_page( 'woocommerce', __( 'Store Reports for WooCommerce', 'woo_sr' ), __( 'Store Reports', 'woo_sr' ), 'manage_woocommerce', 'woo_sr', 'woo_sr_html_page' );

	}
	add_action( 'admin_menu', 'woo_sr_admin_menu' );

	// Display admin notice on screen load
	function woo_sr_admin_notice( $message = '', $priority = 'updated', $screen = '' ) {

		if( empty( $priority ) )
			$priority = 'updated';
		if( !empty( $message ) )
			add_action( 'admin_notices', woo_sr_admin_notice_html( $message, $priority, $screen ) );

	}

	// HTML template for admin notice
	function woo_sr_admin_notice_html( $message = '', $priority = 'updated', $screen = '' ) {

		// Display admin notice on specific screen
		if( !empty( $screen ) ) {
			global $pagenow;
			if( $pagenow <> $screen )
				return;
		} ?>
<div id="message" class="<?php echo $priority; ?>">
	<p><?php echo $message; ?></p>
</div>
<?php

	}

	function woo_sr_default_screen() {

		woo_sr_template_header( __( 'Store Reports', 'woo_sr' ) );
		$reports = woo_sr_get_reports();
		if( $reports ) {
			if( file_exists( WOO_SR_PATH . 'templates/admin/woo-sr_admin_reports.php' ) ) {
				include_once( WOO_SR_PATH . 'templates/admin/woo-sr_admin_reports.php' );
			} else {
				$troubleshooting_url = 'http://www.visser.com.au/documentation/store-reports/';
				$message = '<strong>' . __( 'No reports dashboard template could be found, please re-install Store Reports for WooCommerce.', 'woo_sr' ) . '</strong> <a href="' . $troubleshooting_url . '" target="_blank">' . __( 'Need help?', 'woo_sr' ) . '</a>';
				woo_sr_admin_notice( $message, 'error' );
			}
		}
		woo_sr_template_footer();

	}

	function woo_sr_get_reports() {

		$reports = array();
		$reports[] = array(
			'name' => 'count_sheet',
			'title' => __( 'Count Sheet', 'woo_sr' ),
			'description' => __( 'Track physical inventory within your store.', 'woo_sr' ),
			'url' => admin_url( 'admin.php?page=woo_sr&action=view&report=count_sheet' )
		);
		$reports[] = array(
			'name' => 'product_orders',
			'title' => __( 'Product Orders', 'woo_sr' ),
			'description' => __( 'A drill down of Orders linked to individual Products.', 'woo_sr' ),
			'url' => admin_url( 'admin.php?page=woo_sr&action=view&report=product_orders' )
		);
		$reports[] = array(
			'name' => 'product_customers',
			'title' => __( 'Product Customers', 'woo_sr' ),
			'description' => __( 'A drill down of Products linked to customers by Billing E-mail.', 'woo_sr' ),
			'url' => admin_url( 'admin.php?page=woo_sr&action=view&report=product_customers' )
		);
/*
		$reports[] = array(
			'name' => 'customers',
			'title' => __( 'Customers', 'woo_sr' ),
			'description' => __( 'A summary of sale volume and earnings per store customer.', 'woo_sr' ),
			'url' => admin_url( 'index.php?page=woo_sr&action=view&report=customers' )
		);
		$reports[] = array(
			'name' => 'products_sold',
			'title' => __( 'Products Sold', 'woo_sr' ),
			'description' => __( 'A summary of sale volume and earnings within that date period.', 'woo_sr' ),
			'url' => admin_url( 'index.php?page=woo_sr&action=view&report=products_sold' )
		);
		$reports[] = array(
			'name' => 'payments',
			'title' => __( 'Payments', 'woo_sr' ),
			'description' => __( 'A summary of sale volume and earnings per payment method.', 'woo_sr' ),
			'url' => admin_url( 'index.php?page=woo_sr&action=view&report=payments' )
		);
		$reports[] = array(
			'name' => 'sales',
			'title' => __( 'Monthly Sales', 'woo_sr' ),
			'description' => __( 'A summary of sale volume and earnings per day within date period.', 'woo_sr' ),
			'url' => admin_url( 'index.php?page=woo_sr&action=view&report=sales' )
		);
		$reports[] = array(
			'name' => 'coupons',
			'title' => __( 'Coupons', 'woo_sr' ),
			'description' => __( 'A summary of store-wide coupon usage.', 'woo_sr' ),
			'url' => admin_url( 'index.php?page=woo_sr&action=view&report=coupons' )
		);
		$reports[] = array(
			'name' => 'no_stock',
			'title' => __( 'Out of Stock', 'woo_sr' ),
			'description' => __( 'A summary of out of stock Products.', 'woo_sr' ),
			'url' => admin_url( 'index.php?page=woo_sr&action=view&report=no_stock' )
		);
		$reports[] = array(
			'name' => 'low_stock',
			'title' => __( 'Low Stock', 'woo_sr' ),
			'description' => __( 'A summary of low stocked Products.', 'woo_sr' ),
			'url' => admin_url( 'index.php?page=woo_sr&action=view&report=low_stock' )
		);
		$reports[] = array(
			'name' => 'top_earners',
			'title' => __( 'Top Earners', 'woo_sr' ),
			'description' => __( 'A summary of top earning Products based on Product cost and total earnings.', 'woo_sr' ),
			'url' => admin_url( 'index.php?page=woo_sr&action=view&report=top_earners' )
		);
		$reports[] = array(
			'name' => 'bestsellers',
			'title' => __( 'Bestsellers', 'woo_sr' ),
			'description' => __( 'A summary of top selling Products based on unit sales.', 'woo_sr' ),
			'url' => admin_url( 'index.php?page=woo_sr&action=view&report=bestsellers' )
		);
		$reports[] = array(
			'name' => 'rate_of_sales',
			'title' => __( 'Rate of Sales', 'woo_sr' ),
			'description' => __( 'See how many of each Product is selling per week. Remove any days that the product was out of stock.', 'woo_sr' ),
			'url' => admin_url( 'index.php?page=woo_sr&action=view&report=rate_of_sales' )	
		);
*/
		return $reports;

	}

	function woo_sr_get_product( $product_id = 0 ) {

		if( $product_id ) {
			if( $post = get_post( $product_id ) ) {
				$product = (object)array(
					'name' => $post->post_title,
					'quantity_sold' => (int)get_post_meta( $product_id, 'total_sales', true )
				);
				return $product;
			}
		}

	}

	function woo_sr_get_products( $args = array() ) {

		// Set defaults
		$meta_key = null;
		$do_search = false;

		if( $args ) {
			$s = strtolower( $args['search'] );
			if( !empty( $s ) )
				$do_search = true;
			if( $args['orderby'] ) {
				$orderby = $args['orderby'];
				// Use different rules for common filters
				if( $orderby == 'stock' ) {
					$orderby = 'meta_value_num';
					$meta_key = '_stock';
				} else if( $orderby == 'sku' ) {
					$orderby = 'meta_value';
					$meta_key = '_sku';
				} else if( $orderby == 'name' ) {
					$orderby = 'title';
				} else if( $orderby == 'qty_sold' ) {
					$orderby = 'meta_value_num';
					$meta_key = 'total_sales';
				}
			}
			if( $args['order'] )
				$order = $args['order'];
		}
		$manage_stock = get_option( 'woocommerce_manage_stock', 'yes' );
		$post_type = 'product';
		$args = array(
			'post_type' => $post_type,
			'numberposts' => -1,
			'orderby' => $orderby,
			'order' => $order,
			'meta_key' => $meta_key
		);
		if( $products = get_posts( $args ) ) {
			foreach( $products as $key => $product ) {
				$products[$key]->sku = get_post_meta( $product->ID, '_sku', true );
				$products[$key]->name = get_the_title( $product->ID );
				$products[$key]->regular_price = get_post_meta( $product->ID, '_regular_price', true );
				$products[$key]->price = get_post_meta( $product->ID, '_price', true );
				if( !empty( $products[$key]->regular_price ) && ( $products[$key]->regular_price <> $products[$key]->price ) )
					$products[$key]->price = $products[$key]->regular_price;
				$products[$key]->quantity_sold = (int)get_post_meta( $product->ID, 'total_sales', true );
				$products[$key]->manage_stock = get_post_meta( $product->ID, '_manage_stock', true );
				$products[$key]->stock = '';
				if( $manage_stock == 'yes' && $products[$key]->manage_stock == 'yes' )
					$products[$key]->stock = (int)get_post_meta( $product->ID, '_stock', true );
				$products[$key]->edit_url = get_edit_post_link( $product->ID );
				if( $do_search ) {
					if( ( strpos( strtolower( $products[$key]->name ), $s ) === false ) && strpos( strtolower( $products[$key]->sku ), $s ) === false )
						unset( $products[$key] );
				}
			}
		}
		return $products;

	}

	function woo_sr_get_product_customers( $product_id = 0 ) {

		if( $product_id ) {
			if( $orders = woo_sr_get_product_orders( $product_id, 'ID' ) ) {
				$customers = array();
				foreach( $orders as $key => $order ) {
					$orders[$key] = new WC_Order( $order->ID );
					// Check if e-mail has already been added to customers list
					if( !isset( $customers[$orders[$key]->billing_email] ) ) {
						if( !empty( $orders[$key]->billing_email ) ) {
							$orders[$key]->invoices = woo_sr_get_customer_orders_count( $orders[$key]->billing_email, 'count' );
							$customers[$orders[$key]->billing_email] = $orders[$key];
						}
					}
				}
				return $customers;
			}
		}

	}

	function woo_sr_get_product_orders( $product_id = 0 ) {

		global $wpdb;

		$orders = false;
		if( $product_id ) {
			$ids_sql = $wpdb->prepare( "SELECT order_items.order_id FROM `" . $wpdb->prefix . "woocommerce_order_items` AS order_items, `" . $wpdb->prefix . "woocommerce_order_itemmeta` AS order_itemmeta WHERE order_items.order_item_id = order_itemmeta.order_item_id AND order_itemmeta.meta_key = '_product_id' AND order_itemmeta.meta_value = %d", $product_id );
			if( $ids = $wpdb->get_col( $ids_sql ) ) {
				$ids = implode( ",", $ids );
				$ids = substr( $ids, 0, -1 );
				$post_type = 'shop_order';
				$args = array(
					'post_type' => $post_type,
					'include' => $ids,
					'numberposts' => -1
				);
				$orders = get_posts( $args );
			}
		}
		return $orders;

	}

	function woo_sr_get_order_product_quantity( $order = false, $product_id = 0 ) {

		if( $order && $product_id ) {
			if( $order_item = woo_sr_get_order_line_item( $order->id, $product_id ) ) {
				$quantity = woo_sr_get_order_line_item_meta_by_key( $order_item->order_item_id, '_qty' );
				return $quantity;
			}
		}

	}

	function woo_sr_get_order_line_item( $order_id = 0, $product_id = 0 ) {

		global $wpdb;

		$line_item_sql = $wpdb->prepare( "SELECT order_items.* FROM `" . $wpdb->prefix . "woocommerce_order_items` as order_items LEFT JOIN `" . $wpdb->prefix . "woocommerce_order_itemmeta` as order_item_meta ON order_items.order_item_id = order_item_meta.order_item_id WHERE order_items.order_id = %d AND order_item_meta.meta_key = '_product_id' AND order_item_meta.meta_value = %d", $order_id, $product_id );
		$line_item = $wpdb->get_row( $line_item_sql );
		return $line_item;

	}

	function woo_sr_get_order_line_item_meta_by_key( $order_item_id = 0, $meta_key = '' ) {

		global $wpdb;

		if( $order_item_id && !empty( $meta_key ) ) {
			$line_item_meta_sql = $wpdb->prepare( "SELECT meta_value FROM `" . $wpdb->prefix . "woocommerce_order_itemmeta` WHERE order_item_id = %d AND meta_key = %s LIMIT 1", $order_item_id, $meta_key );
			$line_item_meta = $wpdb->get_var( $line_item_meta_sql );
			return $line_item_meta;
		}

	}

	function woo_sr_get_customer_orders_count( $billing_email = '', $format = 'posts', $status = 'completed' ) {

		if( empty( $billing_email ) )
			return false;

		$orders = array(); //order ids

		$post_type = 'shop_order';
		$args = array(
			'numberposts'     => -1,
			'meta_key'        => '_billing_email',
			'meta_value'      => $customer_email,
			'post_type'       => $post_type,
		);
		// Check if this is a WooCommerce 2.2+ instance (new Post Status)
		$woocommerce_version = woo_get_woo_version();
		if( version_compare( $woocommerce_version, '2.2' ) >= 0 ) {
			if( $status == 'completed' )
				$status = 'wc-completed';
			$args['post_status'] = $status;
		} else {
			$args['post_status'] = 'publish';
			$args['tax_query'] = array(
				array(
					'taxonomy'  => 'shop_order_status',
					'field'     => 'slug',
					'terms'     => $status
				)
			);
		}
		$orders = get_posts( $args );
		if( $format == 'count' ) {
			$orders = count( $orders );
		}
		return $orders;

	}

	function woo_sr_get_user_login( $user_id = 0 ) {

		if( $user_id ) {
			$user = get_userdata( $user_id );
			$output = $user->user_login;
			return $output;
		}

	}

	/* End of: WordPress Administration */

}

/* Start of: Common */

/*
 * Do something after WooCommerce sets an order on completed
 */
function woo_sr_save_coupon_code( $order_status, $order_id ) {

	$order = new WC_Order( $order_id );
	if ( count( $order->get_items() ) > 0 ) {
		print_r( $order );
	}
	echo '222';

}
add_action( 'woocommerce_payment_complete_order_status', 'woo_sr_save_coupon_code', 10, 2 );

/* End of: Common */
?>