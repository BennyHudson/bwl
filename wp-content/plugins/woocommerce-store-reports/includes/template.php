<?php
function woo_sr_template_header( $title = '', $icon = 'woocommerce', $action = '' ) { ?>
<div class="wrap">
	<div id="icon-<?php echo $icon; ?>" class="icon32"><br /></div>
	<h2>
		<?php echo $title; ?>
		<?php echo $action; ?>
	</h2>
<?php
}

function woo_sr_template_footer() { ?>
</div>
<!-- .wrap -->
<?php
}

function woo_sr_default_screen_button() {

	echo woo_sr_default_screen_button_html();

}

function woo_sr_get_default_screen_button() {

	return woo_sr_default_screen_button_html();

}

function woo_sr_default_screen_button_html() {

	$url = add_query_arg( 'page', 'woo_sr', 'admin.php' );
	$output = '<a href="' . $url . '" class="button">' . __( 'Return to Reports', 'woo_sr' ) . '</a>';
	return $output;

}

function woo_sr_get_report_search() {

	return woo_sr_report_search_html();

}

function woo_sr_report_search_html() {

	$output = '';
	$s = '';
	if( isset( $_GET['s'] ) ) {
		$s = $_GET['s'];
		$output = '<span class="subtitle">' . sprintf( __( 'Search results for "%s"', 'woo_sr' ), $s ) . '</span>';
	}
	return $output;

}

function woo_sr_column_order( $key = '', $output = '' ) {

	$orderby = '';
	if( isset( $_GET['orderby'] ) )
		$orderby = $_GET['orderby'];
	if( $key == $orderby && $output == 'ASC' )
		$output = 'DESC';
	else
		$output = 'ASC';
	return $output;

}

function woo_sr_format_date( $date = '' ) {

	if( !empty( $date ) ) {
		$output = mysql2date( get_option( 'date_format' ), $date );
		return $output;
	}

}

function woo_sr_date_ago( $date = '', $format = 'timestamp' ) {

	if( !empty( $date ) ) {
		if( $format == 'date' )
			$date = strtotime( $date );
		$output = sprintf( __( '%s ago', 'woo_sr' ), human_time_diff( $date ) );
		return $output;
	}

}

function woo_sr_format_order_address( $order = '', $format = 'billing' ) {

	if( !empty( $order ) ) {
		$address = array(
			'address_1'		=> $order->{$format . _address_1},
			'address_2'		=> $order->{$format . _address_2},
			'city'			=> $order->{$format . _city},
			'state'			=> $order->{$format . _state},
			'postcode'		=> $order->{$format . _postcode},
			'country'		=> $order->{$format . _country}
		);
		$joined_address = array();
		foreach( $address as $part ) {
			if( !empty( $part ) )
				$joined_address[] = $part;
		}
		$address = implode( ', ', $joined_address );
		$output = esc_html( preg_replace( '#<br\s*/?>#i', ', ', $address ) );
		return $output;
	}

}

function woo_sr_endash_empty( $value = '' ) {

	if( empty( $value ) ) {
		$value = '-';
	}
	return $value;

}
?>