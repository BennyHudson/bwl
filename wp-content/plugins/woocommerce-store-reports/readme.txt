*** This readme must accompany the Plugin at all times and not to be altered or changed in any way. ***

=== WooCommerce - Store Reports ===
Contributors: Visser Labs
Tags: e-commerce, woocommerce, shop, cart, ecommerce, store reports
Requires at least: 2.9.2
Tested up to: 3.8.1
Stable tag: 1.2

== Description ==

Generate detailed live reports from your WooCommerce store including Count Sheet, Product Sold and Customer reports.

See http://www.visser.com.au/woocommerce/plugins/store-reports/

== Installation ==

1. Upload the folder 'woocommerce-store-reports' to the '/wp-content/plugins/' directory

2. Activate 'Store Reports for WooCommerce' through the 'Plugins' menu in WordPress

== Usage ==

1. Open WooCommerce > Store Reports

2. Choose a report to view

Have fun!

== Support ==

If you have any problems, questions or suggestions please join the members discussion on our WooCommerce dedicated forum.

http://www.visser.com.au/woocommerce/forums/

== Changelog ==

= 1.2 =
* Added: Column sorting support for Count Sheet report
* Added: Edit Post link within Count Sheet report
* Changed: Hide stock levels for non-managed inventory

= 1.1 =
* Added: Count Sheet report
* Added: Product Search filtering for Count Sheet report

= 1.0 =
* Added: Initial release of the Plugin

== Disclaimer ==

It is not responsible for any harm or wrong doing this Plugin may cause. Users are fully responsible for their own use. This Plugin is to be used WITHOUT warranty.