<?php
/*
Plugin Name: WooCommerce - Store Reports
Plugin URI: http://www.visser.com.au/woocommerce/plugins/store-reports/
Description: View detailed live reports from across your WooCommerce store.
Version: 1.2
Author: Visser Labs
Author URI: http://www.visser.com.au/about/
License: GPL2
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

define( 'WOO_SR_DIRNAME', basename( dirname( __FILE__ ) ) );
define( 'WOO_SR_RELPATH', basename( dirname( __FILE__ ) ) . '/' . basename( __FILE__ ) );
define( 'WOO_SR_PATH', plugin_dir_path( __FILE__ ) );
define( 'WOO_SR_PREFIX', 'woo_sr' );

include_once( WOO_SR_PATH . 'common/common.php' );
include_once( WOO_SR_PATH . 'includes/functions.php' );

function woo_sr_i18n() {

	load_plugin_textdomain( 'woo_sr', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );

}
add_action( 'init', 'woo_sr_i18n' );

if( is_admin() ) {

	/* Start of: WordPress Administration */

	function woo_sr_enqueue_scripts( $hook ) {

		$pages = array( 'woocommerce_page_woo_sr' );
		if( in_array( $hook, $pages ) ) {
			// WooCommerce

			// Simple check that WooCommerce is activated
			if( class_exists( 'WooCommerce' ) ) {

				global $woocommerce;

				wp_enqueue_style( 'woocommerce_admin_styles', $woocommerce->plugin_url() . '/assets/css/admin.css' );

			}
			// Common
			wp_enqueue_style( 'woo_sr-styles', plugins_url( '/templates/admin/woo-admin_sr.css', __FILE__ ) );
			wp_enqueue_script( 'woo_sr-scripts', plugins_url( '/templates/admin/woo-sr_admin.js', __FILE__ ), array( 'jquery' ) );

			// Print stylesheet
			wp_enqueue_style( 'woo_sr-print', plugins_url( '/templates/admin/woo-admin_sr-print.css', __FILE__ ), false, '1.0.0', 'print' );
		}

	}
	add_action( 'admin_enqueue_scripts', 'woo_sr_enqueue_scripts' );

	function woo_sr_html_page() {

		include_once( WOO_SR_PATH . 'includes/template.php' );
		$action = woo_get_action();
		switch( $action ) {

			case 'view':
				$report = $_GET['report'];
				if( $report ) {
					$s = '';
					if( isset( $_GET['s'] ) ) {
						$s = $_GET['s'];
					}
					$order = '';
					if( isset( $_GET['order'] ) ) {
						$order = $_GET['order'];
					}
					$orderby = '';
					if( isset( $_GET['orderby'] ) ) {
						$orderby = $_GET['orderby'];
					}
					switch( $report ) {

						// Count Sheet
						case 'count_sheet':
							$args = array(
								'search' => $s,
								'orderby' => $orderby,
								'order' => $order
							);
							$products = woo_sr_get_products( $args );
							$size = count( $products );
							$total = (object)array(
								'quantity_sold' => 0,
								'stock' => 0
							);
							for( $i = 0; $i < $size; $i++ ) {
								if( empty( $products[$i]->stock ) ) {
									unset( $products[$i] );
								} else {
									$total->stock = $total->stock + $products[$i]->stock;
								}
								$total->quantity_sold = $total->quantity_sold + $products[$i]->quantity_sold;
							}
							break;

						// Product Orders
						case 'product_orders':
							$product_id = 0;
							if( isset( $_GET['product_id'] ) ) {
								$product_id = $_GET['product_id'];
								// Redirect to different template file
								$report = 'product_orders_detail';
							}
							if( $product_id ) {
								// Product detail screen
								$product = woo_sr_get_product( $product_id );
								$args = array(
									'orderby' => $orderby,
									'order' => $order
								);
								if( $orders = woo_sr_get_product_orders( $product_id, $args ) ) {
									$size = count( $orders );
									foreach( $orders as $key => $order ) {
										$orders[$key] = new WC_Order( $order->ID );
										$orders[$key]->qty_sold = woo_sr_get_order_product_quantity( $orders[$key], $product_id );
									}
								}
							} else {
								// Default screen with Products list
								$args = array(
									'orderby' => $orderby,
									'order' => $order
								);
								$products = woo_sr_get_products( $args );
								$size = count( $products );
								for( $i = 0; $i < $size; $i++ ) {
									if( $products[$i]->quantity_sold == 0 ) {
										unset( $products[$i] );
									}
								}
								$size = count( $products );
							}
							break;

						// Product Customers
						case 'product_customers':
							$product_id = 0;
							if( isset( $_GET['product_id'] ) ) {
								$product_id = $_GET['product_id'];
								// Redirect to different template file
								$report = 'product_customers_detail';
							}
							if( $product_id ) {
								// Product detail screen
								$product = woo_sr_get_product( $product_id );
								if( $customers = woo_sr_get_product_customers( $product_id ) ) {
									$size = count( $customers );
								} else {
									// If this returns empty then its likely no e-mail has been filled for orders
									$troubleshooting_url = 'http://www.visser.com.au/documentation/store-reports/';
									$report_url = add_query_arg( array( 'report' => 'product_orders', 'product_id' => $product_id ) );
									$message = sprintf( __( 'While there are Orders assigned to %s they need to have the Billing E-mail field filled to fill this report, to see a list of Orders to be filled open the following <a href="%s">Product Orders</a> report.', 'woo_sr' ), $product->name, $report_url ) . ' <a href="' . $troubleshooting_url . '" target="_blank">' . __( 'Need help?', 'woo_sr' ) . '</a>';
									woo_sr_admin_notice( $message, 'error' );
								}
							} else {
								// Default screen with Products list
								$args = array(
									'orderby' => $orderby,
									'order' => $order
								);
								$products = woo_sr_get_products( $args );
								$size = count( $products );
								for( $i = 0; $i < $size; $i++ ) {
									if( $products[$i]->quantity_sold == 0 ) {
										unset( $products[$i] );
									}
								}
								$size = count( $products );
							}
							break;

					}
					if( file_exists( WOO_SR_PATH . 'templates/admin/woo-sr_admin_report-' . $report . '.php' ) ) {
						include_once( WOO_SR_PATH . 'templates/admin/woo-sr_admin_report-' . $report . '.php' );
					} else {
						$troubleshooting_url = 'http://www.visser.com.au/documentation/store-reports/';
						$message = '<strong>' . __( 'No report template could be found for this report, please re-install Store Reports for WooCommerce.', 'woo_sr' ) . '</strong> <a href="' . $troubleshooting_url . '" target="_blank">' . __( 'Need help?', 'woo_sr' ) . '</a>';
						woo_sr_admin_notice( $message, 'error' );
						woo_sr_default_screen();
					}
				}
				break;

			default:
				woo_sr_default_screen();
				break;

		}

	}

	/* End of: WordPress Administration */

}
?>