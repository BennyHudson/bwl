<?php woo_sr_template_header( __( 'Count Sheet', 'woo_sr' ), '', woo_sr_get_default_screen_button() . woo_sr_get_report_search() ); ?>
<div id="content" class="no_pagination">
	<form action="" method="get" class="exclude_printer">
		<input type="hidden" name="page" value="woo_sr" />
		<input type="hidden" name="action" value="view" />
		<input type="hidden" name="report" value="count_sheet" />
		<p class="search-box">
			<input type="input" id="post-search-input" name="s" value="<?php echo $s; ?>" />
			<input type="submit" name="" id="search-submit" class="button-primary" value="<?php _e( 'Search Products', 'woo_sr' ); ?>">
		</p>
	</form>
	<table class="wp-list-table widefat fixed clear" cellspacing="0">
		<thead>

			<tr>
				<th class="manage-column short-column" nowrap><a href="<?php echo add_query_arg( array( 'orderby' => 'sku', 'order' => woo_sr_column_order( 'sku', $order ) ) ); ?>"><?php _e( 'SKU', 'woo_sr' ); ?></a></th>
				<th class="manage-column" nowrap><a href="<?php echo add_query_arg( array( 'orderby' => 'name', 'order' => woo_sr_column_order( 'name', $order ) ) ); ?>"><?php _e( 'Product Name', 'woo_sr' ); ?></a></th>
				<th class="manage-column currency-column text-align-right"><?php _e( 'Price', 'woo_sr' ); ?></th>
				<th class="manage-column number-column"><a href="<?php echo add_query_arg( array( 'orderby' => 'qty_sold', 'order' => woo_sr_column_order( 'qty_sold', $order ) ) ); ?>"><?php _e( 'Qty. Sold', 'woo_sr' ); ?></a></th>
				<th class="manage-column number-column"><a href="<?php echo add_query_arg( array( 'orderby' => 'stock', 'order' => woo_sr_column_order( 'stock', 	$order ) ) ); ?>"><?php _e( 'Stock', 'woo_sr' ); ?></a></th>
			</tr>

		</thead>
<?php if( $products ) { ?>
		<tbody>

	<?php foreach( $products as $product ) { ?>
		<tr id="product-<?php echo $product->ID; ?>">
			<td><?php echo woo_sr_endash_empty( $product->sku ); ?></td>
			<td>
				<a href="<?php echo $product->edit_url; ?>" title="<?php printf( __( 'Edit %s', 'woo_sr' ), $product->name ); ?>"><?php echo $product->name; ?></a>
			</td>
			<td class="text-align-right"><?php echo woocommerce_price( $product->price ); ?></td>
			<td><?php echo $product->quantity_sold; ?></td>
			<td><strong><?php echo $product->stock; ?></strong></td>
		</tr>

	<?php } ?>
		</tbody>
		<tfoot>

			<tr>
				<td colspan="3">&nbsp;</td>
				<td><strong><?php echo $total->quantity_sold; ?></strong></td>
				<td><strong><?php echo $total->stock; ?></strong></td>
			</tr>

		</tfoot>
<?php } else { ?>
	<tbody>

		<tr>
			<td colspan="5"><?php _e( 'No stocked Products were found.', 'woo_sr' ); ?></td>
		</tr>

	</tbody>
<?php } ?>
	</table>
	<!-- .widefat -->
	<p class="text-align-right"><?php printf( __( 'Number of Products: %d', 'woo_sr' ), $size ); ?></p>
</div>
<!-- #content -->
