<?php woo_sr_template_header( __( 'Product Customers', 'woo_sr' ), '', woo_sr_get_default_screen_button() . woo_sr_get_report_search() ); ?>
<h3><?php printf( __( 'Product: %s', 'woo_sr' ), $product->name ); ?></h3>
<div id="content" class="no_pagination">
	<table class="wp-list-table widefat fixed clear" cellspacing="0">
		<thead>

			<tr>
				<th class="manage-column" nowrap><?php _e( 'Name', 'woo_sr' ); ?></th>
				<th class="manage-column"><?php _e( 'Billing', 'woocommerce' ); ?></th>
				<th class="manage-column"><?php _e( 'Shipping', 'woocommerce' ); ?></th>
				<th class="manage-column"><?php _e( 'Last Order', 'woo_sr' ); ?></th>
				<th class="manage-column"><?php _e( 'Invoices', 'woo_sr' ); ?></th>
			</tr>

		</thead>
<?php if( $customers ) { ?>
		<tbody>
	<?php foreach( $customers as $customer ) { ?>
		<tr id="customer-<?php echo $customer->id; ?>">
			<td>
		<?php if( $customer->customer_user ) { ?>
				<a href="<?php echo get_edit_user_link( $customer->customer_user ); ?>"><strong><?php printf( '%s %s', $customer->billing_first_name, $customer->billing_last_name ); ?></strong></a>
		<?php } else { ?>
				<strong><?php printf( '%s %s', $customer->billing_first_name, $customer->billing_last_name ); ?></strong>
		<?php } ?>
				<small class="meta">E-mail: <?php echo $customer->billing_email; ?></small>
			</td>
			<td>
<?php if( $customer->billing_address ) { ?>
				<?php echo $customer->billing_address; ?>
<?php } else { ?>
				<?php _e( 'No billing address set.', 'woocommerce' ); ?>
<?php } ?>
			</td>
			<td>
<?php if( $customer->shipping_address ) { ?>
				<?php echo $customer->shipping_address; ?>
<?php } else { ?>
				<?php _e( 'No shipping address set.', 'woocommerce' ); ?>
<?php } ?>
			</td>
			<td>
				<?php echo woo_sr_date_ago( $customer->order_date, 'date' ); ?>
				<small class="meta"><?php echo woo_sr_format_date( $customer->order_date ); ?></small>
			</td>
			<td><?php echo $customer->invoices; ?></td>
		</tr>

	<?php } ?>
		</tbody>
<?php } else { ?>
	<tbody>

		<tr>
			<td colspan="1"><?php _e( 'No Customers were found.', 'woo_sr' ); ?></td>
		</tr>

	</tbody>
<?php } ?>
	</table>
	<!-- .widefat -->
	<p class="text-align-right"><?php printf( __( 'Number of Customers: %d', 'woo_sr' ), $size ); ?></p>
</div>
<!-- #content -->