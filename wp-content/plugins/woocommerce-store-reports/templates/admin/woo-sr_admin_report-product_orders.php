<?php woo_sr_template_header( __( 'Product Orders', 'woo_sr' ), '', woo_sr_get_default_screen_button() . woo_sr_get_report_search() ); ?>
<div id="content" class="no_pagination">
	<table class="wp-list-table widefat fixed clear" cellspacing="0">
		<thead>

			<tr>
				<th class="manage-column short-column" nowrap><a href="<?php echo add_query_arg( array( 'orderby' => 'sku', 'order' => woo_sr_column_order( 'sku', $order ) ) ); ?>"><?php _e( 'SKU', 'woo_sr' ); ?></a></th>
				<th class="manage-column" nowrap><a href="<?php echo add_query_arg( array( 'orderby' => 'name', 'order' => woo_sr_column_order( 'name', $order ) ) ); ?>"><?php _e( 'Product Name', 'woo_sr' ); ?></a></th>
				<th class="manage-column number-column"><a href="<?php echo add_query_arg( array( 'orderby' => 'stock', 'order' => woo_sr_column_order( 'stock', $order ) ) ); ?>"><?php _e( 'Stock', 'woocommerce' ); ?></th>
				<th class="manage-column number-column"><a href="<?php echo add_query_arg( array( 'orderby' => 'qty_sold', 'order' => woo_sr_column_order( 'qty_sold', $order ) ) ); ?>"><?php _e( 'Qty. Sold', 'woo_sr' ); ?></a></th>
			</tr>

		</thead>
<?php if( $products ) { ?>
		<tbody>

	<?php foreach( $products as $product ) { ?>
		<tr id="product-<?php echo $product->ID; ?>">
			<td nowrap><?php echo woo_sr_endash_empty( $product->sku ); ?></td>
			<td>
				<a href="<?php echo add_query_arg( 'product_id', $product->ID ); ?>" title="<?php printf( __( 'View Orders of %s', 'woo_sr' ), $product->name ); ?>"><?php echo $product->name; ?></a>
			</td>
			<td><?php echo woo_sr_endash_empty( $product->stock ); ?></td>
			<td><strong><?php echo $product->quantity_sold; ?></strong></td>
		</tr>

	<?php } ?>
		</tbody>
<?php } else { ?>
	<tbody>

		<tr>
			<td colspan="3"><?php _e( 'No Product sales were found.', 'woo_sr' ); ?></td>
		</tr>

	</tbody>
<?php } ?>
	</table>
	<!-- .widefat -->
	<p class="text-align-right"><?php printf( __( 'Number of Products: %d', 'woo_sr' ), $size ); ?></p>
</div>
<!-- #content -->