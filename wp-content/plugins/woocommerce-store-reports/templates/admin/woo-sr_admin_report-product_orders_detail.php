<?php woo_sr_template_header( __( 'Product Orders', 'woo_sr' ), '', woo_sr_get_default_screen_button() . woo_sr_get_report_search() ); ?>
<h3><?php printf( __( 'Product: %s', 'woo_sr' ), $product->name ); ?></h3>
<div id="content" class="no_pagination">
	<table class="wp-list-table widefat fixed posts clear" cellspacing="0">
		<thead>

			<tr>
				<th id="order_status" class="manage-column column-order_status"><a href="<?php echo add_query_arg( array( 'orderby' => 'status', 'order' => woo_sr_column_order( 'status', $order ) ) ); ?>"><?php _e( '...', 'woocommerce' ); ?></a></th>
				<th class="manage-column" nowrap><?php _e( 'Order', 'woocommerce' ); ?></th>
				<th class="manage-column"><?php _e( 'Billing', 'woocommerce' ); ?></th>
				<th class="manage-column"><?php _e( 'Shipping', 'woocommerce' ); ?></th>
				<th class="manage-column number-column"><?php _e( 'Qty. Sold', 'woocommerce' ); ?></th>
				<th class="manage-column number-column"><?php _e( 'Total', 'woocommerce' ); ?></th>
				<th class="manage-column"><?php _e( 'Date', 'woocommerce' ); ?></th>
			</tr>

		</thead>
<?php if( $orders ) { ?>
		<tbody id="the-list">
	<?php foreach( $orders as $order ) { ?>
		<tr id="order-<?php echo $order->id; ?>" class="type-shop_order">
			<td class="order_status column-order_status">
				<?php printf( '<mark class="%s tips" data-tip="%s">%s</mark>', sanitize_title( $order->status ), esc_html__( $order->status, 'woocommerce' ), esc_html__( $order->status, 'woocommerce' ) ); ?>
			</td>
			<td nowrap>
				<?php printf( __( '<a href="%s"><strong>Order #%s</strong></a> by ', 'woocommerce' ), get_edit_post_link( $order->id ), $order->id ); ?>
		<?php if( $order->customer_user ) { ?>
				<?php printf( '<a href="%s">%s</a>', get_edit_user_link( $order->customer_user ), woo_sr_get_user_login( $order->customer_user ) ); ?>
		<?php } else { ?>
				<?php _e( 'Guest', 'wordpress' ); ?>
		<?php } ?>
				<small class="meta">E-mail: <?php echo $order->billing_email; ?></small>
				<small class="meta">Tel: <?php echo $order->billing_phone; ?></small>
			</td>
			<td>
<?php if( $order->billing_address ) { ?>
				<?php echo $order->billing_address; ?>
<?php } else { ?>
				<?php _e( 'No billing address set.', 'woocommerce' ); ?>
<?php } ?>
			</td>
			<td>
<?php if( $order->shipping_address ) { ?>
				<?php echo $order->shipping_address; ?>
<?php } else { ?>
				<?php _e( 'No shipping address set.', 'woocommerce' ); ?>
<?php } ?>
			</td>
			<td><?php print_r( $order->qty_sold ); ?></td>
			<td><?php echo woocommerce_price( $order->order_total ); ?></td>
			<td>
				<?php echo woo_sr_date_ago( $order->order_date, 'date' ); ?>
				<small class="meta"><?php echo woo_sr_format_date( $order->order_date ); ?></small>
			</td>
		</tr>

	<?php } ?>
		</tbody>
<?php } else { ?>
	<tbody>

		<tr>
			<td colspan="7"><?php _e( 'No Orders were found.', 'woo_sr' ); ?></td>
		</tr>

	</tbody>
<?php } ?>
	</table>
	<!-- .widefat -->
	<p class="text-align-right"><?php printf( __( 'Number of Orders: %d', 'woo_sr' ), $size ); ?></p>
</div>
<!-- #content -->