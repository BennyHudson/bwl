<div id="woo_sr-widgets" class="metabox-holder">
	<div id="woo_sr-widgets-wrap">
		<div class="postbox-container">

<?php foreach( $reports as $report ) { ?>
			<div id="report-<?php echo $report['name']; ?>" class="postbox">
				<h3 class="hndle"><?php echo $report['title']; ?></h3>
				<div class="inside">
					<form action="" method="post">
						<div class="widget-content">
							<p><?php echo $report['description']; ?></p>
						</div>
						<!-- .widget-content -->
						<div class="widget-control-actions">
							<div class="alignright">
								<a href="<?php echo $report['url']; ?>" class="button-primary"><?php _e( 'View Report', 'wpsc_sr' ); ?></a>
							</div>
							<br class="clear" />
						</div>
						<!-- .widget-control-actions -->
					</form>
				</div>
				<!-- .inside -->
			</div>
			<!-- .postbox -->

<?php } ?>
			<br class="clear" />
		</div>
		<!-- #postbox-container -->
	</div>
	<!-- #woo_sr-widgets-wrap -->
</div>
<!-- .metabox-holder -->
<br class="clear" />
