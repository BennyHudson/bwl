<?php 
/**
  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

require_once 'Customweb/Core/Http/Request.php';
require_once 'Customweb/Core/Http/Client/Factory.php';
require_once 'Customweb/Form/ElementGroup.php';
require_once 'Customweb/Core/Url.php';
require_once 'Customweb/Form/Control/Html.php';
require_once 'Customweb/Form/Element.php';
require_once 'Customweb/I18n/Translation.php';
require_once 'Customweb/Payment/BackendOperation/Form/Abstract.php';


/**
 * 
 * @author Björn Hasselmann
 * @BackendForm()
 *
 */
class Customweb_SagePay_BackendOperation_Form_IpAddress extends Customweb_Payment_BackendOperation_Form_Abstract {
	
	public function getTitle(){
		return Customweb_I18n_Translation::__("IP Address");
	}
	
	public function getElementGroups(){
		return $this->getIpElementGroup();
	}
	
	private function getIpElementGroup(){
		
		$request = new Customweb_Core_Http_Request(new Customweb_Core_Url("http://www.customweb.com/my-ip.php"));
		$client = Customweb_Core_Http_Client_Factory::createClient();
		$response = $client->send($request);
				
		$ip = "";
		if($response->getStatusCode() == 200){
			$ip = $response->getBody();
		} else {
			$ip = Customweb_I18n_Translation::__("Couldn't obtain IP address.");
		}
		
		$group = new Customweb_Form_ElementGroup();
		
		$control = new Customweb_Form_Control_Html('ipaddress', $ip);
		$element = new Customweb_Form_Element('IP Address', $control);
		$element->setDescription(
					Customweb_I18n_Translation::__("Please enter this IP in the Sage Pay Backend > Settings > Valid IPs")
				);
		$element->setRequired(false);
		
		return array($group->addElement($element));
	}
	
}