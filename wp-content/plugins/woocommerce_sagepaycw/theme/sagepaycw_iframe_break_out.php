<script type="text/javascript"> 
	top.location.href = '<?php echo $url; ?>';
</script>

<noscript>
	<a class="button sagepaycw-continue-button" href="<?php echo $url; ?>" target="_top"><?php echo __('Continue', 'woocommerce_sagepaycw'); ?></a>
</noscript>
