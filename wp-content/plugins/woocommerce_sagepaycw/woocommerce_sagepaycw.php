<?php

/**
 * Plugin Name: WooCommerce SagePayCw
 * Plugin URI: http://www.customweb.ch
 * Description: This plugin adds the SagePayCw payment gateway to your WooCommerce.
 * Version: 2.1.200
 * Author: customweb GmbH
 * Author URI: http://www.customweb.ch
 */

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2015 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

// Make sure we don't expose any info if called directly
if (!function_exists('add_action')) {
	echo "Hi there!  I'm just a plugin, not much I can do when called directly.";
	exit();
}

// Load Language Files
load_plugin_textdomain('woocommerce_sagepaycw', false, basename(dirname(__FILE__)) . '/translations');

require_once dirname(__FILE__) . '/lib/loader.php';
require_once 'SagePayCwUtil.php';

// Add translation Adapter
SagePayCwUtil::includeClass('SagePayCw_TranslationResolver');

// Get all admin functionality
require_once SagePayCwUtil::getBasePath() . '/admin.php';

register_activation_hook(__FILE__, array(
	'SagePayCwUtil',
	'installPlugin' 
));
add_filter('woocommerce_payment_gateways', array(
	'SagePayCwUtil',
	'addPaymentMethods' 
));

if (!is_admin()) {

	function woocommerce_sagepaycw_add_frontend_css(){
		wp_register_style('woocommerce_sagepaycw_frontend_styles', plugins_url('resources/css/frontend.css', __FILE__));
		wp_enqueue_style('woocommerce_sagepaycw_frontend_styles');
		
		wp_register_script('sagepaycw_frontend_script', plugins_url('resources/js/frontend.js', __FILE__), array(
			'jquery' 
		));
		wp_enqueue_script('sagepaycw_frontend_script');
	}
	add_action('wp_enqueue_scripts', 'woocommerce_sagepaycw_add_frontend_css');
}

// Log Errors
function woocommerce_sagepaycw_add_errors(){
	if (isset($_GET['sagepaycw_failed_transaction_id']) && isset($_GET['sagepaycw_transaction_token'])) {
		SagePayCwUtil::includeClass('SagePayCw_Transaction');
		$dbTransaction = SagePayCwUtil::getTransactionById($_GET['sagepaycw_failed_transaction_id']);
		$validateHash = SagePayCwUtil::computeValidateHash($dbTransaction);
		if ($validateHash == $_GET['sagepaycw_transaction_token']) {
			

	if (!function_exists('wc_add_notice')) {
		global $woocommerce;
		$woocommerce->add_error( current($dbTransaction->getTransactionObject()->getErrorMessages()) );
	}
	else {
		wc_add_notice(current($dbTransaction->getTransactionObject()->getErrorMessages()), 'error');
	}
	

		}
	}
}
add_action('init', 'woocommerce_sagepaycw_add_errors');

add_action('woocommerce_before_checkout_billing_form', array(
	'SagePayCwUtil',
	'actionBeforeCheckoutBillingForm' 
));
add_action('woocommerce_before_checkout_shipping_form', array(
	'SagePayCwUtil',
	'actionBeforeCheckoutShippingForm' 
));

function woocommerce_sagepaycw_create_order_status(){
	register_post_status('wc-sagepaycw-pending', 
			array(
				'label' => 'Sage Pay pending',
				'public' => true,
				'exclude_from_search' => false,
				'show_in_admin_all_list' => true,
				'show_in_admin_status_list' => true,
				'label_count' => _n_noop('Sage Pay pending <span class="count">(%s)</span>', 
						'Sage Pay pending <span class="count">(%s)</span>') 
			));
}

// Add to list of WC Order statuses
function woocommerce_sagepaycw_add_order_status( $order_statuses ) {

	$order_statuses['wc-sagepaycw-pending'] = 'Sage Pay pending';
	return $order_statuses; 
}

if (defined('WOOCOMMERCE_VERSION') && version_compare(WOOCOMMERCE_VERSION, '2.2.0') >= 0) {
	add_action('init', 'woocommerce_sagepaycw_create_order_status');
	add_filter( 'wc_order_statuses', 'woocommerce_sagepaycw_add_order_status' );
}



function createSagePayCwCronInterval($schedules){
	$schedules['SagePayCwCronInterval'] = array(
		'interval' => 120,
		'display' => __('SagePayCw Interval', 'woocommerce_sagepaycw') 
	);
	return $schedules;
}

function createSagePayCwCron(){
	$timestamp = wp_next_scheduled('SagePayCwCron');
	if ($timestamp == false) {
		wp_schedule_event(time() + 120, 'SagePayCwCronInterval', 'SagePayCwCron');
	}
}

function deleteSagePayCwCron(){
	wp_clear_scheduled_hook('SagePayCwCron');
}

function runSagePayCwCron(){
	SagePayCwUtil::includeClass('SagePayCw_UpdateObserver');
	SagePayCw_UpdateObserver::run();
}

//Cron Functions to pull update
register_activation_hook(__FILE__, 'createSagePayCwCron');
register_deactivation_hook(__FILE__, 'deleteSagePayCwCron');

add_filter('cron_schedules', 'createSagePayCwCronInterval');
add_action('SagePayCwCron', 'runSagePayCwCron');




//Action to add payment information to order confirmation page, and email
add_action('woocommerce_thankyou', array(
	'SagePayCwUtil',
	'thankYouPageHtml' 
));
add_action('woocommerce_email_before_order_table', array(
	'SagePayCwUtil',
	'orderEmailHtml' 
), 10, 3);


//Ajax Handling
add_action('wp_ajax_woocommerce_sagepaycw_update_payment_form', 'woocommerce_sagepaycw_ajax_update_payment_form');
add_action('wp_ajax_nopriv_woocommerce_sagepaycw_update_payment_form', 'woocommerce_sagepaycw_ajax_update_payment_form');

function woocommerce_sagepaycw_ajax_update_payment_form(){
	if (!isset($_POST['payment_method'])) {
		die();
	}
	$length = strlen('SagePayCw');
	if (substr($_POST['payment_method'], 0, $length) != 'SagePayCw') {
		die();
	}
	try {
		$paymentMethod = SagePayCwUtil::getPaymentMehtodInstance($_POST['payment_method']);
		$paymentMethod->payment_fields();
		die();
	}
	catch (Exception $e) {
		die();
	}
}

//Fix to avoid multiple cart calculations
function woocommerce_sagepaycw_before_calculate_totals($cart){
	$cart->disableValidationCw = true;
	return;
}
add_action('woocommerce_before_calculate_totals', 'woocommerce_sagepaycw_before_calculate_totals');


//Fix to avoid multiple cart calculations
function woocommerce_sagepaycw_after_calculate_totals($cart){
	$cart->totalCalculatedCw = true;
	$cart->disableValidationCw = false;
	return;
}
add_action('woocommerce_after_calculate_totals', 'woocommerce_sagepaycw_after_calculate_totals');