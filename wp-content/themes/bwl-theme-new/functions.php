<?php
	include 'includes/theme-custom-posts.php';
	add_action( 'after_setup_theme', 'tastic_theme_setup' );
	function tastic_theme_setup() {
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );
		add_action( 'widgets_init', 'tastic_register_sidebars' );
		add_filter('wp_mail_from_name', 'new_mail_from_name');
		add_action( 'init', 'register_my_menus' );
		//add_action( 'wp_enqueue_scripts', 'jk_load_dashicons' );
		add_theme_support( 'woocommerce' );
		add_action('init', 'bwl_partners');
		add_action('init', 'bwl_taxonomies_partner');
		add_action('init', 'bwl_adverts');
		add_action('init', 'bwl_strengthpower');
		add_action('init', 'bwl_strengthpowerentries');
		add_action('init', 'bwl_taxonomies_advert');
		add_action('init', 'bwl_taxonomies_agegroups');
		add_action('init', 'bwl_taxonomies_challenges');
		add_action('init', 'flush_rewrite_rules');
		add_action( 'admin_init', 'add_theme_caps');
		add_filter('gform_pre_render_8', 'populate_posts');
		add_action( 'gform_user_registered', 'pi_gravity_registration_autologin',  10, 4 );
		add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 20;' ), 20 );
		add_filter('the_title', 'the_title_trim');
		add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );
		add_action( 'wp_enqueue_scripts', 'bwl_scripts' );
		add_action('login_head', 'add_favicon');
		add_action('admin_head', 'add_favicon');
	}

// Enqueue Scripts
	function bwl_scripts() {
		//wp_enqueue_script( 'production', get_template_directory_uri() . '/js/production.js', array(), '1.0.0', false );
	}

// Enqueue Dashicons
	function jk_load_dashicons() {
		wp_enqueue_style( 'dashicons' );
	}
	remove_action('wp_head', 'wp_print_scripts'); 
	remove_action('wp_head', 'wp_print_head_scripts', 9);
	add_action('wp_footer', 'wp_print_scripts', 5);
	add_action('wp_footer', 'wp_print_head_scripts', 5);
	add_filter('gform_init_scripts_footer', 'init_scripts');
	function init_scripts() {
	    return true;
	}

// Widget Areas
	function tastic_register_sidebars() {
		register_sidebar( array(
			'name' => 'Sidebar',
			'id' => 'blog-sidebar',
			'before_widget' => '<section class="widget">',
			'after_widget' => '</section>',
			'before_title' => '<h2>',
			'after_title' => '</h2>',
		) );
		register_sidebar( array(
			'name' => 'Shop Sidebar',
			'id' => 'shop-sidebar',
			'before_widget' => '<section class="widget">',
			'after_widget' => '</section>',
			'before_title' => '<h2>',
			'after_title' => '</h2>',
		) );
		register_sidebar( array(
			'name' => 'Home Widgets',
			'id' => 'home-widgets',
			'before_widget' => '<aside><section class="widget">',
			'after_widget' => '</section></aside>',
			'before_title' => '<h2>',
			'after_title' => '</h2>',
		) );
		register_sidebar( array(
			'name' => 'Header Search',
			'id' => 'header-search',
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '',
			'after_title' => '',
		) );
		register_sidebar( array(
			'name' => 'Header Adverts',
			'id' => 'header-adverts',
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '',
			'after_title' => '',
		) );
		register_sidebar( array(
			'name' => 'Footer Adverts',
			'id' => 'footer-adverts',
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '',
			'after_title' => '',
		) );
		register_sidebar( array(
			'name' => 'Footer Menus',
			'id' => 'footer-menus',
			'before_widget' => '<aside class="footer-menu">',
			'after_widget' => '</aside>',
			'before_title' => '<h2>',
			'after_title' => '</h2>',
		) );
		register_sidebar( array(
			'name' => 'Login Area',
			'id' => 'login-area',
			'before_widget' => '<section>',
			'after_widget' => '</section>',
			'before_title' => '<h2>',
			'after_title' => '</h2>',
		) );
	}	

// Menus
	function register_my_menus() {
	  register_nav_menus(
		array( 'mobile-menu' => __( 'Mobile Menu' ),
			   'footer-menu' => __( 'Footer Menu' ),
			   'header-menu' => __( 'Header Menu' ),
			   'about-menu' => __( 'About Menu' )
			 )
	  );
	}

// Add User Role

	function add_theme_caps() {
		// gets the author role
		$role = get_role( 'shop_manager' );
		// This only works, because it accesses the class instance.
		// would allow the author to edit others' posts for current theme only
		$role->add_cap( 'edit_plugins' ); 
	}

// Custom Post Types
	function bwl_partners() {
		$labels = array(
			'name' 					=> _x( 'Partners', 'post type general name'),
			'singular_name' 		=> _x( 'Partner', 'post type singular name'),
			'add_new' 				=> _x( 'Add New Partner', 'book'),
			'add_new_item'			=> ( 'Add New Partner' ),
			'edit_item'				=> ( 'Edit Partner' ),
			'new_item'				=> ( 'New Partner' ),
			'all_items'				=> ( 'All Partners' ),
			'view_item'				=> ( 'View Partner' ),
			'search_items'			=> ( 'Search Partners' ),
			'not_found'				=> ( 'No partners found' ),
			'not_found_in_trash'	=> ( 'No partners found in trash' ),
			'parent_item_colon'		=> '',
			'menu_name'				=> 'Partners'
		);
		$args = array(
			'labels'        => $labels,
			'description'   => 'Holds our partners and partner specific data',
			'public'        => true,
			'menu_position' => 7,
			'menu_icon'		=> 'dashicons-groups',
			'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
			'has_archive'   => true,
		);
		register_post_type( 'partners', $args );
	}
	function bwl_adverts() {
		$labels = array(
			'name' 					=> _x( 'Adverts', 'post type general name'),
			'singular_name' 		=> _x( 'Advert', 'post type singular name'),
			'add_new' 				=> _x( 'Add New Advert', 'book'),
			'add_new_item'			=> ( 'Add New Advert' ),
			'edit_item'				=> ( 'Edit Advert' ),
			'new_item'				=> ( 'New Advert' ),
			'all_items'				=> ( 'All Adverts' ),
			'view_item'				=> ( 'View Advert' ),
			'search_items'			=> ( 'Search Adverts' ),
			'not_found'				=> ( 'No adverts found' ),
			'not_found_in_trash'	=> ( 'No adverts found in trash' ),
			'parent_item_colon'		=> '',
			'menu_name'				=> 'Adverts'
		);
		$args = array(
			'labels'        => $labels,
			'description'   => 'Holds our adverts and advert specific data',
			'public'        => true,
			'menu_position' => 7,
			'menu_icon'		=> 'dashicons-images-alt2',
			'supports'      => array( 'title', 'thumbnail', 'excerpt' ),
			'taxonomies'	=> array( 'post_tag' ),
			'has_archive'   => true,
		);
		register_post_type( 'adverts', $args );
	}
	function bwl_strengthpower() {
		$labels = array(
			'name' 					=> _x( 'Strength & Power Series', 'post type general name'),
			'singular_name' 		=> _x( 'Challenge', 'post type singular name'),
			'add_new' 				=> _x( 'Add New Challenge', 'book'),
			'add_new_item'			=> ( 'Add New Challenge' ),
			'edit_item'				=> ( 'Edit Challenge' ),
			'new_item'				=> ( 'New Challenge' ),
			'all_items'				=> ( 'All Challenges' ),
			'view_item'				=> ( 'View Challenge' ),
			'search_items'			=> ( 'Search Challenges' ),
			'not_found'				=> ( 'No challenges found' ),
			'not_found_in_trash'	=> ( 'No challenges found in trash' ),
			'parent_item_colon'		=> '',
			'menu_name'				=> 'Strength & Power Series'
		);
		$args = array(
			'labels'        => $labels,
			'description'   => 'Holds our challenges and challenge specific data',
			'public'        => true,
			'menu_icon'		=> 'dashicons-universal-access',
			'supports'      => array( 'title', 'editor', 'thumbnail' ),
			'taxonomies'	=> array( 'post_tag' ),
			'has_archive'   => true,
			'menu_position' => 7,
		);
		register_post_type( 'strengthpower', $args );
	}
	function bwl_strengthpowerentries() {
		$labels = array(
			'name' 					=> _x( 'Strength & Power Series ', 'post type general name'),
			'singular_name' 		=> _x( 'Challenger', 'post type singular name'),
			'add_new' 				=> _x( 'Add New Challenger', 'book'),
			'add_new_item'			=> ( 'Add New Challenger' ),
			'edit_item'				=> ( 'Edit Challenger' ),
			'new_item'				=> ( 'New Challenger' ),
			'all_items'				=> ( 'All Challengers' ),
			'view_item'				=> ( 'View Challenger' ),
			'search_items'			=> ( 'Search Challengers' ),
			'not_found'				=> ( 'No challengers found' ),
			'not_found_in_trash'	=> ( 'No challengers found in trash' ),
			'parent_item_colon'		=> '',
			'menu_name'				=> 'Strength & Power Entries'
		);
		$args = array(
			'labels'        => $labels,
			'description'   => 'Holds our challenges and challenge specific data',
			'public'        => true,
			'menu_icon'		=> 'dashicons-universal-access-alt',
			'supports'      => array( 'title' ),
			'has_archive'   => true,
			'menu_position' => 7,
		);
		register_post_type( 'strengthentries', $args );
	}

// Custom Taxonomies	
	function bwl_taxonomies_partner() {
		$labels = array(
			'name'              => _x( 'Partner Type', 'taxonomy general name' ),
			'singular_name'     => _x( 'Partner Type', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Partner  Types' ),
			'all_items'         => __( 'All Partner  Types' ),
			'parent_item'       => __( 'Parent Partner  Type' ),
			'parent_item_colon' => __( 'Parent Partner  Type:' ),
			'edit_item'         => __( 'Edit Partner  Type' ), 
			'update_item'       => __( 'Update Partner  Type' ),
			'add_new_item'      => __( 'Add New Partner  Type' ),
			'new_item_name'     => __( 'New Partner  Typey' ),
			'menu_name'         => __( 'Partner  Types' ),
		);
		$args = array(
			'labels' => $labels,
			'hierarchical' => true,
		);
		register_taxonomy( 'partner-category', 'partners', $args );
	}
	function bwl_taxonomies_advert() {
		$labels = array(
			'name'              => _x( 'Advert Size', 'taxonomy general name' ),
			'singular_name'     => _x( 'Advert Size', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Advert Sizes' ),
			'all_items'         => __( 'All Advert Sizes' ),
			'parent_item'       => __( 'Parent Advert Size' ),
			'parent_item_colon' => __( 'Parent Advert Size:' ),
			'edit_item'         => __( 'Edit Advert Size' ), 
			'update_item'       => __( 'Update Advert Size' ),
			'add_new_item'      => __( 'Add New Advert Size' ),
			'new_item_name'     => __( 'New Advert Size' ),
			'menu_name'         => __( 'Advert Sizes' ),
		);
		$args = array(
			'labels' => $labels,
			'hierarchical' => true,
		);
		register_taxonomy( 'advert-category', 'adverts', $args );
	}
	function bwl_taxonomies_agegroups() {
		$labels = array(
			'name'              => _x( 'Age Group', 'taxonomy general name' ),
			'singular_name'     => _x( 'Age Group', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Age Groups' ),
			'all_items'         => __( 'All Age Groups' ),
			'parent_item'       => __( 'Parent Age Group' ),
			'parent_item_colon' => __( 'Parent Age Group:' ),
			'edit_item'         => __( 'Edit Age Group' ), 
			'update_item'       => __( 'Update Age Group' ),
			'add_new_item'      => __( 'Add New Age Group' ),
			'new_item_name'     => __( 'New Age Group' ),
			'menu_name'         => __( 'Age Groups' ),
		);
		$args = array(
			'labels' => $labels,
			'hierarchical' => true,
		);
		register_taxonomy( 'age-group', 'strengthentries', $args );
	}
	function bwl_taxonomies_challenges() {
		$labels = array(
			'name'              => _x( 'Challenges', 'taxonomy general name' ),
			'singular_name'     => _x( 'Challenge', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Challenges' ),
			'all_items'         => __( 'All Age Challenges' ),
			'parent_item'       => __( 'Parent Challenge' ),
			'parent_item_colon' => __( 'Parent Challenge:' ),
			'edit_item'         => __( 'Edit Challenge' ), 
			'update_item'       => __( 'Update Challenge' ),
			'add_new_item'      => __( 'Add New Challenge' ),
			'new_item_name'     => __( 'New Challenge' ),
			'menu_name'         => __( 'Challenges' ),
		);
		$args = array(
			'labels' => $labels,
			'hierarchical' => true,
		);
		register_taxonomy( 'challenges', 'strengthentries', $args );
	}	
	
// Email Settings
	function new_mail_from_name($old) {
		return 'British Weight Lifting';
	}

// WooCommerce Functions
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

// Extra Functions
	add_filter('single_template', create_function(
		'$the_template',
		'foreach( (array) get_the_category() as $cat ) {
			if ( file_exists(TEMPLATEPATH . "/single-{$cat->slug}.php") )
			return TEMPLATEPATH . "/single-{$cat->slug}.php"; }
		return $the_template;' )
	);	
	add_filter( 'post_class', 'wpse_2266_custom_taxonomy_post_class', 10, 3 );
	if ( ! function_exists('wpse_2266_custom_taxonomy_post_class') ) {
		function wpse_2266_custom_taxonomy_post_class($classes, $class, $ID) {
			$taxonomies_args = array(
				'public' => true,
				'_builtin' => false,
			);
			$taxonomies = get_taxonomies( $taxonomies_args, 'names', 'and' );
			$terms = get_the_terms( (int) $ID, (array) $taxonomies );
			if ( ! empty( $terms ) ) {
				foreach ( (array) $terms as $order => $term ) {
					if ( ! in_array( $term->slug, $classes ) ) {
						$classes[] = $term->slug;
					}
				}
			}
			$classes[] = 'clearfix';
			return $classes;
		}
	}
	function the_title_trim($title) {
		$title = attribute_escape($title);
		$findthese = array(
			'#Protected:#',
			'#Private:#'
		);
		$replacewith = array(
			'', // What to replace "Protected:" with
			'' // What to replace "Private:" with
		);
		$title = preg_replace($findthese, $replacewith, $title);
		return $title;
	}
	define('WOOCOMMERCE_USE_CSS', false);
	function IsPostcode($postcode) {
		return $postcode != '';
		/*$postcode = strtoupper(str_replace(' ','',$postcode));
		if(preg_match("/^[A-Z]{1,2}[0-9]{2,3}[A-Z]{2}$/",$postcode) || preg_match("/^[A-Z]{1,2}[0-9]{1}[A-Z]{1}[0-9]{1}[A-Z]{2}$/",$postcode) || preg_match("/^GIR0[A-Z]{2}$/",$postcode))
			return true;
		else
			return false;*/
	}
	function populate_posts($form){
	    foreach($form['fields'] as &$field){
	        if($field['type'] != 'select' || strpos($field['cssClass'], 'populate-posts') === false)
	            continue;
	        // you can add additional parameters here to alter the posts that are retreieved
	        // more info: http://codex.wordpress.org/Template_Tags/get_posts
	        $posts = get_posts('post_type=clubs&numberposts=-1');
	        // update 'Select a Post' to whatever you'd like the instructive option to be
	        $choices = array(array('text' => 'Unattached', 'value' => ' ', 'orderby' => 'post_title', 'order' => 'ASC'));
	        foreach($posts as $post){
	            $choices[] = array('text' => $post->post_title, 'value' => $post->post_title, 'orderby' => 'post_title', 'order' => 'ASC');
	        }
	        $field['choices'] = $choices;        
	    }
	    return $form;
	}

//Auto login after registration.
	function pi_gravity_registration_autologin( $user_id, $user_config, $entry, $password ) {
		$user = get_userdata( $user_id );
		$user_login = $user->user_login;
		$user_password = $password;
	    wp_signon( array(
			'user_login' => $user_login,
			'user_password' =>  $user_password,
			'remember' => false
	    ) );
	}

// Numbered Pagination
	function bwl_pagination() {
		if( is_singular() )
			return;
		global $wp_query;
		/** Stop execution if there's only 1 page */
		if( $wp_query->max_num_pages <= 1 )
			return;
		$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
		$max   = intval( $wp_query->max_num_pages );
		/**	Add current page to the array */
		if ( $paged >= 1 )
			$links[] = $paged;
		/**	Add the pages around the current page to the array */
		if ( $paged >= 3 ) {
			$links[] = $paged - 1;
			$links[] = $paged - 2;
		}
		if ( ( $paged + 2 ) <= $max ) {
			$links[] = $paged + 2;
			$links[] = $paged + 1;
		}
		echo '<div class="navigation"><ul>' . "\n";
		/**	Next Post Link */
		if ( get_previous_posts_link() )
			printf( '<li>%s</li>' . "\n", get_previous_posts_link('<') );
		/**	Link to first page, plus ellipses if necessary */
		if ( ! in_array( 1, $links ) ) {
			$class = 1 == $paged ? ' class="active"' : '';
			printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
			if ( ! in_array( 2, $links ) )
				echo '<li>…</li>';
		}
		/**	Link to current page, plus 2 pages in either direction if necessary */
		sort( $links );
		foreach ( (array) $links as $link ) {
			$class = $paged == $link ? ' class="active"' : '';
			printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
		}
		/**	Link to last page, plus ellipses if necessary */
		if ( ! in_array( $max, $links ) ) {
			if ( ! in_array( $max - 1, $links ) )
				echo '<li>…</li>' . "\n";

			$class = $paged == $max ? ' class="active"' : '';
			printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
		}
		/**	Previous Post Link */
		if ( get_next_posts_link() )
			printf( '<li>%s</li>' . "\n", get_next_posts_link('>') );
		echo '</ul></div>' . "\n";
	}

	function my_login_logo() { ?>
	    <style type="text/css">
	        body.login div#login h1 a {
	            background-image: url(<?php echo get_bloginfo( 'template_directory' ) ?>/images/favicons/apple-touch-icon-120x120.png);
				display: block;
				background-size: contain;
	        }
	    </style>
	<?php }
	function my_login_logo_url() {
	    return get_bloginfo( 'url' );
	}
	add_filter( 'login_headerurl', 'my_login_logo_url' );
	// First, create a function that includes the path to your favicon
	function add_favicon() {
	  	$favicon_url = get_stylesheet_directory_uri() . '/images/favicons/favicon.ico';
		echo '<link rel="shortcut icon" href="' . $favicon_url . '" />';
	}
	
?>
