<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/jquery-1.8.2.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lib/modernizr-latest.js" type="text/javascript"></script>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width"/>  
<?php get_template_part('includes/includes', 'favicon'); ?>
<title><?php bloginfo('name'); ?></title>
<?php wp_head(); ?>
</head>
<body <?php body_class('bwl-theme'); ?>>
	<section class="page-content">
		<nav class="tablet-nav">
			<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>	
		</nav>
		<main>
		<?php if(!is_user_logged_in()) { ?>
			<div class="overlay" id="login">
				<div class="lightbox-container">
					<a href="#" class="lightbox-close"><i class="fa fa-times-circle"></i></a>
					<?php dynamic_sidebar( 'login-area' ); ?>
				</div>
			</div>
		<?php } ?>
		<section class="footer-fix">
			<header>
				<section class="header-meta">
					<section class="container">
						<aside class="header-links">
							<ul>
								<li class="tab-nav-trigger"><a href="#"><i class="fa fa-bars"></i> Menu</a></li>
								<li>
									<?php if(!is_user_logged_in()) { ?>
										<a href="#" class="login-trigger">Login</a>
									<?php } else { ?>
										<a href="<?php bloginfo('url'); ?>/my-account/">My Account</a>
									<?php } ?>
								</li>
								<li><a href="<?php bloginfo('url'); ?>/contact-bwl/">Contact BWL</a></li>
							</ul>
						</aside>
						<aside class="header-socials">
							<ul>
								<li><a href="https://twitter.com/gbweightlifting" target="_blank"><i class="fa fa-twitter-square"></i></a></li>
								<li><a href="https://www.facebook.com/bwl.org" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
								<li><a href="https://www.youtube.com/user/GBWeights" target="_blank"><i class="fa fa-youtube-square"></i></a></li>
							</ul>
						</aside>
					</section>
				</section>
				<section class="header-main">
					<section class="container">
						<aside class="logo">
							<?php if(is_front_page()) { ?>
								<h1><a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a></h1>
							<?php } else { ?>
								<h2><a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a></h2>
							<?php } ?>
						</aside>
						<aside class="header-ad">
							<?php 
                                $args = array(
                                    'post_type'	=> 'adverts',
                                    'showposts'	=> 1,
									'orderby'	=> 'rand',
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'advert-category',
                                            'field' => 'slug',
                                        	'terms' => '800x100'
                                    	)
                                	)
                                );  
                                $the_query = new WP_Query( $args );
                            	if($the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); 
                            ?>
	                        	<a href="<?php the_field('target'); ?>" title="<?php the_title(); ?>" target="_blank"><?php the_post_thumbnail('full'); ?></a>
                            	<?php wp_reset_postdata(); ?>
                            <?php endwhile; endif; ?>
						</aside>
					</section>
				</section>
				<nav class="header-nav">
					<section class="container">
						<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>	
					</section>
				</nav>
			</header>
