<?php
/**
 * PHP class to show how you can use 4 popular map sites to
 * retrieve coordinates from a UK postcode.
 */
class PostcodeLookup
{
    var $sites;


    public function PostcodeLookup() {
	
        $this->sites = array(
			/*array('url'=>'http://maps.google.com/?q=%s',
                'pattern'=>'/geocode:"[^,]+,([0-9.-]+),([0-9.-]+)"/',
                'name'=>'Google')*/
                array('url'=>'http://www.streetmap.co.uk/streetmap.dll?GridConvert&name=%s&type=PostCode',
                    'pattern'=>'/\( ([0-9.-]+) \).*?\( ([0-9.-]+) \)/',
                    'name'=>'Streetmap'),
                
                array('url'=>'http://maps.yahoo.com/services/1.0.0/location?appid=ymapsaura&flags=HJD&count=30&q=%s&r=1',
                    'pattern'=>'/"latitude":"([0-9.-]+)","longitude":"([0-9.-]+)"/',
                    'name'=>'Yahoo'),
                array('url'=>'http://www.multimap.com/API/geocode/1.2/public_api?output=json&callback=MMGeocoder.prototype._GeneralJSONCallback&qs=%s&countryCode=GB',
                    'pattern'=>'/"lat":"([0-9.-]+)","lon":"([0-9.-]+)"/',
                    'name'=>'MultiMap') 
            );


    }

    public function getCoordinates($postcode) {
        // create context for HTTP requests
			$zip = urlencode($postcode); // post code to look up in this case status however can easily be retrieved from a database or a form post
		    $request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=".$zip."&sensor=true"; // the request URL you'll send to google to get back your XML feed
		    $xml = simplexml_load_file($request_url) or die("url not loading");// XML request
		    $status = $xml->status;// GET the request status as google's api can return several responses
		    if ($status=="OK") {
		        //request returned completed time to get lat / lang for storage
		        $lat = $xml->result->geometry->location->lat;
		        $long = $xml->result->geometry->location->lng;
				return array('lat'=>$lat, 'lon'=>$long, 'site_name'=>'Google');
		    }
		    if ($status=="ZERO_RESULTS") {
		        //indicates that the geocode was successful but returned no results. This may occur if the geocode was passed a non-existent address or a latlng in a remote location.
		    	return $this->use_fallbacks($postcode);
			}
		    if ($status=="OVER_QUERY_LIMIT") {
		        //indicates that you are over your quota of geocode requests against the google api
				return $this->use_fallbacks($postcode);
		    }
		    if ($status=="REQUEST_DENIED") {
		        //indicates that your request was denied, generally because of lack of a sensor parameter.
				return $this->use_fallbacks($postcode);
		    }
		    if ($status=="INVALID_REQUEST") {
		        //generally indicates that the query (address or latlng) is missing.
				return $this->use_fallbacks($postcode);
		    }
    }

	public function use_fallbacks($postcode) {
	 $context = stream_context_create(array(
        'http'=>array(
            'method'=>'GET',
            'timeout'=>10,
            'header'=>"Accept-language: en\r\n" .
                    "User-Agent: Mozilla/5.0\r\n"
        )
        ));
        // shuffle sites within our array
        shuffle($this->sites);
        // loop through sites and attempt to retrieve coordinates
        foreach ($this->sites as $site) {
            extract($site);
            // send HTTP request
            $response = @file_get_contents(sprintf($url, urlencode($postcode)), false, $context);
            // if it contains the information we're after (coordinates)...
            if (preg_match($pattern, $response, $matches)) {
                // return them
                return array('lat'=>$matches[1], 'lon'=>$matches[2], 'site_name'=>$name);
            } else {

                // otherwise, tell us which site failed and continue checking the rest
               //echo "$name failed!\n";
            }
        }
}
	
	// Public function so we can cache postcode lookups
	public function isinDatabase($postcode)
	{
		global $wpdb;
		$r = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "submitted_postcodes WHERE postcode = '" . str_replace(" ", "", strtoupper(trim($postcode))) . "'" );
		if($r)
		{
			return array('lon' => $r->lon, 'lat' => $r->lat);
		} 
		return false;
	}

	// Public function so we can cache postcode lookups	
	public function addToDatabase($postcode, $coords) {
		global $wpdb;
		
		$r = $wpdb->insert($wpdb->prefix . "submitted_postcodes", array(
			'postcode' => str_replace(" ", "", strtoupper(trim($postcode))),
			'lon' => $coords['lon'],
			'lat' => $coords['lat']
		),
		array(
			'%s',
			'%s',
			'%s'
		)
		);
		
	/*	if(!$r) {
			 $wpdb->show_errors();
			$wpdb->print_error();
			echo 'FAILED TO ADD TO DATABASE';
		} */
	}
}
?>
