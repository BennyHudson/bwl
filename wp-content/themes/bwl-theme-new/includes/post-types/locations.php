<?php add_action('init', 'post_type_clubs');

/*-----------------------------------------------------------------------------------
Custom Post Type - Dresses
-------------------------------------------------------------------------------------*/

	function post_type_clubs() {
		$labels = array(
			'name' 					=> _x( 'Clubs', 'post type general name'),
			'singular_name' 		=> _x( 'Club', 'post type singular name'),
			'add_new' 				=> _x( 'Add New Club', 'book'),
			'add_new_item'			=> ( 'Add New Club' ),
			'edit_item'				=> ( 'Edit Club' ),
			'new_item'				=> ( 'New Club' ),
			'all_items'				=> ( 'All Clubs' ),
			'view_item'				=> ( 'View Club' ),
			'search_items'			=> ( 'Search Clubs' ),
			'not_found'				=> ( 'No clubs found' ),
			'not_found_in_trash'	=> ( 'No clubs found in trash' ),
			'parent_item_colon'		=> '',
			'menu_name'				=> 'Clubs'
		);
		$args = array(
			'labels'        => $labels,
			'description'   => 'Holds our clubs and club specific data',
			'public'        => true,
			'menu_position' => 7,
			'menu_icon'		=> 'dashicons-location',
			'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
			'has_archive'   => true,
		);
		register_post_type( 'clubs', $args );
	}

/*-----------------------------------------------------------------------------------
Custom Post Type - Meta Boxes
-------------------------------------------------------------------------------------*/
if($postType == 'clubs' || $_POST['post_type'] == 'clubs') 
	{
		$meta_box = array(
		'id' => 'product-meta-box',
		'title' => 'Club Info',
		'page' => 'clubs',
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			
			/*--  */
			array(
				'type' => 'open_tab_panel'
				),
			array(
				'name' => 'Address Line 1',
				'id' => 'address1',
				'type' => 'text',
				'std' => ''
			),
			array(
				'name' => 'Address Line 2',
				'id' => 'address2',
				'type' => 'text',
				'std' => ''
				),
			array(
				'name' => 'Town/City',
				'id' => 'town',
				'type' => 'text',
				'std' => ''
				),
			array(
				'name' => 'County',
				'id' => 'county',
				'type' => 'text',
				'std' => ''
				),
			array(
				'name' => 'Postcode',
				'id' => 'zip',
				'type' => 'text',
				'std' => ''
				),
			array(
				'name' => 'Club Website',
				'id' => 'url',
				'type' => 'text',
				'std' => ''
				),
			array(
				'name' => 'Club Email',
				'id' => 'clubmail',
				'type' => 'text',
				'std' => ''
				),
			array(
				'name' => 'Club Telephone',
				'id' => 'tel',
				'type' => 'text',
				'std' => ''
				),
			array(
				'name' => 'Club Facebook',
				'id' => 'facebook',
				'type' => 'text',
				'std' => ''
				),
			array(
				'name' => 'Club Twitter',
				'id' => 'twitter',
				'type' => 'text',
				'std' => ''
				),
			array(
				'name' => 'Licenced?',
				'id' => 'licence',
				'type' => 'select',
				'options' => array( // array of name, value pairs for select box
									array('name' => 'Licenced', 'value' => 'Licenced'),
									array('name' => 'Unlicenced', 'value' => 'Unlicenced')
							),
				'std' => ''
				),
			array(
				'name' => 'Coach Name',
				'id' => 'coachname',
				'type' => 'text',
				'std' => ''
				),
			array(
				'name' => 'Coach Licence Number',
				'id' => 'coachid',
				'type' => 'text',
				'std' => ''
				),
			array(
				'name' => 'DBS Number',
				'id' => 'dbs',
				'type' => 'text',
				'std' => ''
				),
			array(
				'name' => 'DBS Issue Date',
				'id' => 'dbsexp',
				'type' => 'text',
				'std' => ''
				),
			array(
				'name' => 'First Aid Certificate Expiry',
				'id' => 'firstaidexp',
				'type' => 'text',
				'std' => ''
				),
			array(
				'name' => 'Safeguarding Expiry',
				'id' => 'safeguardingexp',
				'type' => 'text',
				'std' => ''
				),
			array(
				'name' => 'Secretary Title',
				'id' => 'sectitle',
				'type' => 'text',
				'std' => ''
				),
			array(
				'name' => 'Secretary First Name',
				'id' => 'secname',
				'type' => 'text',
				'std' => ''
				),
			array (
				'name' => 'Secretary Last Name',
				'id' => 'secsurname',
				'type' => 'text',
				'std' => ''
				),
			array(
				'name' => 'Secretary Email',
				'id' => 'secemail',
				'type' => 'text',
				'std' => ''
				),
			array(
				'name' => 'Secretary Telephone',
				'id' => 'sectel',
				'type' => 'text',
				'std' => ''
				),
			array(
				'name' => 'Map',
				'id' => 'location',
				'type' => 'google_map',
				'std' => ''
				),
			array(
				'name' => 'Region',
				'id' => 'region',
				'type' => 'select',
				'options' => array( // array of name, value pairs for select box
									array('name' => 'England - North', 'value' => 'England - North'),
									array('name' => 'England - Central', 'value' => 'England - Central'),
									array('name' => 'England - London &amp; South East', 'value' => 'England - London and South East'),
									array('name' => 'England - South West', 'value' => 'England - South West'),
									array('name' => 'Scotland', 'value' => 'Scotland'),
									array('name' => 'Wales', 'value' => 'Wales'),
									array('name' => 'Northern Ireland', 'value' => 'Northern Ireland')
							),
				'std' => ''
				),
			array(
				'name' => 'Club Caters For',
				'id' => 'catering',
				'type' => 'checkbox_group',
				'options' => array( // array of name, value pairs for select box
									array('name' => 'Adults', 'value' => 'Adults'),
									array('name' => 'Under 18s', 'value' => 'Under 18s'),
									array('name' => 'Vulnerable Adults', 'value' => 'Vulnerable Adults')
							),
				'std' => ''
				),
			array(
				'name' => 'Disciplines',
				'id' => 'disciplines',
				'type' => 'checkbox_group',
				'options' => array( 
									array('name' => 'Olympic Weightlifting', 'value' => 'Olympic Weightlifting'),
									array('name' => 'Weight Lifting for Sports Performance', 'value' => 'Weight Lifting'),
									array('name' => 'CrossFit', 'value' => 'Crossfit'),
									array('name' => 'Weight Training', 'value' => 'Weight Training'),
									array('name' => 'Powerlifting', 'value' => 'Powerlifting'),
									array('name' => 'Body Building', 'value' => 'Body Building'),
									array('name' => 'Disability Weight lifting', 'value' => 'Disability Weight Lifting'),
									array('name' => 'Paralympic Powerlifting', 'value' => 'Paralympic Powerlifting')
								),
				'std' => ''
				),
			array(
				'name' => 'Working towards club mark?',
				'id' => 'clubmark',
				'type' => 'select',
				'options' => array( // array of name, value pairs for select box
									array('name' => 'Yes', 'value' => 'Yes'),
									array('name' => 'No', 'value' => 'No')
							),
				'std' => ''
				),
			array(
				'type' => 'close_tab_panel'
			),
	
		
		
	
		)
	);
}
if($postType == 'strengthentries' || $_POST['post_type'] == 'strengthentries') 
	{
		$meta_box = array(
		'id' => 'entrant-meta-box',
		'title' => 'Entrant Details',
		'page' => 'strengthentries',
		'context' => 'normal',
		'priority' => 'high',
		'fields' => array(
			
			/*--  */
			array(
				'type' => 'open_tab_panel'
				),
			array(
				'name' => 'Date of Birth',
				'id' => 'entrant-dob',
				'type' => 'text',
				'std' => ''
			),
			array(
				'name' => 'Gender',
				'id' => 'entrant-gender',
				'type' => 'select',
				'options' => array(
								array('name' => 'Male', 'value' => 'male'),
								array('name' => 'Female', 'value' => 'female')
							),
				'std' => ''
			),
			array(
				'name' => 'Gym/Club Name',
				'id' => 'entrant-gym-name',
				'type' => 'text',
				'std' => ''
			),
			array(
				'name' => 'Email Address',
				'id' => 'entrant-email',
				'type' => 'text',
				'std' => ''
			),
			array(
				'name' => 'Telephone Number',
				'id' => 'entrant-telephone',
				'type' => 'text',
				'std' => ''
			),
			array(
				'name' => 'Institution',
				'id' => 'entrant-institution',
				'type' => 'text',
				'std' => ''
			),
			array(
				'name' => 'Score',
				'id' => 'entrant-score',
				'type' => 'text',
				'std' => ''
			),
			array(
				'name' => 'Date of Exercise',
				'id' => 'entrant-exercise-date',
				'type' => 'text',
				'std' => ''
			),
			array(
				'name' => 'Primary Sport/Activity',
				'id' => 'entrant-primary-sport',
				'type' => 'text',
				'std' => ''
			),
			array(
				'name' => 'Video of Exercise',
				'id' => 'entrant-exercise-video',
				'type' => 'text',
				'std' => ''
			),
			array(
				'name' => 'Disabilities',
				'id' => 'entrant-disabilities',
				'type' => 'text',
				'std' => ''
			),
			array(
				'name' => 'How did you hear about the Strength & Power Series',
				'id' => 'entrant-how-hear',
				'type' => 'text',
				'std' => ''
			),
			array(
				'type' => 'close_tab_panel'
			),
		)
	);
}
/*-----------------------------------------------------------------------------------
Custom Post Type - Save Action
-------------------------------------------------------------------------------------*/

function update_geocode_info( $post_id ) {
	
	if(get_post_type($post_id) == 'clubs') {
		if($_POST['location']['longittue'] != '' || $_POST['location']['latitude'] != '') {
			global $wpdb;
			$cleanse = $wpdb->query('DELETE from ' . $wpdb->prefix . 'postcodes WHERE post_id = ' . $post_id);
			$result = $wpdb->insert($wpdb->prefix . 'postcodes', 
				array(
					'post_id' => $post_id,
					'zip' => $_POST['zip'],
					'lng' => $_POST['location']['latitude'],
					'lat' => $_POST['location']['longittue'],
				),
				array(
					'%d', 
					'%s',
					'%s',
					'%s'
				)
			);	
			update_post_meta($post_id, 'location', array('lon'=>$_POST['advert_location']['longittue'],'lat'=>$_POST['advert_location']['latitude']));
		}
	}
	
}


add_action( 'save_post', 'update_geocode_info' );





?>