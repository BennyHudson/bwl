<?php
// Add meta box
function advanced_add_box() 
{
	global $meta_box;
 	add_meta_box($meta_box['id'], $meta_box['title'], 'advanced_show_box', $meta_box['page'], $meta_box['context'], $meta_box['priority']);
}
 
 
// Callback function to show fields in meta box
function advanced_show_box() {
	global $meta_box, $post, $postType;

	// Use nonce for verification
	echo '<input type="hidden" name="advanced_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';
	echo '<input type="hidden" name="postType" value="' . $postType . '" />';
	
 	foreach ($meta_box['fields'] as $field) {
		// get current post meta data
		$meta = get_post_meta($post->ID, $field['id'], true);
 
		/* -- Tabs */
		if($field['type']=='tabs') { 
			echo '<div id="advanced_tabset">'; 
				echo '<ul>';
				$i=0;
					foreach($field['labels'] as $k=>$v) {
						if($i==0) { 
							echo '<li ><a class="active" href="#">',$v,'</a></li>';
						} else { 
							echo '<li><a href="#">',$v,'</a></li>';	
						}
						$i++;
					}
				echo '</ul>';
			echo '</div>';
		}

	
		/* -- Open Panel */
		if($field['type']=='open_tab_panel') { echo '<div class="advanced_tabset_panel ',$field['std'],'"><table class="form-table">';  }
		
		echo '<tr>',
				'<th style="width:20%"><label for="', $field['id'], '">', $field['name'], '</label></th>',
				'<td>';
		switch ($field['type']) 
		{

			// --- Text
			case 'text':
				echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />',
				'<br />', $field['desc'];
			break;
			
				// --- Checkbox
				
				// checkbox_group
				case 'checkbox_group':
					foreach ($field['options'] as $option) {
						echo '<input type="checkbox" value="'.$option['value'].'" name="'.$field['id'].'[]" id="'.$option['value'].'"',$meta && in_array($option['value'], $meta) ? ' checked="checked"' : '',' /> 
								<label for="'.$option['value'].'">'.$option['name'].'</label><br />';
					}
					echo '<span class="description">'.$field['desc'].'</span>';
				break;
				
				// --- Select
				
				case 'select':
					echo '<select name="', $field['id'], '" id="', $field['id'], '">';
					foreach ($field['options'] as $option) {
						echo '<option value="', $option['value'], '"', $meta == $option['value'] ? ' selected="selected"' : '', '>', $option['name'], '</option>';
					}
					echo '</select>';
                break;

			// --- Textareas
			case 'textarea':
				echo '<textarea name="', $field['id'], '" id="', $field['id'], '" cols="60" rows="4" style="width:97%">', $meta ? $meta : $field['std'], '</textarea>',
				'<br />', $field['desc'];
			break;
			
			// --- Button
			case 'button':
				echo '<input type="button" class="upload_button" name="', $field['id'], '" id="', $field['id'], '"value="', $meta ? $meta : $field['std'], '" />';
			break;
			
			case 'image_picker':
				if($meta) {
					echo '<img src="' . $meta . '" style="max-width:500px" alt="" />';
				}
			
				echo '<div class="img_picker"><input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:80%" />';
				
				echo '<input type="button" class="upload_button" value="Browse" /><br /> ' . $field['desc'] . '</div>';
			
			break;
			
			// --- Multi Uploader
			case 'multi_upload':
				echo $field['desc'] ."<br/>";
				if($meta) {
					echo "<div class='current_docs' style='border:1px solid #ccc;background:#fff;padding:5px;margin-bottom:15px'><b><small>Current Docs</small></b><br/><ul>";
					foreach($meta as $k => $v) {
						echo '<input type="hidden" name="', $field['id'], '[]" value="',$v,'" />';
						// -- Strip URL
						if($v) {
							$parts=explode('/',$v);
							if($parts) {
								$filename = $parts[sizeof($parts)-1];
							} else {
								$filename = $v;
							}
						
							echo '<li class="' . $k . '|' . $post->ID . '|' . $field['id'] . '">'.$filename.' > <a target="_blank" href="'.$v.'">View</a> | <a class="remove_advanced_asset" href="#">Remove</a></li>';
						}
					}
					echo '</ul></div>';
				}
				
				echo '<div class="multi_uploader_wrapper copyable"><input type="text" name="', $field['id'], '[]" id="', $field['id'], '" size="30" style="width:75%" />';
				echo '<input type="button" class="upload_button" name="', $field['id'], '" id="', $field['id'], '"value="Browse" /></div>';
				echo '<a class="advanced_add_another" href="#">+ Add Another</a>';
			break;
			

			

			// --- Multi Uploader
			case 'multi_add':
				echo $field['desc'] ."<br/>";
				if($meta) {
					foreach($meta as $k => $v) {
						if($v!='')
							echo '<div class="multi_text_wrapper"><input type="text" name="', $field['id'], '[]" id="', $field['id'], '" value="',$v,'" size="30" style="width:75%" /> <a href="#" class="delete_input">[x] Remove</a> ';
					}
				}
				
				echo '<div class="multi_text_wrapper copyable"><input type="text" name="', $field['id'], '[]" id="', $field['id'], '" size="30" style="width:75%" />';
				echo '<br/><a class="multi_text_add" href="#">+ Add Another</a>';
			break;
			
			
			// --- Multi Uploader
			case 'gallery_picker':
				echo $field['desc'] ."<br/>";
				echo '<ul class="sortable">';
				if($meta) {
					foreach($meta as $k => $v) {
						if($v!='') {
							$att_src = get_attachment_id_from_src($v);
							$prev_src = wp_get_attachment_image_src($att_src, 'dress_thumbs');
							echo '<li style="float:left;padding:5px;margin:5px;min-height:150px;min-width:100px;border:1px solid #333"><img src="' . $prev_src[0]  . '" style="max-width:100px;float:left" alt="" /><br/><input type="hidden" name="', $field['id'], '[]" id="', $field['id'], '" value="',$v,'" size="30" style="float:left;" /> <a href="#" class="delete_input">Remove [x]</a></li>';
						}
					}
				}
				echo '</ul>';
				
				echo '<div class="multi_uploader_wrapper copyable" style="clear:both"><input type="text" name="', $field['id'], '[]" id="', $field['id'], '" size="30" style="width:75%" />';
				echo '<input type="button" class="upload_button" name="', $field['id'], '" id="', $field['id'], '"value="Browse" /></div>';
				echo '<a class="advanced_add_another" href="#">+ Add Another</a>';
			break;
			
			case 'fitting':
				global $fitting_icons;
				echo $field['desc'] ."<br/>";
				foreach($fitting_icons as $k => $v) {
					$chk = (in_array($k,$meta)) ? ' checked="checked" ' : '';
					echo '<input ',$chk,' type="checkbox"  name="', $field['id'], '[]"  value="' . $k . '" />' . $v['title'] . '<br/>';
				}
		

				/*echo $field['desc'] ."<br/>";

				$options = array(

				)

				global $ar_countries;
				echo '<select name="', $field['id'], '">';
				foreach($ar_countries as $k => $v) {
					
					if($k == $meta) {
						echo '<option selected="selected" value="' . $k . '">' . $v . '</option>';
					} else {
						echo '<option value="' . $k . '">' . $v . '</option>';
					}
					
					
				}
				echo '</select>'; */
			break;
			
			case 'google_map':
			?>
		    	<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
			    <script>
				 var geocoder;
			      var map;
				 var marker;
				  jQuery(document).ready(function($) {
					
					    geocoder = new google.maps.Geocoder();
						<?php if(!$meta || $meta['longittue']=='' || $meta['latitude']=='') : ?>
				        	var latlng = new google.maps.LatLng(51.5073346,-0.12768310000001293);
						<?php else: ?>
							var latlng = new google.maps.LatLng(<?php echo $meta['longittue']; ?>,<?php echo $meta['latitude']; ?>);
						<?php endif; ?>
				        var mapOptions = {
				          zoom: 10,
				          center: latlng,
				          mapTypeId: google.maps.MapTypeId.ROADMAP
				        }
				        map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions)
				
						marker = new google.maps.Marker({
			                map: map,
							draggable : true,
			                position:latlng
			            });
			
						 google.maps.event.addListener(marker, "dragend", function(event) { 
						    updateFields( event.latLng.lat(), event.latLng.lng() )
						 });
				
				  })
			     
			     
			      function codeAddress() {
					if(marker) {
						marker.setMap(null)
					}
				
			        var address = document.getElementById('geocoder').value;
			        geocoder.geocode( { 'address': address}, function(results, status) {
			          if (status == google.maps.GeocoderStatus.OK) {
			            map.setCenter(results[0].geometry.location);
			            marker = new google.maps.Marker({
			                map: map,
							draggable : true,
			                position: results[0].geometry.location
			            });
						 
						updateFields(  results[0].geometry.location.lat(), results[0].geometry.location.lng())
						
						google.maps.event.addListener(marker, "dragend", function(event) { 
						   updateFields( event.latLng.lat(), event.latLng.lng() )
						});
			
			          } else {
			            alert('Geocode was not successful for the following reason: ' + status);
			          }
			        });
			      }
			
				function updateFields(lat,lng) {
					
						 jQuery('#_lng').attr('value', lng);
						 jQuery('#_lat').attr('value', lat);
				}
				
			
			    </script>
			     <div>
				      <input id="geocoder" type="textbox" value=""> 
				      <input type="button" value="Create Map" onclick="codeAddress()"> (Enter Postcode)
					 	
				 </div>
			     <div id="map_canvas" style="height:300px;width:100%"></div>

				<input type="text" name="<?php echo $field['id']; ?>[latitude]" id="_lng" size="30" value="<?php echo @$meta['latitude']; ?>" style="width:75%" />
				<input type="text" name="<?php echo $field['id']; ?>[longittue]" id="_lat" value="<?php echo @$meta['longittue']; ?>" size="30" style="width:75%" />
				
			<?php
			break;
			
			// --- Date Picker
			case 'date_picker':			
				echo '<p>',$field['desc'] ,'</p>';		
				if($meta) {
					$meta =strftime('%d/%m/%Y',$meta);
				}	
				echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" class="date_picker" value="', $meta ? $meta : $field['std'], '" size="30" style="width:30%" /><br/>';			
			break;
			
			
			case 'wisywig' :
				echo '<p>',$field['desc'] ,'</p>';
				$ad_wisyig_settings = array(
				    'textarea_name' => $field['id'],
				    'media_buttons' => false,
					'textarea_rows' => 20,
					'quicktags' => false
				);
				 wp_editor( $meta, $field['id'], $ad_wisyig_settings);
			break;
				
		}
		echo 	'</td>',
			'</tr>';
		/* -- Close Panel */
			echo '';
		if($field['type']=='close_tab_panel') { echo '</table></div>'; }
	}
 
}
 

add_action('admin_menu', 'advanced_add_box');


// Save data from meta box
function advanced_save_data($post_id) 
{
		global $meta_box;

		// verify nonce
		if (!wp_verify_nonce($_POST['advanced_meta_box_nonce'], basename(__FILE__))) {
			return $post_id;
		}
	
		// check autosave
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return $post_id;
		}
	 
		
	
		// check permissions
		if ('page' == $_POST['post_type']) {
			if (!current_user_can('edit_page', $post_id)) {
				return $post_id;
			}
		} elseif (!current_user_can('edit_post', $post_id)) {
			return $post_id;
		}
		
	
		
		
	 
		foreach ($meta_box['fields'] as $field) {
			$old = get_post_meta($post_id, $field['id'], true);
			$new = $_POST[$field['id']];
	 
			if ($new && $new != $old) {
				update_post_meta($post_id, $field['id'], $new);
			} elseif ('' == $new && $old) {
				delete_post_meta($post_id, $field['id'], $old);
			}
		}
		
			if( $_POST['post_type'] == 'events') {
					$date=trim($_POST['event_date']);
					list($d, $m, $y) = explode('/', $date);
					$mk=mktime(0, 0, 0, $m, $d, $y);
					update_post_meta($post_id, 'event_date', $mk);
					$date=trim($_POST['event_end_date']);
					list($d, $m, $y) = explode('/', $date);
					$mk=mktime(0, 0, 0, $m, $d, $y);
					update_post_meta($post_id, 'event_end_date', $mk);
			}
}

add_action('save_post', 'advanced_save_data');


?>