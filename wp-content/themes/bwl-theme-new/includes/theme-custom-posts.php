<?php
// --- Set Global Post Type
$postType = (isset($_GET['post_type'])) ? $_GET['post_type'] : get_post_type($_GET['post']);

// --- Custom Post Types
include 'post-types/locations.php';

// --- Metaboxes & Panel Writers
include 'post-types/metaboxes.php';
?>