<?php get_header(); ?>
	<section class="container">
		<aside class="page-main">
			<?php if (have_posts()) : ?>
				<h1 class="page-title">
					<?php 
						$cat_name = single_cat_title( '', false ); 
						echo $cat_name;
					?>
				</h1>
	            <?php $post = $posts[0]; $c=0;?>
	            <?php while (have_posts()) : the_post(); ?>
	            	<?php $c++;
	                    if( !$paged && $c == 1) :?>
	                    <article class="feature-post">
	                    	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('full'); ?></a>
	                        <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
	                        <span class="the-date"><?php the_time('l jS F Y'); ?></span>            
	                        <section class="excerpt-clip">
	                        	<?php the_excerpt(); ?>
	                        </section>
	                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="button red read-more">Read more</a>
	                    </article>
	                <?php else :?>
	                	<article class="standard-post">
	                		<div class="feature-clip">
	                			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('medium'); ?></a>
	                		</div>
	                    	<section class="title-clip">
		                    	<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
	                        </section>
	                        <span class="the-date"><?php the_time('l jS F Y'); ?></span> 
	                        <div class="excerpt-clip">
	                        	<?php the_excerpt(); ?>
	                        </div>
	                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="button red read-more">Read more</a>
	                    </article>
	                <?php endif;?>
	            <?php endwhile; ?>
	            <section class="pagination">
	        		<?php bwl_pagination(); ?>
	        	</section>
	        <?php endif; ?>
        </aside>
        <aside class="page-sidebar">
			<?php get_sidebar(); ?>
		</aside>
	</section>
<?php get_footer(); ?>
