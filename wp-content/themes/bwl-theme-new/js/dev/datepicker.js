jQuery(function($) {
    var gd_datepicker_loaded = $('body').hasClass('gd-multi-datepicker') ? true : false;
	//console.log('is_multiple : ' + gd_datepicker_loaded);
	var gdcnt = 0;
	$('.geodir-listing-search #event_start').each(function(){
		gdcnt++;
		$(this).attr('id', 'event_start'+gdcnt);
		$(this).addClass('gd-datepicker-event-start');
	});
	
	var gdcnt = 0;
	$('.geodir-listing-search #event_end').each(function(){
		gdcnt++;
		$(this).attr('id', 'event_end'+gdcnt);
		$(this).addClass('gd-datepicker-event-end');
	});
	
	if(!gd_datepicker_loaded){
		$('body').addClass('gd-multi-datepicker');
		
		$('.gd-datepicker-event-start').each(function(){
			var $this = this;
			$($this).datepicker({
				dateFormat:'yy-mm-dd',changeMonth: true, changeYear: true,
				onClose: function( selectedDate ) {
					$($this).closest('div').find('.gd-datepicker-event-end').datepicker( "option", "minDate", selectedDate );
				}
			});
		});
		
		$('.gd-datepicker-event-end').each(function(){
			$(this).datepicker({changeMonth: true, changeYear: true,dateFormat:'yy-mm-dd'});
		});
	}
});

	jQuery(document).ready(function(){
			jQuery('.expandmore').click(function(){
					var moretext = jQuery.trim(jQuery(this).text());
					
					jQuery(this).closest('ul').find('.more').toggle('slow')
					
					if(moretext=='More')
						jQuery(this).text('Less');
					else
						jQuery(this).text('More');
			});
	});
	

if (typeof window.gdShowFilters === 'undefined') {
    window.gdShowFilters = function(fbutton) {
		var $form = jQuery(fbutton).closest('form');
		jQuery(".customize_filter",$form).slideToggle("slow",function(){
			if(jQuery('.geodir_submit_search:first',$form).css('visibility') == 'visible')													
				jQuery('.geodir_submit_search:first',$form).css({'visibility':'hidden'});
			else
				jQuery('.geodir_submit_search:first',$form).css({'visibility':'visible'});	
		});
	
    }
}


	
	/* Show Hide Filters Start
jQuery(document).ready(function(){
	
	jQuery(".showFilters").click(function () { //alert(1);
		var $form = jQuery(this).closest('form');
		jQuery(".customize_filter",$form).slideToggle("slow",function(){
			if(jQuery('.geodir_submit_search:first',$form).css('visibility') == 'visible')													
				jQuery('.geodir_submit_search:first',$form).css({'visibility':'hidden'});
			else
				jQuery('.geodir_submit_search:first',$form).css({'visibility':'visible'});	
		});
	});
	
});*/


