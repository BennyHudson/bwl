$(document).ready(function() {

	$('.container').fitVids();

	var footerHeight = $('footer').outerHeight();
	//$('.footer-fix').css('padding-bottom', footerHeight + 20);
	
	$('.menu-item-has-children').hover(function() {
		$(this).children('ul').show();
	}, function() {
		$(this).children('ul').hide();
	});

	$('#feature-slider').bxSlider();

	$('.login-trigger, .showlogin').click(function(e) {
		e.preventDefault();
		$('.overlay#login').show();
	});

	$('.lightbox-close').click(function(e) {
		e.preventDefault();
		$('.overlay').hide();
	});
	$('.accordion-trigger').click(function(e) {
		e.preventDefault();
		if($(this).next('.accordion-content').hasClass('open')) {
			$('.accordion-content.open').hide().removeClass('open');
		} else {
			$('.accordion-content.open').hide().removeClass('open');
			$(this).next('.accordion-content').show().addClass('open');
		}
		var div = $(".overlay#member-register .lightbox-container").height();
		var win = $(window).height();
		if (div > win ) {
		    $("body").css('height', div + 120);
		} else {
			$('body').css('height', 'auto');
		}
	});
	if($('div.product').hasClass('product_cat-membership')) {
		$('.overlay#member-register').show();
	}
	$('ul.strength-power-tabs').find('li > a').each( function(i){
	  var postnum = i; // remove any possible overloaded "+" operator ambiguity
	  $(this).attr('data-slide-index', postnum);
	});
	$('.strength-power-tables').bxSlider({
		controls: false,
		adaptiveHeight: true,
		pagerCustom: '.strength-power-tabs',
		mode: 'fade'
	});
	$('#entry-trigger').click(function(e) {
		var series = $(this).data('lift');
		e.preventDefault();
		console.log(series);
		$('.strength-entry-form').slideDown();
	});
	$('.tab-nav-trigger a').click(function(e) {
		e.preventDefault();
        if($('.tablet-nav').hasClass('visible')) {
            $('main').removeClass('fixed-main');
            $(this).children('i').removeClass('fa-times').addClass('fa-bars');
            $('.tablet-nav').removeClass('visible'); 
        } else {
        	$('main').addClass('fixed-main');
            $(this).children('i').removeClass('fa-bars').addClass('fa-times');
            $('.tablet-nav').addClass('visible');
        }
	});

	$('.back-up').click(function(e) {
		e.preventDefault();
        $('html,body').animate({
            scrollTop: 0
        }, 500);
	});
});
$(window).scroll(function() {
	if ($(this).scrollTop() > 50) {
		$('.back-up').show();
	} else {
		$('.back-up').hide();
	}
});
$(document).bind('keyup', function(e) {
	if(e.keyCode == 27) {
		$('.lightbox-close').click();
	}
});
