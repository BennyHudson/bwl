<?php $recent = new WP_Query("page_id=2971"); while($recent->have_posts()) : $recent->the_post();?>
	<?php if(get_field('sidebar_ad_zones')) : ?>
		<?php while(the_repeater_field('sidebar_ad_zones')) : ?>
            <section class="widget">
                <a href="<?php the_sub_field('page_link'); ?>">
                	<img src="<?php the_sub_field('image'); ?>">
            	</a>
        	</section>
    	<?php endwhile; ?>
	<?php endif; ?>
<?php endwhile; ?>
<?php dynamic_sidebar( 'blog-sidebar' ); ?>
