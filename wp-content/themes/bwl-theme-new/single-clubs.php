<?php get_header(); ?>
	<section class="container">
		<aside class="page-main">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <?php $post_meta_data = get_post_custom($post->ID); ?>
                <?php $location =  get_post_meta($post->ID, 'location', true); ?>
				<h1 class="page-title"><?php the_title(); ?></h1>
				<?php the_content(); ?>
                <section class="club-content">
                    <section>
                    	<h2>Find Us</h2>
                    	<div class="bwl-map">
    						<div class="marker" data-lat=" <?php echo $location['longittue']; ?>" data-lng="<?php echo $location['latitude']; ?>"></div>
    					</div>
                        <aside class="club-overview">
                           	<p class="address"><span><?php the_title(); ?></span>, <?php echo get_post_meta($post->ID,'address1', true) ?>, <?php echo get_post_meta($post->ID,'address2', true) ?>, <?php echo get_post_meta($post->ID,'town', true) ?>, <?php echo get_post_meta($post->ID,'county', true); ?>, <?php echo get_post_meta($post->ID,'zip', true); ?></p>
                            <p>
    							<?php if(get_post_meta($post->ID,'tel', true)) : ?>
                                    <?php echo get_post_meta($post->ID,'tel', true); ?><br>
                                <?php endif; ?>
                                <?php if(get_post_meta($post->ID,'url', true)) : ?>
                                    <a target="_blank" href="http://<?php echo str_replace('http://','',get_post_meta($post->ID,'url', true)); ?>"><?php echo str_replace('http://','',get_post_meta($post->ID,'url', true)); ?></a><br>
    							<?php endif; ?>
                                <?php if(get_post_meta($post->ID,'facebook', true)) : ?>
                                    <a target="_blank" href="http://<?php echo str_replace('http://','',get_post_meta($post->ID,'facebook', true)); ?>"><?php echo str_replace('https://','',get_post_meta($post->ID,'facebook', true)); ?></a><br>
                                <?php endif; ?>
                                <?php if(get_post_meta($post->ID,'twitter', true)) : ?>
                                    <a target="_blank" href="http://<?php echo str_replace('http://','',get_post_meta($post->ID,'twitter', true)); ?>"><?php echo str_replace('https://','',get_post_meta($post->ID,'twitter', true)); ?></a><br>
                                <?php endif; ?>
                            </p>
                        </aside>
                    </section>
                    <section>
                    	<h2>Personnel</h2>
                        <h3>Secretary Info</h3>
                        <p><?php echo get_post_meta($post->ID,'secname', true) ?> <?php echo get_post_meta($post->ID, 'secsurname', true) ?><br>
                        <?php echo get_post_meta($post->ID,'sectel', true) ?><br>
                        <a href="mailto:<?php echo get_post_meta($post->ID,'clubmail', true) ?>"><?php echo get_post_meta($post->ID,'clubmail', true) ?></a>
                        <h3>Licence Info</h3>
                        <p><?php echo get_post_meta($post->ID,'licence', true); ?> club
                        	<?php if(get_post_meta($post->ID,'coachname', true)) : ?>
                            	<br><strong>Coach name:</strong> <?php echo get_post_meta($post->ID,'coachname', true); ?>
                            <?php endif; ?>
                        </p>
                    </section>
                    <section class="facilities">
                    	<h2>Facilities</h2>
                        <div>
                        	<aside class="double">
                                <h3>Which weight lifting disciplines does the club provide opportunities for?</h3>
                                <?php $custom_checkbox_group = unserialize($post_meta_data['disciplines'][0]); 
                                    echo '<ul>';
                                    foreach ($custom_checkbox_group as $string) {
                                        echo '<li>'.$string.'</li>';
                                    }
                                    echo '</ul>';
                                ?>
                            </aside>
                            <aside>
                            	<h3>Who does the club cater for?</h3>
                                <?php $custom_checkbox_group = unserialize($post_meta_data['catering'][0]); 
                                    echo '<ul>';
                                    foreach ($custom_checkbox_group as $string) {
                                        echo '<li>'.$string.'</li>';
                                    }
                                    echo '</ul>';
                                ?>
                                <h3>Is the club currently working towards Club Mark</h3>
                                <ul>
                                    <li><?php echo get_post_meta($post->ID,'clubmark', true); ?></li>
                                </ul>
                                <h3>Region</h3>
                                <ul>
                                    <li><?php echo get_post_meta($post->ID,'region', true); ?></li>
                                </ul>
                            </aside>
                        </div>
                    </section>
                </p>
			<?php endwhile; endif; ?>
		</aside>
		<aside class="page-sidebar">
			<?php get_sidebar(); ?>
		</aside>
	</section>
<?php get_footer(); ?>
