<?php get_header(); ?>
	<section class="container">
		<aside class="page-main">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<h1 class="page-title"><?php the_title(); ?></h1>
				<section class="single-feature">
					<?php the_post_thumbnail('full'); ?>
					<div class="post-meta">
						<aside>
							<ul>
								<li><span>Date: </span> <?php the_time('jS F Y'); ?></li>
							</ul>
						</aside>
						<aside>
							<div class="sharers">
								<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank" class="sharer facebook">Share <i class="fa fa-facebook"></i></a>
								<a href="https://twitter.com/intent/tweet?text=<?php the_title(); ?>%20via%20@gbweightlifting&url=<?php the_permalink(); ?>" class="sharer twitter" target="_blank">Tweet <i class="fa fa-twitter"></i></a>
								<a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank" class="sharer google">Plus <i class="fa fa-google-plus"></i></a>
							</div>
						</aside>
					</div>
				</section>
				<?php the_content(); ?>
				<section class="post-nav">
					<span class="post-button previous">
						<?php next_post('%', 'Previous', 'no'); ?>
					</span>
					<span class="post-button next">
						<?php previous_post('%', 'Next', 'no'); ?>
					</span>
				</section>
			<?php endwhile; endif; ?>
		</aside>
		<aside class="page-sidebar">
			<?php get_sidebar(); ?>
		</aside>
	</section>
<?php get_footer(); ?>
