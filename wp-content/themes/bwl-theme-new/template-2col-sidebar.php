<?php 
	// Template Name: Two Column (With Sidebar)
	get_header(); 
?>
	<section class="container">
		<aside class="page-main">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<h1 class="page-title"><?php the_title(); ?></h1>
				<?php if(has_post_thumbnail()) { ?>
					<section class="single-feature">
						<?php the_post_thumbnail('full'); ?>
					</section>
				<?php } ?>
				<?php the_content(); ?>
				<section class="column-area">
	            	<aside class="col1of2">
	                	<?php the_field('content_area_1'); ?>
	                </aside>
	                <aside class="col1of2">
	                	<?php the_field('content_area_2'); ?>
	                </aside>
	            </section>
			<?php endwhile; endif; ?>
		</aside>
		<aside class="page-sidebar">
			<?php get_sidebar(); ?>
		</aside>
	</section>
<?php get_footer(); ?>
