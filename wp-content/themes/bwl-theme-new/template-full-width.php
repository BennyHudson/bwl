<?php
	//Template Name: Full Width 
	get_header(); 
?>
	<section class="container full-width">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<h1 class="page-title"><?php the_title(); ?></h1>
			<?php if(has_post_thumbnail()) { ?>
				<section class="single-feature">
					<?php the_post_thumbnail('full'); ?>
				</section>
			<?php } ?>
			<?php the_content(); ?>
		<?php endwhile; endif; ?>
	</section>
<?php get_footer(); ?>
