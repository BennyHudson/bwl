<?php
	//Template Name: Landing Page
	get_header(); 
?>
	<section class="container">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<section class="home-feature">
			<aside class="home-slider">
				<ul id="feature-slider">
					<?php $postslist = get_posts('numberposts=3&cat=10');
						foreach ($postslist as $post) :
						setup_postdata($post);
					?>
						<li>
							<aside class="feature-content">
								<h2><?php the_title(); ?></h2>
	                            <div class="excerpt-clip">
	                            	<?php the_excerpt(); ?>
	                           	</div>
	                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="button">Read more</a> 
							</aside>
							<aside class="feature-image">
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('thumbnail'); ?></a>
							</aside>
						</li>
						<?php wp_reset_postdata(); ?>
					<?php endforeach; ?>
				</ul>
			</aside>
			<aside class="home-sidebar">
	        	<?php if(get_field('home_sidebar')) : ?>
	            	<?php while(the_repeater_field('home_sidebar')) : ?>
	                	<a href="<?php the_sub_field('page_link'); ?>">
	                    	<img src="<?php the_sub_field('image'); ?>">
	                    </a>
	                <?php endwhile; ?>
	            <?php endif; ?>
			</aside>
		</section>
		<section class="home-split">
        	<?php if(get_field('footer_ad_zones')) : ?>
            	<?php while(the_repeater_field('footer_ad_zones')) : ?>
                	<aside>
                        <a href="<?php the_sub_field('page_link'); ?>">
                            <img src="<?php the_sub_field('image'); ?>">
                        </a>
                    </aside>
                    <?php wp_reset_postdata(); ?>
                <?php endwhile; ?>
            <?php endif; ?>
        </section>
        <section class="home-content">
        	<?php dynamic_sidebar( 'home-widgets' ); ?>
        </section>
        <section class="home-split">
        	<?php if(get_field('extra_home_ad_zones')) : ?>
            	<?php while(the_repeater_field('extra_home_ad_zones')) : ?>
                	<aside>
                        <a href="<?php the_sub_field('page_link'); ?>">
                            <img src="<?php the_sub_field('image'); ?>">
                        </a>
                    </aside>
                    <?php wp_reset_postdata(); ?>
                <?php endwhile; ?>
            <?php endif; ?>
        </section>
		<?php endwhile; endif; ?>
	</section>
<?php get_footer(); ?>
