<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 * 
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( !defined('ABSPATH') ) { die('-1'); }

$event_id = get_the_ID();

?>
	<?php while ( have_posts() ) :  the_post(); ?>
		<section class="tribe-events-single">
            <aside class="page-main">
                <h1 class="page-title"><?php the_title(); ?></h1>
                <?php the_post_thumbnail('full'); ?>
                <?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
                <?php the_content(); ?>
                <?php do_action( 'tribe_events_single_event_after_the_content' ) ?>
                <?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
                    <?php
                    /**
                     * The tribe_events_single_event_meta() function has been deprecated and has been
                     * left in place only to help customers with existing meta factory customizations
                     * to transition: if you are one of those users, please review the new meta templates
                     * and make the switch!
                     */
                    if ( ! apply_filters( 'tribe_events_single_event_meta_legacy_mode', false ) )
                        tribe_get_template_part( 'modules/meta' );
                    else echo tribe_events_single_event_meta()
                    ?>
                <?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>
            </aside>
            <aside class="page-sidebar">
    			<?php get_sidebar(); ?>
    		</aside>   
		</section>
	<?php endwhile; ?>
