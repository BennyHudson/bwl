<?php get_header(); ?>
	<?php if(!is_user_logged_in()) { ?>
		<div class="overlay" id="member-register">
			<div class="lightbox-container">
				<a href="#" class="lightbox-close"><i class="fa fa-times-circle"></i></a>
				<section>
					<h2 class="overlay-head">Login/Register</h2>
					<section class="overlay-content">
						<a href="#" class="accordion-trigger">Login</a>
						<section class="accordion-content open">
							<?php dynamic_sidebar( 'login-area' ); ?>
						</section>
						<a href="#" class="accordion-trigger">Register</a>
                        <section class="accordion-content">
                            <?php echo do_shortcode('[gravityform id=12 description=false ajax=true tabindex=49]'); ?>  
                        </section>
					</section>
				</section>
			</div>
		</div>
	<?php } ?>
	<section class="container">
		<?php woocommerce_content(); ?>
	</section>
<?php get_footer(); ?>
