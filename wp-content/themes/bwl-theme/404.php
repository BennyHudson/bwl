<?php get_header(); ?>

	<section class="container" id="error">
    	<h1>Page Not Found</h1>
        <p>The page you are searching for no longer exists. Please head back to the <a href="<?php bloginfo('url'); ?>">homepage</a> and try again</p>
	</section>

<?php get_footer(); ?>