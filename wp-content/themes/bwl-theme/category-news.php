<?php get_header(); ?>

	<section class="container">
    	<aside id="post-content" class="news">
            <h1 class="post-title"><?php single_cat_title(''); ?></h1>
            <?php if (have_posts()) : ?>
            <?php $post = $posts[0]; $c=0;?>
            <?php while (have_posts()) : the_post(); ?>
                <?php $c++;
                    if( !$paged && $c == 1) :?>
                    <article class="feature-post">
                    	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('full'); ?></a>
                        <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                        <span class="the-date"><?php the_time('l jS F Y'); ?></span>            
                        <section class="excerpt-clip">
                        	<?php the_excerpt(); ?>
                        </section>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="read-more">Read more</a>
                    </article>
                    <?php else :?>
                    <article class="standard-post">
                    	<section class="title-clip">
	                    	<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                        </section>
                        <span class="the-date"><?php the_time('l jS F Y'); ?></span> 
                        <aside class="feature">
                        	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('medium'); ?></a>
                        </aside>  
                        <aside class="excerpt">
                        	<section class="excerpt-clip">
								<?php the_excerpt(); ?>
                            </section>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="read-more">Read more</a>
                        </aside>
                    </article>               
                <?php endif;?>
            <?php endwhile; ?>
            	<div class="clear"></div>
                <div class="page-nav">
                    <span class="prev-posts"><?php previous_posts_link('Previous') ?></span><span class="next-posts"><?php next_posts_link('Next') ?></span>
                </div>
            <?php endif; ?>
        </aside>
        <aside id="post-sidebar">
         	<?php get_sidebar(); ?>
        </aside>
	</section>

<?php get_footer(); ?>