<?php get_header(); ?>

	<section class="container">
    	<aside id="post-content" class="category">
            <h1 class="post-title"><?php single_cat_title(''); ?></h1>
            <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
                <article>
                	<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                    <section class="excerpt-clip"
	                    <?php the_excerpt(); ?>
                    </section>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="read-more">Read More</a>
                </article>
            <?php endwhile; ?>
            	<div class="clear"></div>
                <div class="page-nav">
                    <span class="prev-posts"><?php previous_posts_link('Previous') ?></span><span class="next-posts"><?php next_posts_link('Next') ?></span>
                </div>
            <?php endif; ?>
        </aside>
        <aside id="post-sidebar">
         	<?php get_sidebar(); ?>
        </aside>
	</section>

<?php get_footer(); ?>