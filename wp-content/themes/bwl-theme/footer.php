            <footer>
            	<section class="container">
                	<?php if(!is_front_page()) : ?>
                    	<?php $recent = new WP_Query("page_id=5"); while($recent->have_posts()) : $recent->the_post();?>
                    	<section class="home-split">
                            <?php if(get_field('footer_ad_zones')) : ?>
								<?php while(the_repeater_field('footer_ad_zones')) : ?>
                                    <aside>
                                        <a href="<?php the_sub_field('page_link'); ?>">
                                            <img src="<?php the_sub_field('image'); ?>">
                                        </a>
                                    </aside>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        </section>
                        <?php endwhile; ?>	
                    <?php endif; ?>
                    <section class="footer-ad">
                    	<?php 
                                $args = array(
                                    'post_type'	=> 'adverts',
                                    'showposts'	=> 1,
									'orderby'	=> 'rand',
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'advert-category',
                                            'field' => 'slug',
                                        	'terms' => '1160x150'
                                    	)
                                	)
                                );  
                                $the_query = new WP_Query( $args );
                            	if($the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); 
                            ?>
                            	<section class="header-ad">
	                            	<a href="<?php the_field('target'); ?>" title="<?php the_title(); ?>" target="_blank"><?php the_post_thumbnail('full'); ?></a>
                                </section>
                            <?php wp_reset_postdata(); ?>
                        <?php endwhile; else: ?>
                          	<p>Nothing Here.</p>                                        
                        <?php endif; wp_reset_postdata(); ?>
                    </section>
                    <section class="footer-partners">
                    	<h2>Partners</h2>
                    	<section class="partners">
                        	<?php
								$queryObject = new WP_Query( 'post_type=partners&posts_per_page=10' );
								// The Loop!
								if ($queryObject->have_posts()) {
							?>
                            	<ul>
									<?php
                                        while ($queryObject->have_posts()) {
                                        $queryObject->the_post();
                                    ?>
                                    	<?php
											$terms = get_the_terms( $post->ID , 'partner-category' );
											foreach ( $terms as $term ) {
											}
										?>
                                        <li>
                                        	<a href="<?php the_field('url'); ?>" target="_blank"><?php the_post_thumbnail('small'); ?></a>
                                            <span><?php echo($term->name); ?></span>
                                        </li>
                                    <?php
                                        }
                                    ?>
                            	</ul>
							<?php
								}
							?>                           
                        </section>
                    </section>
                    <section class="footer-main">
                    	<?php dynamic_sidebar( 'footer-menus' ); ?>
                        <aside class="contact-info">
                        	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt="<?php wp_title('|'); ?>">
                            <h2>British Weight Lifting</h2>
                            <address>Belmont House, 20 Wood Lane, Headingley, Leeds, LS6 2AE</address>
                            <ul>
                            	<li><span>T:</span> 0113 224 9402</li>
                                <li><span>E:</span> <a href="mailto:enquiries@britishweightlifting.org">enquiries@britishweightlifting.org</a></li>
                            </ul>
                            <section class="footer-socials">
	                            <a href="https://twitter.com/gbweightlifting" target="_blank"><i class="fa fa-twitter fa-2x"></i></a><a href="https://www.facebook.com/bwl.org" target="_blank"><i class="fa fa-facebook fa-2x"></i></a><a href="https://www.youtube.com/user/GBWeights" target="_blank"><i class="fa fa-youtube-square fa-2x"></i></a>
                            </section>
                        </aside>
                    </section>
                </section>            
            </footer>
        </section>
    </section>
</body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
 
  ga('create', 'UA-12080482-35', 'auto');
  ga('send', 'pageview');
 
</script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/script.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/jquery.bxslider.min.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/jquery.fitvids.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/scripts/font-awesome/css/font-awesome.min.css">
<?php if(!is_front_page()) { ?>
    <?php wp_footer() ?>
<?php } ?>
</html>