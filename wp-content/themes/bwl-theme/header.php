<!doctype html>

<html xmlns="http://www.w3.org/1999/xhtml">

<?php get_template_part('includes/includes', 'favicon'); ?>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />

<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/modernizr-latest.js" type="text/javascript"></script>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/jquery-1.8.2.min.js" type="text/javascript"></script>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<meta name="viewport" content="width=device-width"/>  

<title><?php wp_title('|'); ?></title>

<?php wp_head(); ?>

<?php if(is_front_page()) { ?>

    <?php wp_footer() ?>

<?php } ?>

</head>

<body <?php post_class(); ?>>

	<div class="lightbox-overlay" id="login">

        <div class="lightbox-bg">

            <div class="lightbox-container">

                <a href="#" class="close-button icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/lightbox-close.png" alt="close"></a>

                <section class="bwl-login">

                   	<?php dynamic_sidebar( 'login-area' ); ?>                    

                </section>

            </div>

        </div>

    </div>

    <?php if(!is_front_page()) { ?>

        <?php if(!is_user_logged_in() ) { ?>

            <div class="lightbox-overlay" id="member-register">

                <div class="lightbox-bg">

                    <div class="lightbox-container">

                        <a href="#" class="close-button icon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/lightbox-close.png" alt="close"></a>

                        <section class="registration-form">

                            <a href="#" class="accordion-trigger active">Login</a>

                            <section class="accordion-content open">

                                <?php dynamic_sidebar( 'login-area' ); ?>

                            </section>

                            <a href="#" class="accordion-trigger">Register</a>

                            <section class="accordion-content">

                                <?php echo do_shortcode('[gravityform id=12 description=false ajax=true tabindex=49]'); ?>  

                            </section>

                        </section>

                    </div>

                </div>

            </div>

        <?php } ?>

    <?php } ?>

    <section id="footer-fix">

    	<section id="page-content">

        	<nav class="clearfix"> 

				<?php wp_nav_menu( array( 'theme_location' => 'mobile-menu' ) ); ?>

                <a href="#" id="pull">Menu</a>  

            </nav>

        	<header>

            	<section class="header-meta">

                	<section class="container">

                    	<aside>

                        	<ul>

                        		<li>

									<?php if ( is_user_logged_in() ) { ?>

                                        <a href="/my-account/" title="My Account">My Account</a>

                                     <?php } 

                                     else { ?>

                                        <a href="#" id="login-trigger">Login</a>

                                     <?php } ?>

                                 </li>

                            </ul>

                            <?php wp_nav_menu( array( 'theme_location' => 'about-menu' ) ); ?>	

                        </aside>

                        <aside class="header-search">

                        	<a href="https://twitter.com/gbweightlifting" target="_blank"><i class="fa fa-twitter fa-2x"></i></a><a href="https://www.facebook.com/bwl.org" target="_blank"><i class="fa fa-facebook fa-2x"></i></a><a href="https://www.youtube.com/user/GBWeights" target="_blank"><i class="fa fa-youtube-square fa-2x"></i></a><?php dynamic_sidebar( 'header-search' ); ?>

                        </aside>	

                    </section>

                </section>

                <section class="header-main">

                	<section class="container">

                    	<aside id="logo">

                        	<a href="<?php bloginfo('url'); ?>">

								<?php echo is_front_page() ? '<h1 id="wf-logo">' : '<h2 id="wf-logo">'; ?><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt="<?php wp_title('|'); ?>"><?php echo is_front_page() ? '</h1>' : '</h2>'; ?>

                            </a>

                        </aside>

                        <aside id="header-ad">

                        	<?php 

                                $args = array(

                                    'post_type'	=> 'adverts',

                                    'showposts'	=> 1,

									'orderby'	=> 'rand',

                                    'tax_query' => array(

                                        array(

                                            'taxonomy' => 'advert-category',

                                            'field' => 'slug',

                                        	'terms' => '800x100'

                                    	)

                                	)

                                );  

                                $the_query = new WP_Query( $args );

                            	if($the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); 

                            ?>

                            	<section class="header-ad">

	                            	<a href="<?php the_field('target'); ?>" title="<?php the_title(); ?>" target="_blank"><?php the_post_thumbnail('full'); ?></a>

                                </section>

                            <?php wp_reset_postdata(); ?>

                        <?php endwhile; else: ?>

                          	<p>Nothing Here.</p>                                        

                        <?php endif; wp_reset_postdata(); ?>

                        </aside>	

                    </section>

                </section>

                <nav>

                	<section class="container">

                    	<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>	

                    </section>

                </nav>

                <?php

					if($post->post_parent)

						$children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0");

					else

						$children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");

						if ($children) { ?>

                	<nav class="secondary">

                    	<section class="container">

                        	<ul id="secondary-nav">

                            	<?php echo $children; ?>

                            </ul>

                        </section>

                    </nav>        

                <?php } ?>

            </header>