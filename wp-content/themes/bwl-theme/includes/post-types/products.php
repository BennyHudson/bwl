<?php add_action('init', 'post_type_product');

/*-----------------------------------------------------------------------------------
Custom Post Type - Dresses
-------------------------------------------------------------------------------------*/
function post_type_product()
{
	$labels = array(
    'name' => _x('Products', 'post type general name'),
    'singular_name' => _x('Products', 'post type singular name'),
    'add_new' => _x('Add New', 'v4_product'),
    'add_new_item' => __('Add New Product')
  );
  
   $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
	'has_archive' => true,
    'show_ui' => true, 
    'query_var' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_icon' => get_stylesheet_directory_uri() .'/images/product.png',
    'menu_position' => null,
	'rewrite' => array('slug'=>'wood-flooring-products','with_front'=>false),
	'supports' => array('title','editor','thumbnail','page-attributes')
  ); 
  register_post_type('v4_product',$args);
  flush_rewrite_rules();
}

/*-----------------------------------------------------------------------------------
Custom Post Type - Meta Boxes
-------------------------------------------------------------------------------------*/
if($postType == 'v4_product' || $_POST['post_type'] == 'v4_product') 
{
	$meta_box = array(
	'id' => 'product-meta-box',
	'title' => 'Product Fields',
	'page' => 'v4_product',
	'context' => 'normal',
	'priority' => 'high',
	'fields' => array(
		/*--  */
		array(
			'type' => 'tabs',
			'labels' => array('Basic Info')
		),
		
		/*--  */
		array(
			'type' => 'open_tab_panel'
			),
		array(
			'name' => 'Sub Title',
			'desc' => 'Please enter the subtitle here',
			'id' => 'sub_title',
			'type' => 'text',
			'std' => ''
		),
		array(
			'name' => 'Maintenence &amp; Fitting Advice',
			'desc' => 'Please check the values that are relevant to the product',
			'id' => 'fitting',
			'type' => 'fitting',
			'std' => ''
		),
		array(
			'name' => 'Technical Specs',
			'desc' => 'Add your technical specs',
			'id' => 'tech_specs',
			'type' => 'wisywig',
			'std' => ''
		),
		array(
			'type' => 'close_tab_panel'
		)
	)
);

}


/*-----------------------------------------------------------------------------------
Custom Post Type - Taxonommies
-------------------------------------------------------------------------------------*/

add_action( 'init', 'create_product_taxonomies', 0 );

function create_product_taxonomies() 
{
  $labels = array(
    'name'                => _x( 'Product Category', 'taxonomy general name' ),
    'singular_name'       => _x( 'Product Category', 'taxonomy singular name' ),
    'search_items'        => __( 'Search Categories' ),
    'all_items'           => __( 'All Categories' ),
    'parent_item'         => __( 'Parent Category' ),
    'parent_item_colon'   => __( 'Parent Category:' ),
    'edit_item'           => __( 'Edit Category' ), 
    'update_item'         => __( 'Update Category' ),
    'add_new_item'        => __( 'Add New Category' ),
    'new_item_name'       => __( 'New Type Category' ),
    'menu_name'           => __( 'Product Categories' )
  ); 	

  $args = array(
    'hierarchical'        => true,
    'labels'              => $labels,
    'show_ui'             => true,
    'show_admin_column'   => true,
    'query_var'           => true,
    'rewrite'             => array( 'slug' => 'wood-flooring-collection' )
  );

  register_taxonomy( 'flooring_collection', array( 'v4_product' ), $args );
}


/*-----------------------------------------------------------------------------------
Custom Post Type - Admin Columns
-------------------------------------------------------------------------------------*/
add_filter( 'manage_edit-v4_product_columns', 'edit_v4_product_columns' ) ;

function edit_v4_product_columns( $columns ) {

	$columns = array(
		'cb' => '<input type="checkbox" />',
		'image' => __( 'Image' ),
		'title' => __( 'Title' ),
		'wpseo-score' => __( 'Score' ),
		'wpseo-metadesc' => __( 'Meta Desc' ),
		'wpseo-title' => __( 'SEO Title' ),
		'date' => __( 'Date' )
	);

	return $columns;
}

/* -- Add custom columns to custom post list table */
add_action( 'manage_v4_product_posts_custom_column', 'manage_v4_product_columns', 10, 2 );
function manage_v4_product_columns( $column, $post_id ) {
	global $post, $member_classes, $member_functions;

	switch( $column ) {
		/* If displaying the 'class' column. */
		case 'image' :
			echo get_the_post_thumbnail($post_id,'thumbnail');
			break;
		
		default :
			break;
	}
}

?>