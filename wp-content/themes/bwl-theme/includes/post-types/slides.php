<?php add_action('init', 'post_type_slides');

/*-----------------------------------------------------------------------------------
Custom Post Type - Dresses
-------------------------------------------------------------------------------------*/
function post_type_slides()
{
	$labels = array(
    'name' => _x('Slides', 'post type general name'),
    'singular_name' => _x('Slides', 'post type singular name'),
    'add_new' => _x('Add New', 'slide'),
    'add_new_item' => __('Add New Slide')
  );
  
   $args = array(
    'labels' => $labels,
    'public' => false,
    'publicly_queryable' => false,
	'has_archive' => false,
    'show_ui' => true, 
    'query_var' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_icon' => get_stylesheet_directory_uri() .'/images/slide.png',
    'menu_position' => null,
	'rewrite' => array('slug'=>'slide','with_front'=>false),
	'supports' => array('title','thumbnail','page-attributes','editor')
  ); 
  register_post_type('slide',$args);
  flush_rewrite_rules();
}





  


// --- Custom Columns
add_filter( 'manage_edit-slide_columns', 'edit_slide_columns' ) ;

function edit_slide_columns( $columns ) {

	$columns = array(
		'cb' => '<input type="checkbox" />',
		'image' => __( 'Image' ),
		'title' => __( 'Title' ),
		'wpseo-score' => __( 'Score' ),
		'wpseo-metadesc' => __( 'Meta Desc' ),
		'wpseo-title' => __( 'SEO Title' ),
		'date' => __( 'Date' )
	);

	return $columns;
}

/* -- Add custom columns to custom post list table */
add_action( 'manage_slide_posts_custom_column', 'manage_slide_columns', 10, 2 );
function manage_slide_columns( $column, $post_id ) {
	global $post, $member_classes, $member_functions;

	switch( $column ) {
		/* If displaying the 'class' column. */
		case 'image' :
			echo get_the_post_thumbnail($post_id,'thumbnail');
			break;
		
		default :
			break;
	}
}

?>