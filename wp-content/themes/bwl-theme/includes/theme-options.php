<?php
// -- http://www.onedesigns.com/tutorials/how-to-create-a-wordpress-theme-options-page

// Default options values
$sa_options = array(
	'slot_1',
	'slot_2',
	'slot_3',
	'slot_4'
);

if ( is_admin() ) : // Load only if we are viewing an admin page

function sa_register_settings() {
	register_setting( 'sa_theme_options', 'sa_options', 'sa_validate_options' );
}
add_action( 'admin_init', 'sa_register_settings' );

function sa_theme_options() {
	add_theme_page( 'Theme Options', 'Theme Options', 'edit_theme_options', 'theme_options', 'sa_theme_options_page' );
}
add_action( 'admin_menu', 'sa_theme_options' );


// Store categories in array
$saCollections[0] = array(
	'value' => 0,
	'label' => ''
);
$sa_cats = get_posts('post_type=v4_product&numberposts=-1'); $i = 1;
foreach( $sa_cats as $sa_cat ) :
	$saCollections[$sa_cat->ID] = array(
		'value' => $sa_cat->ID,
		'label' => $sa_cat->post_title
	);
	$i++;
endforeach;


// Function to generate options page
function sa_theme_options_page() {
	global $sa_options, $saCollections;

	if ( ! isset( $_REQUEST['updated'] ) )
		$_REQUEST['updated'] = false; // This checks whether the form has just been submitted. ?>

	<div class="wrap">

	<?php screen_icon(); echo "<h2>" . get_current_theme() . __( ' Theme Options' ) . "</h2>";
	// This shows the page's name and an icon if one has been provided ?>

	<?php if ( false !== $_REQUEST['updated'] ) : ?>
	<div class="updated fade"><p><strong><?php _e( 'Options saved' ); ?></strong></p></div>
	<?php endif; // If the form has just been submitted, this shows the notification ?>

	<form method="post" action="options.php">

	<?php $settings = get_option( 'sa_options', $sa_options ); ?>
	
	<?php settings_fields( 'sa_theme_options' ); ?>

	<table class="form-table"><!-- Grab a hot cup of coffee, yes we're using tables! -->


	<?php for($i=1;$i<5;$i++) : ?>
	<tr valign="top">
		<th scope="row"><label for="slot_<?php echo $i; ?>">Featured Product Slot <?php echo $i; ?></label><br/><small>This appears on the left under the main header</small></th>
		<td>
		<select id="featured_cat" name="sa_options[slot_<?php echo $i; ?>]">
		<?php
		foreach ( $saCollections as $category ) :
			$label = $category['label'];
			$selected = '';
			if ( $category['value'] == $settings['slot_' . $i] )
				$selected = 'selected="selected"';
			echo '<option style="padding-right: 10px;" value="' . esc_attr( $category['value'] ) . '" ' . $selected . '>' . $label . '</option>';
		endforeach;
		?>
		</select>
		</td>
	</tr>
	<?php endfor; ?>
	
	
	</table>

		<p class="submit"><input type="submit" class="button-primary" value="Save Options" /></p>

	</form>

	</div>

	<?php
}

// -- Validate & Clean
function sa_validate_options( $input ) {
	global $sa_options, $saCollections;

	$settings = get_option( 'sa_options', $sa_options );
	$prev = $settings['slot_1'];
	if ( !array_key_exists( $input['slot_1'], $saCollections ) )
		$input['slot_1'] = $prev;

	$prev = $settings['slot_2'];
	if ( !array_key_exists( $input['slot_2'], $saCollections ) )
		$input['slot_2'] = $prev;

	$prev = $settings['slot_3'];
	if ( !array_key_exists( $input['slot_3'], $saCollections ) )
		$input['slot_3'] = $prev;

	$prev = $settings['slot_4'];
	if ( !array_key_exists( $input['slot_4'], $saCollections ) )
		$input['slot_4'] = $prev;

	$prev = $settings['slot_5'];
	if ( !array_key_exists( $input['slot_5'], $saCollections ) )
		$input['slot_5'] = $prev;

	$prev = $settings['slot_6'];
	if ( !array_key_exists( $input['slot_6'], $saCollections ) )
		$input['slot_6'] = $prev;


	return $input;
}

endif;  // EndIf is_admin()
?>