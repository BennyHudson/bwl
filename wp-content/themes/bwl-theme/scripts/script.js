$(document).ready(function(){
	$(".container").fitVids();
	var pull        = $('#pull');  
		menu        = $('nav ul');  
		menuHeight  = menu.height();  		  
	$(pull).on('click', function(e) {  
		e.preventDefault();  
		menu.slideToggle();  
	});
	$(function() {
		$('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
					if (target.length) {
						$('html,body').animate({
					  		scrollTop: target.offset().top - 60
						}, 1000);
					return false;
				}
			}
		});
	});
	$('#home-slider').bxSlider({
		auto: true,
		autoHover: true,
		adaptiveHeight: true
	});
	$('.menu-item-has-children').hover(function() {
		$(this).children('ul').show();
	}, function() {
		$(this).children('ul').hide();
	});
	$('#login-trigger').click(function(e) {
		$('#login').show();
		slider = $('#login-forms').bxSlider({
			controls: false,
			pagerCustom: '#login-options',
			mode: 'fade'
		});
	});
	$('.close-button').click(function(e) {
		$('.lightbox-overlay').hide();
		e.preventDefault();
		slider.destroySlider();
	});
	$('#entry-trigger').click(function(e) {
		$('#gform_wrapper_4').slideDown();
		$(this).addClass('hidden');
		e.preventDefault();		
	});
	$('ul.strength-power-tabs').find('li > a').each( function(i){
	  var postnum = i; // remove any possible overloaded "+" operator ambiguity
	  $(this).attr('data-slide-index', postnum);
	});
	$('.strength-power-tables').bxSlider({
		controls: false,
		adaptiveHeight: true,
		pagerCustom: '.strength-power-tabs',
		mode: 'fade'
	});
	$('.accordion-trigger').click(function(e){
		if($(this).hasClass('active')) {
			$('.open').slideUp().removeClass('open');
			$('.active').removeClass('active');
		} else {
			$('.open').slideUp().removeClass('open');
			$('.active').removeClass('active');
			$(this).addClass('active');
			$(this).next('.accordion-content').slideDown().addClass('open');
		}
	});
});
$(document).bind('keyup', function(e) {
	if(e.keyCode == 27) {
		$('.close-button').click();
	}
});