<?php get_header(); ?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    	<section class="container woocommerce-content">
        	<section class="strength-power">
            	<?php get_template_part('partials/content', 'meta'); ?>
                <?php the_post_thumbnail('full'); ?>
                <h1 class="post-title"><?php the_title(); ?></h1>
                <?php the_content(); ?>
                <a href="#" id="entry-trigger">SUBMIT AND SHARE YOUR RESULTS</a>
                <section class="strength-entry-form">
                	<?php echo do_shortcode('[gravityform id=4 title=false description=false tabindex=49]'); ?>
                </section>
                <section class="strength-power-content">
                	<ul class="strength-power-tabs">
                    	<li><a href="#">Under 18s</a></li>
                        <li><a href="#">18-35s</a></li>
                        <li><a href="#">Over 35s</a></li>
                        <li><a href="#">Over 55s</a></li>
                        <li><a href="#">Students</a></li>
                    </ul>
                    <ul class="strength-power-tables">
                    	<li>
                            <h2>Under 18s</h2>
                            <table class="strength-table" width="100%" cellpadding="0" cellspacing="0" border="0">
                            	<thead>
                                	<tr>
                                    	<th>Date</th>
                                        <th>Name</th>
                                        <th>Gym/Club Name</th>
                                        <th>Gender</th>
                                        <th>Score</th>
                                    </tr>	
                                </thead>
                                <?php
								$terms = get_field('challenge_link');
								$slug = $terms->slug;
								$args = array(
									'post_type' => 'strengthentries',
									'posts_per_page' => 0,
									'meta_key' => 'entrant-score',
									'orderby' => 'meta_value_num',
									'order' => 'DESC',
									'tax_query' => array(
										'relation' => 'AND',
											array(
												'taxonomy' => 'challenges',
												'field' => 'slug',
												'terms' => $slug
											),
											array(
												'taxonomy' => 'age-group',
												'terms' => 'under-18',
												'field' => 'slug'
											),
										)
									);
								$the_query = new WP_Query( $args );
								if($the_query->have_posts() ) : ?>
								<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                	<tr>
                                    	<td><?php echo get_post_meta($post->ID,'entrant-exercise-date', true) ?></td>
		                                <td><?php the_title(); ?></td>
                                        <td><?php echo get_post_meta($post->ID,'entrant-gym-name', true) ?></td>
                                        <td><?php echo get_post_meta($post->ID,'entrant-gender', true) ?></td>
                                        <td><?php echo get_post_meta($post->ID,'entrant-score', true) ?></td>
                                    </tr>
                                <?php endwhile; else: ?>
                            	<tr>
                                	<td colspan="5">No Entries yet, be the first</td>
                                </tr>
                            <?php endif; wp_reset_postdata(); ?>	
                            </table> 
                        </li>
                        <li>
                            <h2>18-35s</h2>
                            <table class="strength-table" width="100%" cellpadding="0" cellspacing="0" border="0">
                            	<thead>
                                	<tr>
                                    	<th>Date</th>
                                        <th>Name</th>
                                        <th>Gym/Club Name</th>
                                        <th>Gender</th>
                                        <th>Score</th>
                                    </tr>	
                                </thead>
                                <?php
								$terms = get_field('challenge_link');
								$slug = $terms->slug;
								$args = array(
									'post_type' => 'strengthentries',
									'posts_per_page' => 0,
									'meta_key' => 'entrant-score',
									'orderby' => 'meta_value_num',
									'order' => 'DESC',
									'tax_query' => array(
										'relation' => 'AND',
											array(
												'taxonomy' => 'challenges',
												'field' => 'slug',
												'terms' => $slug
											),
											array(
												'taxonomy' => 'age-group',
												'terms' => '18-35',
												'field' => 'slug'
											),
										)
									);
								$the_query = new WP_Query( $args );
								if($the_query->have_posts() ) : ?>
								<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                	<tr>
                                    	<td><?php echo get_post_meta($post->ID,'entrant-exercise-date', true) ?></td>
		                                <td><?php the_title(); ?></td>
                                        <td><?php echo get_post_meta($post->ID,'entrant-gym-name', true) ?></td>
                                        <td><?php echo get_post_meta($post->ID,'entrant-gender', true) ?></td>
                                        <td><?php echo get_post_meta($post->ID,'entrant-score', true) ?></td>
                                    </tr>
                                <?php endwhile; else: ?>
                            	<tr>
                                	<td colspan="5">No Entries yet, be the first</td>
                                </tr>
                            <?php endif; wp_reset_postdata(); ?>	
                            </table> 
                        </li>
                        <li>
                            <h2>Over 35s</h2>
                            <table class="strength-table" width="100%" cellpadding="0" cellspacing="0" border="0">
                            	<thead>
                                	<tr>
                                    	<th>Date</th>
                                        <th>Name</th>
                                        <th>Gym/Club Name</th>
                                        <th>Gender</th>
                                        <th>Score</th>
                                    </tr>	
                                </thead>
                                <?php
								$terms = get_field('challenge_link');
								$slug = $terms->slug;
								$args = array(
									'post_type' => 'strengthentries',
									'posts_per_page' => 0,
									'meta_key' => 'entrant-score',
									'orderby' => 'meta_value_num',
									'order' => 'DESC',
									'tax_query' => array(
										'relation' => 'AND',
											array(
												'taxonomy' => 'challenges',
												'field' => 'slug',
												'terms' => $slug
											),
											array(
												'taxonomy' => 'age-group',
												'terms' => 'over-35',
												'field' => 'slug'
											),
										)
									);
								$the_query = new WP_Query( $args );
								if($the_query->have_posts() ) : ?>
								<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                	<tr>
                                    	<td><?php echo get_post_meta($post->ID,'entrant-exercise-date', true) ?></td>
		                                <td><?php the_title(); ?></td>
                                        <td><?php echo get_post_meta($post->ID,'entrant-gym-name', true) ?></td>
                                        <td><?php echo get_post_meta($post->ID,'entrant-gender', true) ?></td>
                                        <td><?php echo get_post_meta($post->ID,'entrant-score', true) ?></td>
                                    </tr>
                                <?php endwhile; else: ?>
                            	<tr>
                                	<td colspan="5">No Entries yet, be the first</td>
                                </tr>
                            <?php endif; wp_reset_postdata(); ?>	
                            </table> 
                        </li>
                        <li>
                            <h2>Over 55s</h2>
                            <table class="strength-table" width="100%" cellpadding="0" cellspacing="0" border="0">
                            	<thead>
                                	<tr>
                                    	<th>Date</th>
                                        <th>Name</th>
                                        <th>Gym/Club Name</th>
                                        <th>Gender</th>
                                        <th>Score</th>
                                    </tr>	
                                </thead>
                                <?php
								$terms = get_field('challenge_link');
								$slug = $terms->slug;
								$args = array(
									'post_type' => 'strengthentries',
									'posts_per_page' => 0,
									'meta_key' => 'entrant-score',
									'orderby' => 'meta_value_num',
									'order' => 'DESC',
									'tax_query' => array(
										'relation' => 'AND',
											array(
												'taxonomy' => 'challenges',
												'field' => 'slug',
												'terms' => $slug
											),
											array(
												'taxonomy' => 'age-group',
												'terms' => 'over-55',
												'field' => 'slug'
											),
										)
									);
								$the_query = new WP_Query( $args );
								if($the_query->have_posts() ) : ?>
								<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                	<tr>
                                    	<td><?php echo get_post_meta($post->ID,'entrant-exercise-date', true) ?></td>
		                                <td><?php the_title(); ?></td>
                                        <td><?php echo get_post_meta($post->ID,'entrant-gym-name', true) ?></td>
                                        <td><?php echo get_post_meta($post->ID,'entrant-gender', true) ?></td>
                                        <td><?php echo get_post_meta($post->ID,'entrant-score', true) ?></td>
                                    </tr>
                                <?php endwhile; else: ?>
                            	<tr>
                                	<td colspan="5">No Entries yet, be the first</td>
                                </tr>
                            <?php endif; wp_reset_postdata(); ?>	
                            </table> 
                        </li>
                        <li>
                            <h2>Students</h2>
                            <table class="strength-table" width="100%" cellpadding="0" cellspacing="0" border="0">
                            	<thead>
                                	<tr>
                                    	<th>Date</th>
                                        <th>Name</th>
                                        <th>Institution</th>
                                        <th>Gender</th>
                                        <th>Score</th>
                                    </tr>	
                                </thead>
                                <?php
									$terms = get_field('challenge_link');
									$slug = $terms->slug;
									$args = array(
										'post_type' => 'strengthentries',
										'posts_per_page' => 0,
										'meta_key' => 'entrant-score',
										'orderby' => 'meta_value_num',
										'order' => 'DESC',
										'tax_query' => array(
											'relation' => 'AND',
												array(
													'taxonomy' => 'challenges',
													'field' => 'slug',
													'terms' => $slug
												),
												array(
													'taxonomy' => 'age-group',
													'terms' => 'student',
													'field' => 'slug'
												),
											)
										);
									$the_query = new WP_Query( $args );
									if($the_query->have_posts() ) : ?>
								<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                	<tr>
                                    	<td><?php echo get_post_meta($post->ID,'entrant-exercise-date', true) ?></td>
		                                <td><?php the_title(); ?></td>
                                        <td><?php echo get_post_meta($post->ID,'entrant-institution', true) ?></td>
                                        <td><?php echo get_post_meta($post->ID,'entrant-gender', true) ?></td>
                                        <td><?php echo get_post_meta($post->ID,'entrant-score', true) ?></td>
                                    </tr>
                                <?php endwhile; else: ?>
                            	<tr>
                                	<td colspan="5">No Entries yet, be the first</td>
                                </tr>
                            <?php endif; wp_reset_postdata(); ?>	
                            </table> 
                        </li>
                    </ul>
                </section>
            </section>
            
        </section>
        
    <?php endwhile; else: ?>
    	<?php get_template_part('partials/template', 'error'); ?>
    <?php endif; ?>

<?php get_footer(); ?>