<?php
	// Template Name: Alt Page
?>
<?php get_header(); ?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    	<section class="container">
            <aside id="post-content">
                <h1 class="post-title"><?php the_title(); ?></h1>
                <?php the_post_thumbnail('full'); ?>
                <?php the_content(); ?>
            </aside>
            <aside id="post-sidebar">
            	<?php get_sidebar(); ?>
            </aside>
        </section>
    <?php endwhile; else: ?>
    	<?php get_template_part('partials/template', 'error'); ?>
    <?php endif; ?>

<?php get_footer(); ?>