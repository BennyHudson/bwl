<?php
	// Template Name: Club Finder
	include TEMPLATEPATH . '/includes/classes/geocode.class.php';
	$errors = array();
	$locations = null;
	if(isset($_POST['submit'])) {
		if($_POST['zip']) {	
			if(IsPostcode($_POST['zip'])) {
				$geocode = new PostcodeLookup;
				//$pcode = $geocode->isinDatabase($_POST['zip']);
				if(!$pcode) {
					$geo = $geocode->getCoordinates($_POST['zip']);
					if($geo) {
						$add = $geocode->addToDatabase($_POST['zip'],$geo);
						$pcode = $geo;
					} else {
						$errors[] = 'Error Geocoding that postcode! Please try again';
					}
				} 
				
				
				$results = $wpdb->get_results("SELECT A.post_id, 
				( 3959 * 
					acos( 
						cos( 
							radians(" . $pcode['lat']  . ") 
						) 
						* cos( radians( lat ) ) * cos( radians(lng) - radians(" . $pcode['lon'] . ") ) 
						+ sin(radians(" . $pcode['lat'] . ")) * sin( radians( lat ) ) 
					) 
				) AS distance 
				FROM " . $wpdb->prefix . "postcodes A ORDER BY DISTANCE LIMIT 5");
	
			} else {
				$errors[] = 'Invalid post code entered';
			}
		} else {
			$errors[] = 'No post code entered';
		}
	}
?>
<?php get_header(); ?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    	<section class="container">
            <aside id="post-content">
                <h1 class="post-title"><?php the_title(); ?></h1>
                <?php the_content(); ?>
				
				<form method="post">
					<input type="text"  name="zip" class="zip-box" value="<?php echo @$_POST['zip']; ?>" placeholder="Enter postcode" />
					<input name="submit" type="submit" value="Search Now" class="retailers-submit"/>
				</form>
				
				<?php if($_POST['submit']) : ?>
					<?php  if( $results ) : ?>
						<ul class="club-listings">
						<?php foreach($results as $r) : ?>
							<?php $p = get_post($r->post_id); if($p) : ?>
							<?php $location =  get_post_meta($p->ID,'location', true); ?>
							<li>
                            	<h2><?php echo $p->post_title; ?> - <?php echo floor($r->distance); ?> miles away</h2>
                                <aside class="map">
                                   	<a href="<?php echo get_permalink($p); ?>"><img src="http://maps.google.com/maps/api/staticmap?center=<?php echo $location['longittue']; ?>,<?php echo $location['latitude']; ?>&amp;zoom=15&amp;markers=color:red|label:P|<?php echo $location['longittue']; ?>,<?php echo $location['latitude']; ?>&amp;sensor=false&amp;size=400x200" alt=""></a>
                                </aside>
                                <aside class="club-overview">
                                   	<p><?php echo get_post_meta($p->ID,'address1', true) ?><br/>
                                   	<?php if(get_post_meta($p->ID, 'address2', true)) { ?>
			                        	<?php echo get_post_meta($p->ID,'address2', true) ?><br/>
			                        <?php } ?>
									<?php echo get_post_meta($p->ID,'town', true) ?><br/>
			                        <?php echo get_post_meta($p->ID,'county', true); ?><br/>
			                        <?php echo get_post_meta($p->ID,'zip', true); ?></p>
                                    <?php if(get_post_meta($p->ID,'tel', true)) : ?>
										<p><?php echo get_post_meta($p->ID,'tel', true); ?></p>
									<?php endif; ?>
									<?php if(get_post_meta($p->ID,'url', true)) : ?>
										<p><a href="http://<?php echo str_replace('http://','',get_post_meta($p->ID,'url', true)); ?>"><?php echo str_replace('http://','',get_post_meta($p->ID,'url', true)); ?></a></p>
									<?php endif; ?>
                                    <a href="<?php echo get_permalink($p); ?>" class="read-more">More info</a>
                                </aside>	
							</li>
						<?php endif; endforeach; ?>
						</ul>
					<?php endif; ?>
				<?php endif; ?>
            </aside>
            <aside id="post-sidebar">
            	<?php get_sidebar(); ?>
            </aside>
        </section>
    <?php endwhile; else: ?>
    	<?php get_template_part('partials/template', 'error'); ?>
    <?php endif; ?>

<?php get_footer(); ?>
