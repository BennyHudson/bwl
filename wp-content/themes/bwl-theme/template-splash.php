<?php 
	// Template Name: Splash Page
?>
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/modernizr-latest.js" type="text/javascript"></script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5089c16f5cedcc91"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width"/>  
<title><?php wp_title('|'); ?></title>
<?php wp_head(); ?>
</head>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php $splashbackground = wp_get_attachment_image_src( get_post_thumbnail_id( $page->ID ), 'full' ); ?>
    <body class="splash-page" style="background: url('<?php echo $splashbackground[0]; ?>') top center no-repeat <?php the_field('background_colour'); ?>">
        <section id="footer-fix">
            <section id="page-content">
                <header>
                    <section class="header-meta">
                        <section class="container">
                            <aside>
                                <ul>
                                    <li>
                                        <?php if ( is_user_logged_in() ) { ?>
                                            <a href="/bwl/profile/" title="My Account">My Account</a>
                                         <?php } 
                                         else { ?>
                                            <a href="#" id="login-trigger">Login/Register</a>
                                         <?php } ?>
                                     </li>
                                </ul>
                                <?php wp_nav_menu( array( 'theme_location' => 'about-menu' ) ); ?>	
                            </aside>
                            <aside class="header-search">
                                <a href="#"><i class="fa fa-twitter fa-2x"></i></a><a href="#"><i class="fa fa-facebook fa-2x"></i></a><a href="#"><i class="fa fa-youtube fa-2x"></i></a><?php dynamic_sidebar( 'header-search' ); ?>
                            </aside>	
                        </section>
                    </section>
                    <section class="header-main">
                        <section class="container">
                            <aside id="logo">
                                <a href="<?php bloginfo('url'); ?>">
                                    <?php echo is_front_page() ? '<h1 id="wf-logo">' : '<h2 id="wf-logo">'; ?><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" alt="<?php wp_title('|'); ?>"><?php echo is_front_page() ? '</h1>' : '</h2>'; ?>
                                </a>
                            </aside>	
                        </section>
                    </section>
                </header>
                <section class="container">
                	<section class="splash-content">
                    	<aside class="splash-main">
                        	<h1 style="color: <?php the_field('text_colour'); ?>"><?php the_title(); ?></h1>
                            <div  style="color: <?php the_field('text_colour'); ?>">
	                            <?php the_content(); ?>
                            </div>
                        </aside>
                        <aside class="splash-buttons">
                        	<?php if(get_field('ticket_link')) : ?>
	                        	<a href="<?php the_field('ticket_link'); ?>">Buy Tickets</a>
                            <?php endif; ?>
                            <?php if(get_field('website_link')) : ?>
	                            <a href="<?php the_field('website_link'); ?>">Enter Website</a>
                            <?php endif; ?>
                        </aside>
                    </section>
                </section>
            </section>
        </section>
    </body>
<?php endwhile; else: ?>
  	<?php get_template_part('partials/template', 'error'); ?>
<?php endif; ?>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/script.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/jquery.bxslider.min.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/jquery.fitvids.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/scripts/font-awesome/css/font-awesome.min.css">
<?php wp_footer() ?>
</html>