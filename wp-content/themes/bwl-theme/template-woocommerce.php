<?php
	// Template Name: Woocommerce
?>
<?php get_header(); ?>
	<p class="demo_store">This is a demo store for testing purposes — no orders shall be fulfilled.</p>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    	<section class="container woocommerce-content">
        	<h1 class="post-title"><?php the_title(); ?></h1>
			<?php the_content(); ?>
        </section>
    <?php endwhile; else: ?>
    	<?php get_template_part('partials/template', 'error'); ?>
    <?php endif; ?>

<?php get_footer(); ?>