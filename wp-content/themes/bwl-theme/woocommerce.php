<?php get_header(); ?>
	<p class="demo_store">This is a demo store for testing purposes — no orders shall be fulfilled.</p>
	<section class="container">
    	<aside id="post-content" class="woocommerce">
        	<?php woocommerce_content(); ?>
        </aside>
        <aside id="post-sidebar">
        	<?php dynamic_sidebar( 'shop-sidebar' ); ?>
        	<?php get_sidebar(); ?>
        </aside>
    </section>

<?php get_footer(); ?>